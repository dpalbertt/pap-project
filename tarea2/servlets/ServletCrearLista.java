

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.DtLista;
import publicadores.TipoLista;
import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;

/**
 * Servlet implementation class ServletCrearLista
 */
@WebServlet("/ServletCrearLista")
public class ServletCrearLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletCrearLista() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			WSControladorElementoService cps2 = new WSControladorElementoServiceLocator();
			WSControladorElemento port2 = cps2.getWSControladorElementoPort();
			
			String nick = (String) request.getSession().getAttribute("ul");
			
			String nombreLista = request.getParameter("nombreLista").toString();
			//si el nombre de la lista es vacio, redirijo devuelta para atras
			if (nombreLista.equals("")) {
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('Por favor, ingrese un nombre valido'); window.location='/uytubewebapp/CrearLista.jsp' \"></body></html>");
			}
			else {
				//chequeo si ya tiene la lista
				String[] listasAux = port2.listarListasRepUsuario(nick);
				List<String> listas = new ArrayList<String>();
				for (int i =0; i < listasAux.length; i++) {
					listas.add(listasAux[i]);
				}
				if (listas.contains(nombreLista)) {
					String someMessage = "Ya tenes una lista con ese nombre. \n"
							+ "Te vamos a redirigir a Crear Lista nuevamente.";
					PrintWriter out = response.getWriter();
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/CrearLista.jsp' \"></body></html>");
				}
				else {
					boolean publica;
					if (request.getParameter("listaPub") != null)
						publica = false;
					else
						publica = true;
					
					DtLista aux = new DtLista(publica, nombreLista, null, TipoLista.Otra, nombreLista);
					port2.crearLista(aux,nick);
					String cat = request.getParameter("categoria");
					if (cat != null) {
						port2.asignarCategoria(cat, aux, nick);
					}
					String someMessage = "Se ha ingresado correctamente la lista <" + request.getParameter("nombreLista").toString() + ">";
					PrintWriter out = response.getWriter();
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
				}
			}
			
		} catch(Exception e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}
		
		

	}
	

}