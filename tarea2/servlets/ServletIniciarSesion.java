import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.DtUsuario;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletIniciarSesion")
public class ServletIniciarSesion extends HttpServlet {
   private static final long serialVersionUID = 1L;
    
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  // response.setContentType("text/plain");
	   //	boolean iniciarSesion = ucon.iniciarSesion(request.getParameter("nickname"),request.getParameter("password"));
	   	boolean iniciarSesion = false;
	   	String nick = request.getParameter("nickname");
		try {
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			publicadores.DtUsuario dtu	= port.getUsuario(nick);
			
			iniciarSesion = iniciarSesion(request.getParameter("nickname"),request.getParameter("password"));
			
			if(iniciarSesion && dtu.isHabilitado()== true) {				    
					request.getSession().setAttribute("ul",nick);
				   	 javax.servlet.RequestDispatcher rd;
				   	System.out.println(nick);
				     rd = request.getRequestDispatcher("/indexconSesion.jsp");
				     rd.forward(request, response);		
			   }else {
				   	javax.servlet.RequestDispatcher rd;
				   	rd = request.getRequestDispatcher("/iniciarSesion.jsp");
				   	rd.forward(request, response);
			   }			
			
			} catch (Exception e) {
			e.printStackTrace();
		}
	   
	}
   public boolean iniciarSesion(String usuario, String password) throws Exception {
		WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
		return port.iniciarSesion(usuario, password);
	}
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");
   
            
   }
}