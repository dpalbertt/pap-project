import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletValorarVideo")
public class ServletValorarVideo extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain"); 
			
	   	try{
	   	WSControladorElementoService cps = new WSControladorElementoServiceLocator();
	   	WSControladorElemento port = cps.getWSControladorElementoPort();	      
	      port.valorarVideo((String) request.getSession().getAttribute("ul"),request.getParameter("video"), request.getParameter("valorar"));
	   	}catch(Exception e) {
	   		e.printStackTrace();
	   	}
	   		request.setAttribute("mensaje", "Se ha valorado correctamente el video ");   	       		
	   	
	   	 javax.servlet.RequestDispatcher rd;
	   	if(request.getSession().getAttribute("ul") != null) {
	   		rd = request.getRequestDispatcher("/indexconSesion.jsp");
	   	 }else {
	   	rd = request.getRequestDispatcher("/index.jsp");}
	     rd.forward(request, response);
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");

       }
}