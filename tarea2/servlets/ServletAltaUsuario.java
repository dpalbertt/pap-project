

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;
import publicadores.DtUsuario;
import publicadores.DtCanal;


/**
 * Servlet implementation class ServletAltaUsuario
 */
@WebServlet("/ServletAltaUsuario")
public class ServletAltaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
      
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			DtCanal dtc= null;
			boolean tipoCanal = true;
			if (request.getParameter("_tipoCanal").equals("privado")) {
				tipoCanal = false;
			}
			String date = request.getParameter("_fecha");
			Date date2 = ParseFecha(date); 
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date2);

			dtc = new DtCanal();
			dtc.setNombre(request.getParameter("_nombreCanal"));
			dtc.setPublico(tipoCanal);
			dtc.setDescripcion(request.getParameter("_descripcion"));
	
		    DtUsuario dtu = new DtUsuario();
		    dtu.setNickname(request.getParameter("_nickname"));
		    dtu.setPassword(request.getParameter("clave1"));
		    dtu.setNombre(request.getParameter("_nombre"));
		    dtu.setApellido(request.getParameter("_apellido"));
		    dtu.setEmail(request.getParameter("_email"));
		    dtu.setFNacimiento(Calendar.getInstance());
		    dtu.setFoto(request.getParameter("_imagen"));
		    dtu.setPublico(tipoCanal);
		    dtu.setHabilitado(true);
		    
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
	
			List<String> usuarios = new ArrayList<String>(Arrays.asList(port.listarUsuariosTodos()));
			System.out.println("la cantidad de usuarios es"+usuarios.size());
			String _nickname = request.getParameter("_nickname").toString();
			String _email = request.getParameter("_email").toString();
			if (usuarios.contains(_nickname)) {
				String someMessage = "El  nickname " + request.getParameter("_nickname").toString() + " ya existe.";
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/altaUsuario.jsp' \"></body></html>");
			}else if(usuarios.contains(_email)) {
				String someMessage = "El  Email " + request.getParameter("_email").toString() + " ya existe.";
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/altaUsuario.jsp' \"></body></html>");
			}else{
				System.out.println("entre aca");
				dtu.setCanal(dtc);
				port.crearUsuario(dtu);
			
				String someMessage = "Usuario creado correctamente.";
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			}
		}catch(Exception e) {
			System.out.println("Error lanzado en ServletAltaUsuario "+e.getLocalizedMessage());
		}
	}
	
	
	
	 public static Date ParseFecha(String fecha)
	    {
	        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	        Date fechaDate = null;
	        try {
	            fechaDate = formato.parse(fecha);
	        } 
	        catch (ParseException ex) 
	        {
	            System.out.println(ex);
	        }
	        return fechaDate;
	    }



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	   



}
