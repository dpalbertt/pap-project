import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.persistence.*;
import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletAltaVideo")
public class ServletAltaVideo extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain");
	   System.out.println(request.getParameter("duracion"));
	      String categoria = request.getParameter("categoria");
	      System.out.println(categoria);
	      if(categoria.equals("Seleccione una categoria...")){
	    	 categoria = "";
	      }    
	      		
	      		String video_id = request.getParameter("url");
	      		if(video_id.contains("v=")) video_id = video_id.split("v=")[1];
	      		
	      		int ampersandPosition = video_id.indexOf('&');
	      		if(ampersandPosition != -1) {
	      		  video_id = video_id.substring(0, ampersandPosition);
	      		}	 
	      		String url = "http://www.youtube.com/embed/" + video_id;
	    	  try {
				datosVideo(request.getParameter("nomVid"), Integer.valueOf(request.getParameter("duracion")),url, request.getParameter("descripcion"),(String) request.getSession().getAttribute("ul"),categoria);
			} catch (NumberFormatException e) {

				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	  request.setAttribute("mensaje", "Se ha ingresado correctamente el video ");    
	       		
	   	 javax.servlet.RequestDispatcher rd;
	   	if(request.getSession().getAttribute("ul") != null) {
			   rd = request.getRequestDispatcher("/indexconSesion.jsp");
		   }else {
			   rd = request.getRequestDispatcher("/index.jsp");
		   }		   
	   		rd.forward(request, response);
	}

   public void datosVideo(String nomVid, int duracion, String url, String descripcion, String nomUsr, String categoria) throws Exception {
		WSControladorElementoService cps = new WSControladorElementoServiceLocator();
		WSControladorElemento port = cps.getWSControladorElementoPort();
		port.datosVideo(nomVid, duracion, url, descripcion, nomUsr, categoria);
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");

            
   }
}