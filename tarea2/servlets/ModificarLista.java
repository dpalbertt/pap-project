import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;

/**
 * Servlet implementation class ServletAgregarVideoListaInicial
 */
@WebServlet("/ModificarLista")
public class ModificarLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ModificarLista() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
				
			String nomLista = request.getParameter("lista");
			
			boolean publica;
			if (request.getParameter("listaPub") == null)
				publica = false;
			else
				publica = true;
			
			String usuario = request.getParameter("usuario");
			
			String categoriaVieja = request.getParameter("categoriaVieja");
			
			String categoriaNueva = request.getParameter("categoria");
			
			if (categoriaNueva != null && 
					!categoriaNueva.equals( categoriaVieja ) ) {
				//mando el modificar lista con el nombre de la categoria nueva
				modificarLista(nomLista,usuario,publica,categoriaNueva);
			} else {
				//mando el modificar lista con el nombre de la categoria vieja
				modificarLista(nomLista,usuario,publica,categoriaVieja);
				
			}
			
			//termino de hacer todo, lo mando a la ventana notificacion
			
			String someMessage = "Se ha modificado correctamente la lista <" + nomLista + ">";
			PrintWriter out = response.getWriter();
			out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		} catch(Exception e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}
		
		

	}
	  public void modificarLista(String nomLista, String usuario, boolean publica, String categoria) throws Exception {
			WSControladorElementoService cps = new WSControladorElementoServiceLocator();
			WSControladorElemento port = cps.getWSControladorElementoPort();
			port.modificarLista(nomLista, usuario, publica, categoria);
		}
}