

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.DtUsuario;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;

@WebServlet("/ServletBajaUsuario")
public class ServletBajaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/plain");
		String nick = request.getParameter("usuario");
		
		try {
			String pass = request.getParameter("password");
			String confirm = request.getParameter("confirmar");
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			port.getUsuario(nick);
			publicadores.DtUsuario dtu	= port.getUsuario(nick);
			String pUser = dtu.getPassword();

			if (pUser.equals(pass)  && pass.equals(confirm)) {
			port.bajaUsuario(dtu, false);
			String someMessage = "Baja exitosa.";
			PrintWriter out = response.getWriter();
			out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			}else{
				String someMessage = "Constraseña incorrecta.";
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/bajaUsuario.jsp' \"></body></html>");
			}

		} catch (ServiceException e) {
//			String sesion = (String) request.getSession().getAttribute("ul");
//			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
//			PrintWriter out = response.getWriter();
//				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");

		}      
		
	}
	

}
	
	
	
	
	

