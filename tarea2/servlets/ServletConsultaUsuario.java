

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletConsultaUsuario")
public class ServletConsultaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletConsultaUsuario() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		if(request.getParameter("sesion") != null) {
			request.setAttribute("mensaje", "Se ha ingresado correctamente el usuario en el sistema.");
	   		rd = request.getRequestDispatcher("/indexconSesion.jsp");
	   	 }else {
	   	request.setAttribute("mensaje", "Se ha ingresado correctamente el usuario en el sistema.");
	   	rd = request.getRequestDispatcher("/index.jsp");}
		rd.forward(request, response);
	}

}
