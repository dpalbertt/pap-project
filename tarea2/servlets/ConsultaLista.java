

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.DtLista;
import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;



@WebServlet("/ConsultaLista")
public class ConsultaLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ConsultaLista() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			WSControladorElementoService cps2 = new WSControladorElementoServiceLocator();
			WSControladorElemento port2 = cps2.getWSControladorElementoPort();
			String[] usuariosAux = port.listarUsuarios();
			List<String> usuarios = new ArrayList<String>();
			for (int i = 0; i < usuariosAux.length ; i++) {
				usuarios.add(usuariosAux[i]);
			}
			
			
			String usuario = "";
			String lista = request.getParameter("lista");
			DtLista dtLista = null;
			
			//busco la lista que seleccione
			for (String aux : usuarios) {
				DtLista dtL = port2.buscarListaDT(aux,lista);
				if (!dtL.getNombre().equals("")) {
					dtLista = dtL;
					usuario = aux;
					//ver una manera de salir de este for rapido
				}
					
			}
			
			if (dtLista.getNombre().equals("") || usuario.equals("")) {
				String sesion = (String) request.getSession().getAttribute("ul");
				String someMessage = "No encontre o la lista o el usuario, soy una verga";
				PrintWriter out = response.getWriter();
				if (sesion == null)
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
				else
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
			}
			else {
				String categoria;
				if (port2.buscarListaDT(usuario, lista).getCategoria() == null)
					categoria = "";
				else
					categoria = port2.buscarListaDT(usuario, lista).getCategoria();
					
				String[] categoriasAux = port.listarCategorias();
				List<String> listCategorias = new ArrayList<String>();
				for (int i = 0; i < categoriasAux.length; i++) {
					listCategorias.add(categoriasAux[i]);
				}
				
				RequestDispatcher rd;
				request.setAttribute("lista", dtLista);
				request.setAttribute("usuario", usuario);
				request.setAttribute("listCategorias", listCategorias);
				request.setAttribute("categoria", categoria);
				
				rd = request.getRequestDispatcher("/consultaListaFinal.jsp");
				rd.forward(request, response);
			}
		} catch(Exception e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}
		
		
	}

}
