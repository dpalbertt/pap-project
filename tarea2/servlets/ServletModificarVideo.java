import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletModificarVideo")
public class ServletModificarVideo extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain"); 
			
		try{
		   	WSControladorElementoService cps = new WSControladorElementoServiceLocator();
		   	WSControladorElemento port = cps.getWSControladorElementoPort();
	      String cambiarPriv = request.getParameter("cambiarpriv");
	      System.out.println(cambiarPriv);
	      Boolean esPrivado = true;
	      if(cambiarPriv.equals("No")){
	    	 esPrivado = false;
	      }  
	          	port.datosVideoMV(request.getParameter("nomUser"),request.getParameter("nomV"), Integer.valueOf(request.getParameter("duracion")),request.getParameter("url"), request.getParameter("descripcion"),esPrivado, request.getParameter("nomVid"));
		
	    		request.setAttribute("mensaje", "Se ha modificado correctamente el video ");   	     
				javax.servlet.RequestDispatcher rd;			
				rd = request.getRequestDispatcher("/indexconSesion.jsp");	   	 	
				rd.forward(request, response);
		}catch(Exception e) {
	   		e.printStackTrace();
	   	}
			
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");

   }
}