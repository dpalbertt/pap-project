

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.DtUsuario;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;

@WebServlet("/ServletModificarDatosUsuario")
public class ServletModificarDatosUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		boolean tipoCanal = true;
		if (request.getParameter("_tipoCanalNueva").equals("privado")) {
			tipoCanal = false;
		}
		String date = request.getParameter("_fechaNueva");
		Date date2 = ParseFecha(date);  
	
		try {
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			
			DtUsuario dtu	= new DtUsuario(date, date, date, date, date, cal, date, tipoCanal, tipoCanal, null);
			
			dtu.setNickname(request.getParameter("usuario"));
			dtu.setPassword(request.getParameter("passNueva1"));
			dtu.setApellido(request.getParameter("_apellidoNuevo"));
			dtu.setNombre(request.getParameter("_nombreNuevo"));
			dtu.setEmail(request.getParameter("_email"));
			dtu.setFNacimiento(cal);
			dtu.setFoto(request.getParameter("_imagenNueva"));
			dtu.setPublico(tipoCanal);
			//dtu.setHabilitado(true);

			port.modificarUsuario(dtu,
							request.getParameter("usuario"),
							request.getParameter("passNueva1"),
							request.getParameter("_nombreNuevo"),
							request.getParameter("_apellidoNuevo"),
							cal,
							request.getParameter("_imagenNueva"),
							tipoCanal,
							request.getParameter("_nombreCanalNueva"),
							request.getParameter("_descripcionNueva"),true);
		} catch (ServiceException e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}      
		RequestDispatcher rd;
		request.setAttribute("mensaje", "Se ha modificado correctamente el usuario en el sistema.");
		rd = request.getRequestDispatcher("/indexconSesion.jsp");
		rd.forward(request, response);
	}
	
	 public static Date ParseFecha(String fecha)
	    {
	        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	        Date fechaDate = null;
	        try {
	            fechaDate = formato.parse(fecha);
	        } 
	        catch (ParseException ex) 
	        {
	            System.out.println(ex);
	        }
	        return fechaDate;
	    }
}
	
	
	
	
	

