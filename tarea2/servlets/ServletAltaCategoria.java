import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;
import javax.persistence.*;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioServiceLocator;
/**
 * Servlet implementation class ServletAltaCategoria
 */
@WebServlet("/ServletAltaCategoria")
public class ServletAltaCategoria extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain");
	   
		try{
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			port.altaCategoria(request.getParameter("nombreCat"));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    //port.altaCategoria(request.getParameter("nombreCat"));
	   		
	   		
	   		
	     // response.getWriter().write("Hello World! Maven Web Project Example." + dtElemento.getNombre());    
	      //System.out.println("Hello World! Maven Web Project Example." + total);
	      javax.servlet.RequestDispatcher rd;
	      if(request.getSession().getAttribute("ul") != null) {
	    	  	request.setAttribute("mensaje", "Se ha ingresado correctamente la categoria en el sistema.");
		   		rd = request.getRequestDispatcher("/indexconSesion.jsp");
		   	 }else {
		   		request.setAttribute("mensaje", "Se ha ingresado correctamente la categoria en el sistema.");
		   		rd = request.getRequestDispatcher("/index.jsp");
		   		}
	      		rd.forward(request, response);
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");

      Enumeration<String> parameterNames = req.getParameterNames();
      
	  System.out.println("no tiene elementos");
      
      while (parameterNames.hasMoreElements()) {
    	  
    	  System.out.println("tiene elementos");
    	  
          String paramName = parameterNames.nextElement();
          System.out.println(paramName);
          System.out.println("n");

          String[] paramValues = req.getParameterValues(paramName);
          for (int i = 0; i < paramValues.length; i++) {
              String paramValue = paramValues[i];
              System.out.println("t" + paramValue);
              System.out.println("n");
          }

      }
      
      javax.servlet.RequestDispatcher rd;
      rd = req.getRequestDispatcher("/consultaVideo.jsp");
      rd.forward(req, resp);
            
   }
}