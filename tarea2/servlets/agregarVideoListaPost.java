

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;

@WebServlet("/agregarVideoListaPost")
public class agregarVideoListaPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public agregarVideoListaPost() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
     		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
     		WSControladorElementoService cps2 = new WSControladorElementoServiceLocator();
			WSControladorElemento port2 = cps2.getWSControladorElementoPort();
			List<String> usuarios = new ArrayList<String>();
			String[] usuariosReturn = port.listarUsuarios();
        	
        	for(int i=0; i<usuariosReturn.length ; i++){
        		usuarios.add(usuariosReturn[i]);
        	}
			String video = request.getParameter("video").toString();
			String lista = request.getParameter("lista").toString();
			
			String usuarioLista = (String) request.getSession().getAttribute("ul");
			String usuarioVideo = "";
			
			//busco al usuario del video
			for (String aux : usuarios) {
				List<String> videos = new ArrayList<String>();
				String[] videosAux = port2.listarVideoUsuario(aux);
				for(int i=0; i<videosAux.length ; i++){
	        		videos.add(videosAux[i]);
	        	}
				   if (videos.contains(video)) {
					   usuarioVideo = aux;
				   }
			}
			
			//esta condicion es por si no encuentra el usuario del video
			if (usuarioVideo.equals("")) {
				String sesion = (String) request.getSession().getAttribute("ul");
				String someMessage = "No encontre el usuario del video, soy una verga";
				PrintWriter out = response.getWriter();
				if (sesion == null)
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
				else
					out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
			}
			else {
				agregarVideoALista(usuarioVideo, usuarioLista, video, lista);
				String someMessage = "Se ha ingresado correctamente el video <" + video + "> en la lista la lista <" + lista + ">";
				PrintWriter out = response.getWriter();
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
			}
			
		} catch(Exception e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}
		
		
	}


		

	   public void agregarVideoALista(String usuarioVideo, String usuarioLista, String video, String lista) throws Exception {
			WSControladorElementoService cps = new WSControladorElementoServiceLocator();
			WSControladorElemento port = cps.getWSControladorElementoPort();
			port.agregarVideoALista(usuarioVideo, usuarioLista, video, lista);
		}
}