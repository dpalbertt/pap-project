import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;

/**
 * Servlet implementation class ServletAgregarVideoListaInicial
 */
@WebServlet("/ModificarListaSeleccion")
public class ModificarListaSeleccion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ModificarListaSeleccion() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
					
			WSControladorElementoService cps1 = new WSControladorElementoServiceLocator();
			WSControladorElemento port2 = cps1.getWSControladorElementoPort();
			String sesion = (String) request.getSession().getAttribute("ul");
			String lista = request.getParameter("lista");
			
			publicadores.DtLista dtl = port2.buscarListaDT(sesion, lista);
			
			request.setAttribute("lista", dtl.getNombre());
			request.setAttribute("usuario", sesion);
			String categoria;
			if (dtl.getCategoria() == null)
				categoria = "";
			else
				categoria = dtl.getCategoria();
			//si hay errores en 49, 54 y 56, no estas levantando la ultima version de la tarea uno, del webservice.
			request.setAttribute("categoria", categoria);
			String privacidad;
			if (dtl.isPublico())
				privacidad = "Publico";
			else
				privacidad = "Privado";
			
			request.setAttribute("listaPriv", privacidad);
			request.setAttribute("redireccion", "ok");
			
			RequestDispatcher rd;
			rd = request.getRequestDispatcher("/ModificarLista.jsp");
			rd.forward(request, response);
			
		} catch(Exception e) {
			String sesion = (String) request.getSession().getAttribute("ul");
			String someMessage = "Ha ocurrido un error y paso esto: <" + e.toString() + ">";
			PrintWriter out = response.getWriter();
			if (sesion == null)
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/index.jsp' \"></body></html>");
			else
				out.println("<html><head></head><body onload=\"alert('" + someMessage +"'); window.location='/uytubewebapp/indexconSesion.jsp' \"></body></html>");
		}
		
		

	}

}