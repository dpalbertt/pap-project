import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import javax.persistence.*;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletCerrarSesion")
public class ServletCerrarSesion extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain"); 
			String nick = request.getParameter("");
			request.getSession().setAttribute("ul",nick);
		   	 javax.servlet.RequestDispatcher rd;
		   	 System.out.println(nick);
		   	 rd = request.getRequestDispatcher("/index.jsp");
		   	 rd.forward(request, response);
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");
            
   }
}