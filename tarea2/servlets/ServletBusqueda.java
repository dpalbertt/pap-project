import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;


import publicadores.DtVideo;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuarioServiceLocator;
import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
 
@WebServlet("/ServletBusqueda")
public class ServletBusqueda extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	List<String> usuariosReturn;
	List<String> videosGlobalCondicion;
	List<String> listasGlobalCondicion;
	List<String> categoriasReturn;
	try {
		WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
			WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			WSControladorElementoService cps2 = new WSControladorElementoServiceLocator();
			WSControladorElemento port2 = cps2.getWSControladorElementoPort();
		   String condicion = request.getParameter("search");
		   String[] usuarios = port.listarUsuarios();
		   List <String> usuariosL = new ArrayList<String>();
			for(int i = 0; i < usuarios.length ; i++) {
				usuariosL.add(usuarios[i]);
			}
			
		   usuariosReturn = new ArrayList<String>();
		   
		   videosGlobalCondicion = new ArrayList<String>();
		   listasGlobalCondicion = new ArrayList<String>();
		   
		   for (String aux : usuarios) {		  
			   if (aux.contains(condicion)) {
				   usuariosReturn.add(aux);
			   }
			   publicadores.DtVideo[] videos = port2.listarVideos1(aux);
			   List<DtVideo> videosL = new ArrayList<DtVideo>();
				for(int i = 0; i < videos.length ; i++) {
					videosL.add(videos[i]);
				}
			   for (DtVideo nomVid : videosL) {
				   if (nomVid.getNombre().contains(condicion)) {
					   videosGlobalCondicion.add(nomVid.getNombre());
				   }
			   }
			   
			   String[] listasAux = port.listarListaDeUsuario(aux);
		   		List<String> listas = new ArrayList<String>();            	
		   		for (int i = 0; i< listasAux.length; i++ ){
		   			listas.add(listasAux[i]);
		   	}
			   for (String nomLista : listas) {
				   if (nomLista.contains(condicion)) {
					   listasGlobalCondicion.add(nomLista);
				   }
			   }
		   }
		   String[] categoriasAux = port.listarCategorias();
		   List<String> categorias= new ArrayList<String>();
		   for (int i = 0; i< categoriasAux.length; i++ ){
				categorias.add(categoriasAux[i]);
		}
		   categoriasReturn = new ArrayList<String>();
		   for (String cat : categorias) {
			   if (cat.contains(condicion)) {
				   categoriasReturn.add(cat);
			   }
		   }
		   RequestDispatcher rd;
		   request.setAttribute("usuarios", usuariosReturn);
		   request.setAttribute("videos", videosGlobalCondicion);
		   request.setAttribute("listas", listasGlobalCondicion);
		   request.setAttribute("categorias", categoriasReturn);
		   rd = request.getRequestDispatcher("/busqueda.jsp");
		   rd.forward(request, response);
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   

   		
	   
}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/plain");
            
   }
}