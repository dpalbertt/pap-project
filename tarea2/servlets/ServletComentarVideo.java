import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import publicadores.WSControladorElemento;
import publicadores.WSControladorElementoService;
import publicadores.WSControladorElementoServiceLocator;
import publicadores.WSControladorUsuarioService;
import publicadores.WSControladorUsuario;
import publicadores.WSControladorUsuarioServiceLocator;
/**
 * Servlet implementation class ServletVideos
 */
@WebServlet("/ServletComentarVideo")
public class ServletComentarVideo extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   response.setContentType("text/plain");			
		   try {
		   String nombreVideo = request.getParameter("nomVideo");
			String[] usuarios = listarUsuarios();
			String usuario = null;
			
			List <String> usuariosL = new ArrayList<String>();			
			for(int i = 0; i < usuarios.length ; i++) {
				usuariosL.add(usuarios[i]);
			}
			for (String aux : usuariosL) {		
		   			
		   			publicadores.DtVideo[] videos = listarVideos1(aux);			   			
		   			List<publicadores.DtVideo> videosL = new ArrayList<publicadores.DtVideo>();
		   			for(int i = 0; i < videos.length ; i++) {
						videosL.add(videos[i]);
					}
		   			
		   	for (publicadores.DtVideo auxv : videos) {
				 publicadores.DtVideo vid = auxv;
			   		if(vid.isPublico() == true){ 
			   		if(vid.getNombre().equals(nombreVideo)){
		     			usuario = aux;
		     		}
			   	
		     		}
	   }
	 		}			 
	 		System.out.print("el usuario duenio del video es: " + usuario + "el nombre del video es: " + nombreVideo+ "el usuario en sesion es: " + (String) request.getSession().getAttribute("ul"));
	 		String path = "[Videos, " + nombreVideo +"]";
	 		
	 			comentarVideo((String) request.getSession().getAttribute("ul"), usuario, request.getParameter("comentario"), path);    
	       		
	   	 javax.servlet.RequestDispatcher rd;
	   	if(request.getSession().getAttribute("ul") != null) {
	   		rd = request.getRequestDispatcher("/indexconSesion.jsp");
	   	 }else {
	   		 	rd = request.getRequestDispatcher("/index.jsp");
	   	 }
	   			rd.forward(request, response);
	   			
		   }catch(Exception e){
	 			e.printStackTrace();
		   }
	}
   public String[] listarUsuarios() throws Exception {
		WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
		return port.listarUsuarios();
	}
   public publicadores.Usuario findUsuarioByNickname(String nick) throws Exception {
		WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
		return port.findUsuarioByNickname(nick);
	}
   public publicadores.DtVideo[] listarVideos1(String nick) throws Exception {
		WSControladorElementoService cps = new WSControladorElementoServiceLocator();
		WSControladorElemento port = cps.getWSControladorElementoPort();
		return port.listarVideos1(nick);
	}
   public void comentarVideo(String nick, String usuario, String comentario, String path) throws Exception {
		WSControladorElementoService cps = new WSControladorElementoServiceLocator();
		WSControladorElemento port = cps.getWSControladorElementoPort();
		port.comentarVideo(nick, usuario, comentario, path);
	}
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      
            
   }
}