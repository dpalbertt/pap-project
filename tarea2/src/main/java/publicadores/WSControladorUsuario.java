/**
 * WSControladorUsuario.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface WSControladorUsuario extends java.rmi.Remote {
    public void crearUsuarioL(publicadores.DtUsuario arg0, publicadores.DtLista arg1) throws java.rmi.RemoteException;
    public void crearUsuario(publicadores.DtUsuario arg0) throws java.rmi.RemoteException;
    public void altaCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarCategorias() throws java.rmi.RemoteException;
    public publicadores.DtUsuario getUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.Usuario findUsuarioByNickname(java.lang.String arg0) throws java.rmi.RemoteException;
    public void seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void dejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String[] listarUsuarios() throws java.rmi.RemoteException;
    public java.lang.String[] listarUsuariosTodos() throws java.rmi.RemoteException;
    public java.lang.String[] listarUsuariosQueSigo(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] misSeguidores(java.lang.String arg0) throws java.rmi.RemoteException;
    public void modificarUsuario(publicadores.DtUsuario arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8, java.lang.String arg9, boolean arg10) throws java.rmi.RemoteException;
    public void bajaUsuario(publicadores.DtUsuario arg0, boolean arg1) throws java.rmi.RemoteException;
    public void datosUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8, java.lang.String arg9) throws java.rmi.RemoteException;
    public java.lang.String[] listarListaDeUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean existeUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean iniciarSesion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean soySeguidor(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
}
