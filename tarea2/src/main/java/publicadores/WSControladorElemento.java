/**
 * WSControladorElemento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface WSControladorElemento extends java.rmi.Remote {
    public void altaListaDf(java.lang.String arg0) throws java.rmi.RemoteException;
    public void crearLista(publicadores.DtLista arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void asignarCategoria(java.lang.String arg0, publicadores.DtLista arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public publicadores.DtUsuario seleccionarUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtVideo[] listarVideos(publicadores.DtUsuario arg0) throws java.rmi.RemoteException;
    public publicadores.DtVideo[] listarVideos1(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtLista[] listarListas(publicadores.DtUsuario arg0) throws java.rmi.RemoteException;
    public publicadores.DtLista seleccionarLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void agregarVideoALista(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public java.lang.String[] listarVideoUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarVideosLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void datosVideo(java.lang.String arg0, int arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException;
    public void altaVideo(publicadores.DtVideo arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public void comentarVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void modificarLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void quitarVideoLista(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public publicadores.Video[] getUserVideos(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtVideo[] getUserDtVideos(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.Exception;
    public void existeUser(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.Exception;
    public publicadores.DtVideo getUserVideo(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void valorarVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public java.lang.String[] listarCategorias() throws java.rmi.RemoteException;
    public void modificarVideo(publicadores.DtVideo arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public void datosVideoMV(java.lang.String arg0, java.lang.String arg1, int arg2, java.lang.String arg3, java.lang.String arg4, boolean arg5, java.lang.String arg6) throws java.rmi.RemoteException;
    public java.lang.String[] listarPorDefecto() throws java.rmi.RemoteException;
    public java.lang.String[] listarListasRepUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtLista buscarListaDT(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public publicadores.DtConsultaCat[] consultaCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
}
