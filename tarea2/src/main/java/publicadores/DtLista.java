/**
 * DtLista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class DtLista  extends publicadores.DtElemento  implements java.io.Serializable {
    private publicadores.DtVideo[] dtVideos;

    private publicadores.TipoLista tipo;

    private java.lang.String categoria;

    public DtLista() {
    }

    public DtLista(
           boolean publico,
           java.lang.String nombre,
           publicadores.DtVideo[] dtVideos,
           publicadores.TipoLista tipo,
           java.lang.String categoria) {
        super(
            publico,
            nombre);
        this.dtVideos = dtVideos;
        this.tipo = tipo;
        this.categoria = categoria;
    }


    /**
     * Gets the dtVideos value for this DtLista.
     * 
     * @return dtVideos
     */
    public publicadores.DtVideo[] getDtVideos() {
        return dtVideos;
    }


    /**
     * Sets the dtVideos value for this DtLista.
     * 
     * @param dtVideos
     */
    public void setDtVideos(publicadores.DtVideo[] dtVideos) {
        this.dtVideos = dtVideos;
    }

    public publicadores.DtVideo getDtVideos(int i) {
        return this.dtVideos[i];
    }

    public void setDtVideos(int i, publicadores.DtVideo _value) {
        this.dtVideos[i] = _value;
    }


    /**
     * Gets the tipo value for this DtLista.
     * 
     * @return tipo
     */
    public publicadores.TipoLista getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this DtLista.
     * 
     * @param tipo
     */
    public void setTipo(publicadores.TipoLista tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the categoria value for this DtLista.
     * 
     * @return categoria
     */
    public java.lang.String getCategoria() {
        return categoria;
    }


    /**
     * Sets the categoria value for this DtLista.
     * 
     * @param categoria
     */
    public void setCategoria(java.lang.String categoria) {
        this.categoria = categoria;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DtLista)) return false;
        DtLista other = (DtLista) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dtVideos==null && other.getDtVideos()==null) || 
             (this.dtVideos!=null &&
              java.util.Arrays.equals(this.dtVideos, other.getDtVideos()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.categoria==null && other.getCategoria()==null) || 
             (this.categoria!=null &&
              this.categoria.equals(other.getCategoria())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDtVideos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDtVideos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDtVideos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getCategoria() != null) {
            _hashCode += getCategoria().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DtLista.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "dtLista"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dtVideos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dtVideos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "dtVideo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "tipoLista"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
