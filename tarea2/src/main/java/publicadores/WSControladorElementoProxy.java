package publicadores;

public class WSControladorElementoProxy implements publicadores.WSControladorElemento {
  private String _endpoint = null;
  private publicadores.WSControladorElemento wSControladorElemento = null;
  
  public WSControladorElementoProxy() {
    _initWSControladorElementoProxy();
  }
  
  public WSControladorElementoProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSControladorElementoProxy();
  }
  
  private void _initWSControladorElementoProxy() {
    try {
      wSControladorElemento = (new publicadores.WSControladorElementoServiceLocator()).getWSControladorElementoPort();
      if (wSControladorElemento != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSControladorElemento)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSControladorElemento)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSControladorElemento != null)
      ((javax.xml.rpc.Stub)wSControladorElemento)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public publicadores.WSControladorElemento getWSControladorElemento() {
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento;
  }
  
  public void altaListaDf(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.altaListaDf(arg0);
  }
  
  public void crearLista(publicadores.DtLista arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.crearLista(arg0, arg1);
  }
  
  public void asignarCategoria(java.lang.String arg0, publicadores.DtLista arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.asignarCategoria(arg0, arg1, arg2);
  }
  
  public publicadores.DtUsuario seleccionarUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.seleccionarUsuario(arg0);
  }
  
  public publicadores.DtVideo[] listarVideos(publicadores.DtUsuario arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarVideos(arg0);
  }
  
  public publicadores.DtVideo[] listarVideos1(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarVideos1(arg0);
  }
  
  public publicadores.DtLista[] listarListas(publicadores.DtUsuario arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarListas(arg0);
  }
  
  public publicadores.DtLista seleccionarLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.seleccionarLista(arg0, arg1);
  }
  
  public void agregarVideoALista(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.agregarVideoALista(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String[] listarVideoUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarVideoUsuario(arg0);
  }
  
  public java.lang.String[] listarVideosLista(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarVideosLista(arg0, arg1);
  }
  
  public void datosVideo(java.lang.String arg0, int arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.datosVideo(arg0, arg1, arg2, arg3, arg4, arg5);
  }
  
  public void altaVideo(publicadores.DtVideo arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.altaVideo(arg0, arg1, arg2);
  }
  
  public void comentarVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.comentarVideo(arg0, arg1, arg2, arg3);
  }
  
  public void modificarLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.modificarLista(arg0, arg1, arg2, arg3);
  }
  
  public void quitarVideoLista(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.quitarVideoLista(arg0, arg1, arg2);
  }
  
  public publicadores.Video[] getUserVideos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.getUserVideos(arg0);
  }
  
  public publicadores.DtVideo[] getUserDtVideos(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.Exception{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.getUserDtVideos(arg0);
  }
  
  public void existeUser(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.Exception{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.existeUser(arg0);
  }
  
  public publicadores.DtVideo getUserVideo(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.getUserVideo(arg0, arg1);
  }
  
  public void valorarVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.valorarVideo(arg0, arg1, arg2);
  }
  
  public java.lang.String[] listarCategorias() throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarCategorias();
  }
  
  public void modificarVideo(publicadores.DtVideo arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.modificarVideo(arg0, arg1, arg2);
  }
  
  public void datosVideoMV(java.lang.String arg0, java.lang.String arg1, int arg2, java.lang.String arg3, java.lang.String arg4, boolean arg5, java.lang.String arg6) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    wSControladorElemento.datosVideoMV(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
  }
  
  public java.lang.String[] listarPorDefecto() throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarPorDefecto();
  }
  
  public java.lang.String[] listarListasRepUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.listarListasRepUsuario(arg0);
  }
  
  public publicadores.DtLista buscarListaDT(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.buscarListaDT(arg0, arg1);
  }
  
  public publicadores.DtConsultaCat[] consultaCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorElemento == null)
      _initWSControladorElementoProxy();
    return wSControladorElemento.consultaCategoria(arg0);
  }
  
  
}