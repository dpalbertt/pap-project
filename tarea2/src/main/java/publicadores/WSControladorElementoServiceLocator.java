/**
 * WSControladorElementoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class WSControladorElementoServiceLocator extends org.apache.axis.client.Service implements publicadores.WSControladorElementoService {

    public WSControladorElementoServiceLocator() {
    }


    public WSControladorElementoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSControladorElementoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSControladorElementoPort
    private java.lang.String WSControladorElementoPort_address = "http://localhost:1224/cElemento";

    public java.lang.String getWSControladorElementoPortAddress() {
        return WSControladorElementoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSControladorElementoPortWSDDServiceName = "WSControladorElementoPort";

    public java.lang.String getWSControladorElementoPortWSDDServiceName() {
        return WSControladorElementoPortWSDDServiceName;
    }

    public void setWSControladorElementoPortWSDDServiceName(java.lang.String name) {
        WSControladorElementoPortWSDDServiceName = name;
    }

    public publicadores.WSControladorElemento getWSControladorElementoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSControladorElementoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSControladorElementoPort(endpoint);
    }

    public publicadores.WSControladorElemento getWSControladorElementoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            publicadores.WSControladorElementoPortBindingStub _stub = new publicadores.WSControladorElementoPortBindingStub(portAddress, this);
            _stub.setPortName(getWSControladorElementoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSControladorElementoPortEndpointAddress(java.lang.String address) {
        WSControladorElementoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (publicadores.WSControladorElemento.class.isAssignableFrom(serviceEndpointInterface)) {
                publicadores.WSControladorElementoPortBindingStub _stub = new publicadores.WSControladorElementoPortBindingStub(new java.net.URL(WSControladorElementoPort_address), this);
                _stub.setPortName(getWSControladorElementoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSControladorElementoPort".equals(inputPortName)) {
            return getWSControladorElementoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://publicadores/", "WSControladorElementoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://publicadores/", "WSControladorElementoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSControladorElementoPort".equals(portName)) {
            setWSControladorElementoPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
