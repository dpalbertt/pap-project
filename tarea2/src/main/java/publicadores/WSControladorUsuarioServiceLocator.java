/**
 * WSControladorUsuarioServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class WSControladorUsuarioServiceLocator extends org.apache.axis.client.Service implements publicadores.WSControladorUsuarioService {

    public WSControladorUsuarioServiceLocator() {
    }


    public WSControladorUsuarioServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSControladorUsuarioServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSControladorUsuarioPort
    private java.lang.String WSControladorUsuarioPort_address = "http://localhost:1224/cUsuario";

    public java.lang.String getWSControladorUsuarioPortAddress() {
        return WSControladorUsuarioPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSControladorUsuarioPortWSDDServiceName = "WSControladorUsuarioPort";

    public java.lang.String getWSControladorUsuarioPortWSDDServiceName() {
        return WSControladorUsuarioPortWSDDServiceName;
    }

    public void setWSControladorUsuarioPortWSDDServiceName(java.lang.String name) {
        WSControladorUsuarioPortWSDDServiceName = name;
    }

    public publicadores.WSControladorUsuario getWSControladorUsuarioPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSControladorUsuarioPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSControladorUsuarioPort(endpoint);
    }

    public publicadores.WSControladorUsuario getWSControladorUsuarioPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            publicadores.WSControladorUsuarioPortBindingStub _stub = new publicadores.WSControladorUsuarioPortBindingStub(portAddress, this);
            _stub.setPortName(getWSControladorUsuarioPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSControladorUsuarioPortEndpointAddress(java.lang.String address) {
        WSControladorUsuarioPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (publicadores.WSControladorUsuario.class.isAssignableFrom(serviceEndpointInterface)) {
                publicadores.WSControladorUsuarioPortBindingStub _stub = new publicadores.WSControladorUsuarioPortBindingStub(new java.net.URL(WSControladorUsuarioPort_address), this);
                _stub.setPortName(getWSControladorUsuarioPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSControladorUsuarioPort".equals(inputPortName)) {
            return getWSControladorUsuarioPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://publicadores/", "WSControladorUsuarioService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://publicadores/", "WSControladorUsuarioPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSControladorUsuarioPort".equals(portName)) {
            setWSControladorUsuarioPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
