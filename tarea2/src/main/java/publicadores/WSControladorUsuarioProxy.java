package publicadores;

public class WSControladorUsuarioProxy implements publicadores.WSControladorUsuario {
  private String _endpoint = null;
  private publicadores.WSControladorUsuario wSControladorUsuario = null;
  
  public WSControladorUsuarioProxy() {
    _initWSControladorUsuarioProxy();
  }
  
  public WSControladorUsuarioProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSControladorUsuarioProxy();
  }
  
  private void _initWSControladorUsuarioProxy() {
    try {
      wSControladorUsuario = (new publicadores.WSControladorUsuarioServiceLocator()).getWSControladorUsuarioPort();
      if (wSControladorUsuario != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSControladorUsuario)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSControladorUsuario)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSControladorUsuario != null)
      ((javax.xml.rpc.Stub)wSControladorUsuario)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public publicadores.WSControladorUsuario getWSControladorUsuario() {
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario;
  }
  
  public void crearUsuarioL(publicadores.DtUsuario arg0, publicadores.DtLista arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.crearUsuarioL(arg0, arg1);
  }
  
  public void crearUsuario(publicadores.DtUsuario arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.crearUsuario(arg0);
  }
  
  public void altaCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.altaCategoria(arg0);
  }
  
  public java.lang.String[] listarCategorias() throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.listarCategorias();
  }
  
  public publicadores.DtUsuario getUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.getUsuario(arg0);
  }
  
  public publicadores.Usuario findUsuarioByNickname(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.findUsuarioByNickname(arg0);
  }
  
  public void seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.seguirUsuario(arg0, arg1);
  }
  
  public void dejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.dejarSeguirUsuario(arg0, arg1);
  }
  
  public java.lang.String[] listarUsuarios() throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.listarUsuarios();
  }
  
  public java.lang.String[] listarUsuariosTodos() throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.listarUsuariosTodos();
  }
  
  public java.lang.String[] listarUsuariosQueSigo(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.listarUsuariosQueSigo(arg0);
  }
  
  public java.lang.String[] misSeguidores(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.misSeguidores(arg0);
  }
  
  public void modificarUsuario(publicadores.DtUsuario arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8, java.lang.String arg9, boolean arg10) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.modificarUsuario(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
  }
  
  public void bajaUsuario(publicadores.DtUsuario arg0, boolean arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.bajaUsuario(arg0, arg1);
  }
  
  public void datosUsuario(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.util.Calendar arg5, java.lang.String arg6, boolean arg7, java.lang.String arg8, java.lang.String arg9) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    wSControladorUsuario.datosUsuario(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
  }
  
  public java.lang.String[] listarListaDeUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.listarListaDeUsuario(arg0);
  }
  
  public boolean existeUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.existeUsuario(arg0);
  }
  
  public boolean iniciarSesion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.iniciarSesion(arg0, arg1);
  }
  
  public boolean soySeguidor(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wSControladorUsuario == null)
      _initWSControladorUsuarioProxy();
    return wSControladorUsuario.soySeguidor(arg0, arg1);
  }
  
  
}