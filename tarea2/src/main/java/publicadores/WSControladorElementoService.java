/**
 * WSControladorElementoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface WSControladorElementoService extends javax.xml.rpc.Service {
    public java.lang.String getWSControladorElementoPortAddress();

    public publicadores.WSControladorElemento getWSControladorElementoPort() throws javax.xml.rpc.ServiceException;

    public publicadores.WSControladorElemento getWSControladorElementoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
