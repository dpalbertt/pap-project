<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="java.util.List" %>
	<%@page import="java.util.ArrayList" %>
	<%@page import= "publicadores.WSControladorElemento"%>
	<%@page import="publicadores.WSControladorElementoService"%>
	<%@page import="publicadores.DtUsuario"%>
	<%@page import="publicadores.DtLista"%>
	<%@page import= "publicadores.WSControladorElementoServiceLocator"%>
	<%@page import="publicadores.WSControladorUsuarioService"%>
	<%@page import="publicadores.WSControladorUsuario"%>
	<%@page import= "publicadores.WSControladorUsuarioServiceLocator"%>
	<%@page import= "publicadores.DtVideo"%>
	<%@page import="java.text.DateFormat" %>
	<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"/>
<title>UyTube | Consulta Usuario</title>
<% 	String u = (String) request.getSession().getAttribute("ul");
WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
WSControladorUsuario port = cps.getWSControladorUsuarioPort();
		
WSControladorElementoService cps1 = new WSControladorElementoServiceLocator();
WSControladorElemento port2 = cps1.getWSControladorElementoPort();
System.out.println(u); %>	
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>
	<% if(u == null){%>	
		<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="index.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50"  style="width: 122px; height: 64px"/></a>&nbsp;
	<button class="navbar-toggler" type="button" 
	        data-toggle="collapse"
		    data-target="#navbarSupportedContent" 
		    aria-controls="navbarSupportedContent" 
		    aria-expanded="false"
		    aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>	
	</div>
	
	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar">
		<button type="submit" class="btn btn-outline-primary">Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion</a>
	</form>
	</nav>
	<%}else{%>
	<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="indexconSesion.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50"  style="width: 122px; height: 64px"/></a>&nbsp;
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
		<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>	
<!--     			<li class="nav-item"><a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarCategoria.jsp">Listar Categorias</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarUsuario.jsp">Listar Usuarios</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="consultaCategoria.jsp">Consulta Categoria</a></li> -->
    	
	</div>


	

	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0" align="center">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar" >
		<button type="submit" class="btn btn-outline-primary" >Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=u%>&nbsp;&nbsp;

    
    <%       
    	publicadores.DtUsuario dtU = port.getUsuario(u);  
   		%>
   		
   		
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" 
		alt "Foto" width="50" height="50" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" 
		alt "Foto" width="50" height="50" class="rounded-circle" />    

		<%} %>	
	</form>
	</nav>	<%} %>
	

	<%
		String usr = request.getParameter("usuario");
	%>

	
	<h1>Consulta de Usuario</h1>

<form action="ServletConsultaUsuario" method="post"> 



<%       
   		publicadores.DtUsuario dtU = port.getUsuario(usr);  
   		%>

<br></br>

    <div class="form-row">
    <div class="col-md-4 mb-3" style="height: 64px; ">
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" alt "Foto" width="150" height="150" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" alt "Foto" width="150" height="150" class="img-circle" />    

		<%} %>
   	</div>
   	</div>

<br></br>
<br></br>

  <div class="form-row">
    <div class="col-md-4 mb-3" style="height: 64px; ">
      <label for="validationCustom01">Nickname</label>
      <input  type="text" name="_nickname" disabled class="form-control" id="validationCustom01" placeholder="ingrese un nickname"   value= "<%=dtU.getNickname()%>" >

    </div>
	<div class="col-md-4 mb-3">
      <label for="validationCustom02">Nombre</label>
      <input type="text" name="_nombre" disabled class="form-control" id="validationCustom02" value= "<%=dtU.getNombre()%>">

    </div>
  </div>
  
  

    <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido</label>
      <input type="text" name="_nombre" disabled class="form-control" id="validationCustom02" value= "<%=dtU.getApellido()%>">

    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Fecha de nacimiento</label>
      <input type="text" name="_apellido" disabled class="form-control" id="validationCustom02" value= <%=new SimpleDateFormat("dd/MM/yyyy").format(dtU.getFNacimiento().getTime())%>>

    </div>
     
   </div>





   


<p></p>
<p>Informacion del Canal</p>



    <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Nombre del Canal</label>
      <input type="text" name="_nombreCanal" disabled class="form-control" id="validationCustom02" placeholder="Ingrese nombre del canal" value= "<%=dtU.getCanal().getNombre()%>">
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Descripcion</label>
      <input type="text" name="_descripcion" disabled class="form-control" id="validationCustom02" placeholder="Ingrese descripcion del canal" value= "<%=dtU.getCanal().getDescripcion()%>">
    </div>
   </div>
   


<%
	boolean esPublico = dtU.getCanal().isPublico();
	if (esPublico == true){%>
<div class="form-row">
	<div class="col-md-4 mb-3">
        <label for="validationCustom02">Tipo de Canal</label>
		<input type="text" name="_tipoCanal" disabled class="form-control" id="validationCustom02"  value= "Publico" style="width: 143px; height: 32px">
	</div> 
</div> 
<% }else{%>
<div class="form-row">
	<div class="col-md-4 mb-3">
      	<label for="validationCustom02">Tipo de Canal</label>
		<input type="text" name="_tipoCanal" disabled class="form-control" id="validationCustom02"  value= "Privado" style="width: 143px; height: 32px">
	</div> 
</div> 
	<% 	
	}
	
%>



  <%
		String usuario = request.getParameter("usuario");
%>
      
  


<br>

</form>

<p>Videos publicos del usuario</p>
	<form action="consultaVideoInfoArbol.jsp" method="post">
	<input type="hidden" name="nomUsr" value="<%=usuario%>">
	<div class="form-row">
	<div class="form-group">	
		<select name="nomVideo" class="form-control" id="exampleFormControlSelect1" required size="5" style="width: 249px; ">
                <%	
                	publicadores.DtVideo[] cosa = port2.listarVideos(dtU);
                	
                
                	List<publicadores.DtVideo> vPublicos = new ArrayList<publicadores.DtVideo>();
                	for (int i = 0; i < cosa.length; i++){
                		vPublicos.add(cosa[i]);
                	}
                	
                	
              		for(DtVideo video: vPublicos){
              			if (video.isPublico()== true){%>
              			<option><%=video.getNombre()%></opcion>
              			<% }
                    	
                	}%>
        </select>
        </div>
        <div class="form-group">
        <p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Consultar Video</button></p>
        </div>
        </div>
     </form>
         
         
	 <p>Listas publicas del usuario</p>

		
	<form action="ConsultaLista" method="post">
	<input type="hidden" name="usuarioLista" value="<%=usuario%>">
		<div class="form-row">
		<div class="form-group">
			<select name="lista" class="form-control" id="exampleFormControlSelect1" required  size="5" style="width: 249px; ">
	            <%
	            	DtLista[] listaAux = port2.listarListas(dtU);
					List<DtLista> listas = new ArrayList<DtLista>();
					for (int i = 0; i< listaAux.length; i++){
						listas.add(listaAux[i]);
					}
					
	             	for(DtLista aux: listas){
	             		if (aux.isPublico()== true){%>
	             			<option><%=aux.getNombre()%></opcion>
	             		<% }
	                }%>
	        </select>
	   </div>
	   <div class="form-group">
        <p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Consultar Lista</button></p>
        </div>
		</div>
	</form>
	
	
	
	
	
	
	
	<%
	
	     if(usuario.equals(u)){
	%>
	
	
	
	
		 <p>Listas de mis seguidores</p>

		
	<form>
	<input type="hidden" name="usuarioLista" value="<%=usuario%>">
		<div class="form-row">
		<div class="form-group">
			<select name="lista" class="form-control" id="exampleFormControlSelect1" required  size="5" style="width: 249px; ">
	           	    	                <%
					String[] userAux = port.misSeguidores(usuario);		           	    	                
	            	List<String> misSeguidores = new ArrayList<String>();
	            	for (int i = 0; i < userAux.length; i++){
	            		misSeguidores.add(userAux[i]);
	            	}
	            	
	             	for(String aux: misSeguidores){%>
	             			<option><%=aux%></opcion>
	             		<% 
	                }%>
	        </select>
	   </div>
	   <div class="form-group">
        <p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Consultar Lista</button></p>
        </div>
		</div>
	</form>
	
	
	
	
	
	
	
			 <p>Usuarios que sigo</p>

		
	<form>
	<input type="hidden" name="usuarioLista" value="<%=usuario%>">
		<div class="form-row">
		<div class="form-group">
			<select name="lista" class="form-control" id="exampleFormControlSelect1" required  size="5" style="width: 249px; ">
	                          <%
	                String[] sigoAux = port.listarUsuariosQueSigo(usuario);
	            	List<String> UsuariosQueSigo = new ArrayList<String>();
	            	for (int i = 0; i < sigoAux.length; i++){
	            		UsuariosQueSigo.add(sigoAux[i]);
	            	}
	            	
	            	System.out.println("los usuarios que sigo son "+UsuariosQueSigo.size());
	             	for(String aux: UsuariosQueSigo){%>
	             			<option><%=aux%></opcion>
	             		<% 
	                }%>
	        </select>
	   </div>
	   <div class="form-group">
        <p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Consultar Lista</button></p>
        </div>
		</div>
	</form>
	
	
	
	<br><br>

<form action="modificarDatosUsuario.jsp" method="post">
	<input type="hidden" name="usuario" value="<%=usuario%>">
        <div class="form-group">
        <p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Modificar Datos Usuario</button></p>
        </div>
     </form>

<%}%>











<%if (u!=null){ %>

	
	<%
		if(port.soySeguidor(u, usuario)){ %>
		<form action="dejarSeguirUsuario.jsp" method="post">
			<input type="hidden" name="usuario" value="<%=usuario%>">
	   		<div class="form-group">
        	<p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Dejar de seguir usuario</button></p>
			</div>
		</form>
<%}else{ %>
		<form action="seguirUsuario.jsp" method="post">
			<input type="hidden" name=usuario value="<%=usuario%>">
	   		<div class="form-group">
        	<p>&nbsp; <button type="submit" class="btn btn-primary" style="height: 36px; ">Seguir usuario</button></p>
			</div>
		</form>
<%} %>
<%} %>



<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		
		




<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
					format: 'DD/MM/YYYY'
                });
            });
        </script>
        
        

<script type="text/javascript">
 function validarPasswd () {
   
  var p1 = document.getElementById("passNueva1").value;
  var p2 = document.getElementById("passNueva2").value;
  var espacios = false;
  var cont = 0;
  // Este bucle recorre la cadena para comprobar
  // que no todo son espacios
	while (!espacios && (cont < p1.length)) {
		if (p1.charAt(cont) == " ")
			espacios = true;
		cont++;
	}
   
  if (espacios) {
   alert ("La contraseña no puede contener espacios en blanco");
   return false;
  }
   
  if (p1.length == 0 || p2.length == 0) {
   alert("Los campos de la password no pueden quedar vacios");
   return false;
  }
   
  if (p1 != p2) {
   alert("Las passwords deben de coincidir");
   return false;
  } else {
// 		confirm("This is the default confirm!");
   return true; 
  }
 }
</script>