<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="java.util.List" %>
	<%@page import="java.util.ArrayList" %>
	<%@page import= "publicadores.WSControladorElemento"%>
	<%@page import="publicadores.WSControladorElementoService"%>
	<%@page import="publicadores.DtUsuario"%>
	<%@page import= "publicadores.WSControladorElementoServiceLocator"%>
	<%@page import="publicadores.WSControladorUsuarioService"%>
	<%@page import="publicadores.WSControladorUsuario"%>
	<%@page import= "publicadores.WSControladorUsuarioServiceLocator"%>
	<%@page import="java.text.DateFormat" %>
	<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"/>
<title>UyTube | Baja Usuario</title>
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="indexconSesion.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50"  style="width: 122px; height: 64px"/></a>&nbsp;
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>
<!--     			<li class="nav-item"><a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarCategoria.jsp">Listar Categorias</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarUsuario.jsp">Listar Usuarios</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="consultaCategoria.jsp">Consulta Categoria</a></li> -->
    	
	</div>

	<%	String u = (String) request.getSession().getAttribute("ul"); %>
	

	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0" align="center">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar" >
		<button type="submit" class="btn btn-outline-primary" >Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=u%>&nbsp;&nbsp;

    
    <%       
	    WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
   		publicadores.DtUsuario dtU = port.getUsuario(u);  
   		
   		WSControladorElementoService cps1 = new WSControladorElementoServiceLocator();
    	WSControladorElemento cElem = cps1.getWSControladorElementoPort();
   		%>
   		
   		
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" 
		alt "Foto" width="50" height="50" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" 
		alt "Foto" width="50" height="50" class="rounded-circle" />    

		<%} %>
		
		
	</form>

	</nav>	
	

	<%
		String usr = request.getParameter("usuario");
	%>
	

       
	
	

<form action="ServletBajaUsuario" method="post">



<input type="hidden" name="usuario" value="<%=u%>">






   	

 
 
    	<form name="form1" method="POST" onSubmit="return validarPasswd()" action="ServletBajaUsuario">
    	<div align="center">
    	<h1>Baja Usuario</h1>
    		<br><br><br><br><br><br>
      		<input type="password" name="password" class="form-control"required  placeholder= "Password" style="width: 200px; "><p><p>
      		<input type="password" name="confirmar" class="form-control"required  placeholder= "Confirmar password" style="width: 200px; "> <p>
      		<button type="submit" class="btn btn-primary" style="width: 200px; Onclick = "return validarPasswd()" onSubmit="return validarPasswd()">Confirmar baja Usuario</button>
		</div>
  	
		</form>
		

   	








<br></br>










</form>



<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		
		




<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
					format: 'DD/MM/YYYY'
                });
            });
        </script>
        
        

<script type="text/javascript">
 function validarPasswd () {
   
  var p1 = document.getElementById("passNueva1").value;
  var p2 = document.getElementById("passNueva2").value;
  var espacios = false;
  var cont = 0;
  // Este bucle recorre la cadena para comprobar
  // que no todo son espacios
	while (!espacios && (cont < p1.length)) {
		if (p1.charAt(cont) == " ")
			espacios = true;
		cont++;
	}
   
  if (espacios) {
   alert ("La contraseña no puede contener espacios en blanco");
   return false;
  }
   
  if (p1.length == 0 || p2.length == 0) {
   alert("Los campos de la password no pueden quedar vacios");
   return false;
  }
   
  if (p1 != p2) {
   alert("Las passwords deben de coincidir");
   return false;
  } else {
// 		confirm("This is the default confirm!");
   return true; 
  }
 }
</script>



 <link rel="stylesheet" type="text/css" href="CSS.css"> 
 <style type="text/css">
 #fichero{
	display: none;
 } 

 .circle{ 
	display: inline-block; 
	border-radius: 50%;
	width: 150px;
	height: 150px; 
	background-color: grey;
	cursor: pointer;

	}
	
	body {
	background-attachment: fixed !important;
}






<script>$(window).load(function(){

 $(function() {
  $('#file-input').change(function(e) {
      addImage(e); 
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
     }
    });
  });
  </script>