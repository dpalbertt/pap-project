<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="java.util.List" %>
	<%@page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"/>
	
	
	
	
<title>UyTube | Alta Usuario</title>
	<% 	String u = (String) request.getSession().getAttribute("ul");
System.out.println(u); %>	
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="index.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50"  style="width: 122px; height: 64px"/></a>&nbsp;
	<button class="navbar-toggler" type="button" 
	        data-toggle="collapse"
		    data-target="#navbarSupportedContent" 
		    aria-controls="navbarSupportedContent" 
		    aria-expanded="false"
		    aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>
	</div>
	
	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar">
		<button type="submit" class="btn btn-outline-primary">Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion</a>
	</form>
	</nav>
	
	
	<h1>Alta Usuario</h1>
	<p>Informacion del usuario</p>
<form action="ServletAltaUsuario" method="post">




   
<img id="imgSalida" width="100%" height="100%" 
src="https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" style="width: 200px; height: 200px; " class="img-circle"/>
<script>$('.file-upload').file_upload();</script>

   	



<br></br>

  <div class="form-row">
    <div class="col-md-4 mb-3" style="height: 64px; ">
      <label for="validationCustom01">Nickname</label>
      <input  type="text" name="_nickname" class="form-control" id="nick" placeholder="ingrese un nickname"  required>

    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Email</label>
      <input   type="text" name="_email" class="form-control" id="validationCustom02" placeholder="ingrese email" required>
    </div>
  </div>

    <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Nombre</label>
      <input type="text" name="_nombre" class="form-control" id="validationCustom02" placeholder="ingrese nombre" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido</label>
      <input type="text" name="_apellido" class="form-control" id="validationCustom02" placeholder="ingrese apellido" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
   </div>





<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Constraseña</label>
      <input type="password" name="clave1" class="form-control" name = "contraseña" id="passwd" placeholder="Password" required>
      

    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Confirmar contraseña</label>
      <input type="password"  class="form-control" name = "confirmar" id="passwd2" placeholder="Password" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
</div>
   


<div class="form-row">
	<div class="col-md-4 mb-3">
		<label for="validationCustom02">Fecha de nacimiento</label>
   		<div class='input-group date' id='datetimepicker3'>
       		<input type='text' name="_fecha" class="form-control" id="exampleInputPassword1" required>
        	<span class="input-group-addon">
           		<span class="glyphicon glyphicon-calendar"></span>
        	</span>
        	<div class="valid-feedback">
        	Looks good!
     	 	</div>
     	</div> 
   </div>  
   <div class="col-md-4 mb-3">
   <label for="exampleFormControlFile1">Imagen</label>
  		<div class="custom-file ">
  			<input type="file" name="_imagen" class="custom-file-input" id="file-input" lang="es">
  			
 			 <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
		</div>
		
   </div>
</div>






<p></p>
<p>Informacion del Canal</p>



    <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Nombre del Canal</label>
      <input type="text" name="_nombreCanal" class="form-control" id="validationCustom02" placeholder="Ingrese nombre del canal">
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Descripcion</label>
      <input type="text" name="_descripcion" class="form-control" id="validationCustom02" placeholder="Ingrese descripcion del canal" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
   </div>


  <div class="form-group">
    <label for="exampleFormControlSelect1">Tipo de Canal</label>
    <select name="_tipoCanal" class="form-control" id="exampleFormControlSelect1" style="width: 143px; height: 32px">
      <option value="publico">Publico</option>
      <option value="privado">Privado</option>
			</select>

    </select>
  </div>
  
<p></p>
<p></p>

 <div class="container">
    <label class="custom-file" id="customFile">
        <input type="file" class="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp">
        <span class="custom-file-control form-control-file"></span>
    </label>
</div>




<form name="form1" method="POST" onSubmit="return validarPasswd()" action="enviar.php">
<button type="submit" class="btn btn-primary" Onclick = "return validarPasswd()" onSubmit="return validarPasswd()">Confimar Alta Usuario</button>
<a class="btn btn-primary" href="index.jsp" role="button">Cancelar</a>
</form>
</form>





<%String nick2; %>











<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		
		




<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />



<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
					format: 'DD/MM/YYYY'
                });
            });
        </script>
        
        

<script type="text/javascript">
 function validarPasswd () {
   
  var p1 = document.getElementById("passwd").value;
  var p2 = document.getElementById("passwd2").value;
  var espacios = false;
  var cont = 0;
  // Este bucle recorre la cadena para comprobar
  // que no todo son espacios
	while (!espacios && (cont < p1.length)) {
		if (p1.charAt(cont) == " ")
			espacios = true;
		cont++;
	}
 if (nick1 ==nick2) {
			alert ("Usuario ya existe");
   return false;
   }
   
  if (espacios) {
   alert ("La contraseña no puede contener espacios en blanco");
   return false;
  }
   
  if (p1.length == 0 || p2.length == 0) {
   alert("Los campos de la password no pueden quedar vacios");
   return false;
  }
   
  if (p1 != p2) {
   alert("Las passwords deben de coincidir");
   return false;
  } else {
// 		confirm("This is the default confirm!");
   return true; 
  }
 }
</script>




<script>$(window).load(function(){

 $(function() {
  $('#file-input').change(function(e) {
      addImage(e); 
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
     }
    });
  });
  </script>
  
 


<script>
/* show file value after file select */
$('.custom-file-input').on('change',function(){
  var fileName = document.getElementById("exampleInputFile").files[0].name;
  $(this).next('.form-control-file').addClass("selected").html(fileName); })
</script>
  
<label class="custom-file">
    <input type="file" id="Image" class="custom-file-input">
    <span class="custom-file-control"></span>
</label>




</body>
</html>