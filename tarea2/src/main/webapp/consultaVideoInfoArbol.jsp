<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import= "publicadores.WSControladorElemento"%>
<%@page import="publicadores.WSControladorElementoService"%>
<%@page import= "publicadores.WSControladorElementoServiceLocator"%>
<%@page import="publicadores.WSControladorUsuarioService"%>
<%@page import="publicadores.WSControladorUsuario"%>
<%@page import= "publicadores.WSControladorUsuarioServiceLocator"%>
<%@page import="publicadores.DtVideo" %>
<%@page import="publicadores.DtComentario" %>
<%@page import="publicadores.DtUsuario" %>
<%@page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" 
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<<title>UyTube | Consulta de Video</title>
<% 	String u = (String) request.getSession().getAttribute("ul");
WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
WSControladorUsuario port = cps.getWSControladorUsuarioPort();
		
WSControladorElementoService cps1 = new WSControladorElementoServiceLocator();
WSControladorElemento port2 = cps1.getWSControladorElementoPort();
System.out.println(u); %>	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>
		<% if(u == null){%>	
		<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="index.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50" style="width: 122px; height: 64px"/>&nbsp;</a>
	<button class="navbar-toggler" type="button" 
	        data-toggle="collapse"
		    data-target="#navbarSupportedContent" 
		    aria-controls="navbarSupportedContent" 
		    aria-expanded="false"
		    aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>
	</div>
	
	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar">
		<button type="submit" class="btn btn-outline-primary">Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion</a>
	</form>
	</nav>
	<%}else{%>
	<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
		<a href="indexconSesion.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50" style="width: 122px; height: 64px"/>&nbsp;</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
		<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>	
<!--     			<li class="nav-item"><a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarCategoria.jsp">Listar Categorias</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarUsuario.jsp">Listar Usuarios</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="consultaCategoria.jsp">Consulta Categoria</a></li> -->
    	
	</div>


	

	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0" align="center">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar" >
		<button type="submit" class="btn btn-outline-primary" >Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=u%>&nbsp;&nbsp;

    
    <%       
   		DtUsuario dtU = port.getUsuario(u);  
   		%>
   		
   		
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" 
		alt "Foto" width="50" height="50" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" 
		alt "Foto" width="50" height="50" class="rounded-circle" />    

		<%} %>	
	</form>
	</nav>	<%} %>
	
	
	<% String nombreVideo = request.getParameter("nomVideo"); %>
	<% String htmlArbol = "<div id=\"jstree\">"; %>
	<% List<DtVideo> videosUsuario = new ArrayList<DtVideo>(); %>
	
	<h1>Consulta de Video</h1>
	
	
	<form action="consultaVideo.jsp" method="post">
		<input type="hidden" name="nomVideo" id="nomVideo" value="<%=nombreVideo%>">	
		    <% 
		    	try {		    		
            	 	List<DtVideo> videos = new ArrayList<DtVideo>();
            	 	
            		List<DtVideo> videosGlobal = new ArrayList<DtVideo>();
            		
                	String[] usuariosAux = port.listarUsuarios();
                	List<String> usuarios = new ArrayList<String>();
                	for (int i = 0; i < usuariosAux.length ; i++){
                		usuarios.add(usuariosAux[i]);
                	}
                	
               		for (String aux : usuarios) {
               			DtVideo[] videoosAux = port2.listarVideos1(aux);
         		   		List<DtVideo> videoos = new ArrayList<DtVideo>();
         		   		for (int j = 0; j < videoosAux.length; j++){
         		   			videoos.add(videoosAux[j]);
         		   		}
         		   		
	         		   	for (DtVideo auxv : videoos) {
	         				 DtVideo vid = auxv;
	         			   	if((vid.isPublico() == true ) ){ 
	         				 videosGlobal.add(auxv);
	         		   		}else if(aux.equals((String) request.getSession().getAttribute("ul"))){
	         			   		videosGlobal.add(auxv);
	         		   		}
         	   			}               		
              		 }
               			videos = videosGlobal;            	           
               			boolean encontreVideo = false; 
                  		int i = 0;
                  		while(i<videos.size() && !encontreVideo) {                		          			
              			DtVideo dtVideo = videos.get(i);
              			if(dtVideo.getNombre().equals(nombreVideo)) {
    						System.out.println("El nombre del video es "+nombreVideo);
    						System.out.println("El nombre del dtvideo es "+dtVideo.getNombre());
              				encontreVideo = true;
              		  		out.println("<b>Nombre: </b>"+dtVideo.getNombre()+"<br>");
                       		out.println("<b>URL: </b>"+dtVideo.getUrl()+"<br>");
                       		out.println("<b>Descripcion: </b>"+dtVideo.getDescripcion()+"<br>");
                       		out.println("<b>Duracion: </b>"+dtVideo.getDuracion()+"<br>");
                       		out.println("<b>Fecha de publicacion: </b>"+new SimpleDateFormat("dd/MM/yyyy").format(dtVideo.getFechaPublicacion().getTime())+"<br>");%>
                       		<!-- Grid row -->
                       		<div class="row">

                       		  <!-- Grid column -->
                       		  <div class="col-lg-4 col-md-12 mb-4">

                       		    <!--Modal: Name-->
                       		    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       		      <div class="modal-dialog modal-lg" role="document">

                       		        <!--Content-->
                       		        <div class="modal-content">

                       		          <!--Body-->
                       		          <div class="modal-body mb-0 p-0">

                       		            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                       		              <iframe class="embed-responsive-item" src="<%=dtVideo.getUrl() %>"
                       		                allowfullscreen></iframe>
                       		            </div>

                       		          </div>

                       		          <!--Footer-->
                       		          <div class="modal-footer justify-content-center">                       		          
                       		            <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                       		            <!--Twitter-->
                       		            <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                       		            <!--Google +-->
                       		            <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                       		            <!--Linkedin-->
                       		            <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                       		            <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                       		          </div>

                       		        </div>
                       		        <!--/.Content-->

                       		      </div>
                       		    </div>
                       		    <!--Modal: Name-->

                       		    <a><img class="img-fluid z-depth-1" src=https://cdn.icon-icons.com/icons2/195/PNG/128/YouTube_23392.png
                       		    data-toggle="modal" data-target="#modal1"></a>

                       		  </div>
                       		  <!-- Grid column -->

                       		  <!-- Grid column -->
                       		  <div class="col-lg-4 col-md-6 mb-4">

                       		    <!--Modal: Name-->
                       		    <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       		      <div class="modal-dialog modal-lg" role="document">

              			<%}
              			i++;              	   		
              			}                  		
              		}catch(Exception e){
               	out.print("Error lanzado en consultaVideo.jsp linea 53 "+e.toString());
               }

            %>
        

	</form>

   

  

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <!-- 5 include the minified jstree source -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/jstree.min.js" integrity="sha256-NPMXVob2cv6rH/kKUuzV2yXKAQIFUzRw+vJBq4CLi2E=" crossorigin="anonymous"></script>
  <script>
  $(function () {
    $('#jstree').jstree();
    $('#jstree').on("changed.jstree", function (e, data) {
        console.log(" la data es ");
    	console.log(data);
      $("#nomVideo").val(data.node.text);
    });
  });
  </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

		
</body>
</html>