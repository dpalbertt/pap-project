<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import= "publicadores.WSControladorElemento"%>
<%@page import="publicadores.WSControladorElementoService"%>
<%@page import="publicadores.DtUsuario"%>
<%@page import="publicadores.DtConsultaCat"%>
<%@page import= "publicadores.WSControladorElementoServiceLocator"%>
<%@page import="publicadores.WSControladorUsuarioService"%>
<%@page import="publicadores.WSControladorUsuario"%>
<%@page import= "publicadores.WSControladorUsuarioServiceLocator"%>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"/>
<title>UyTube | Consulta Categoria</title>
<% 	String u = (String) request.getSession().getAttribute("ul");
System.out.println(u); %>	
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>
		<% if(u == null){%>	
		<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<a href="index.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50" style="width: 122px; height: 64px"/>&nbsp;</a>
	<button class="navbar-toggler" type="button" 
	        data-toggle="collapse"
		    data-target="#navbarSupportedContent" 
		    aria-controls="navbarSupportedContent" 
		    aria-expanded="false"
		    aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>
	</div>
	
	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar">
		<button type="submit" class="btn btn-outline-primary">Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion</a>
	</form>
	</nav>
	<%}else{%>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a href="indexconSesion.jsp"><img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50" style="width: 122px; height: 64px"/>&nbsp;</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
		<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>	
<!--     			<li class="nav-item"><a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarCategoria.jsp">Listar Categorias</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarUsuario.jsp">Listar Usuarios</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="consultaCategoria.jsp">Consulta Categoria</a></li> -->
    	
	</div>


	

	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0" align="center">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar" >
		<button type="submit" class="btn btn-outline-primary" >Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=u%>&nbsp;&nbsp;

    
    <%       
    publicadores.DtUsuario dtU = null;
   	try{ 
   		WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
   		WSControladorUsuario port = cps.getWSControladorUsuarioPort();
   			dtU = port.getUsuario(u); 
   	}catch (Exception e){
   		e.printStackTrace();
   	}  
   		%>
   		
   		
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" 
		alt "Foto" width="50" height="50" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" 
		alt "Foto" width="50" height="50" class="rounded-circle" />    

		<%} %>	
	</form>
	</nav>	<%} %>

 
 	
	
		<% WSControladorElementoService cps = new WSControladorElementoServiceLocator();
		WSControladorElemento port = cps.getWSControladorElementoPort();
        List <DtConsultaCat> consultacat = new ArrayList<DtConsultaCat>();
        DtConsultaCat[] consultaCat = port.consultaCategoria(request.getParameter("categoria"));
        for (int i = 0; i< consultaCat.length; i++ ){
    		consultacat.add(consultaCat[i]);
    	}
        %>
        <h1>Consulta categoria</h1>
        <form>
	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Video o Lista</th>
			</tr>
		</thead>
		<tbody>
			<%
				int i = 1;
					for (DtConsultaCat dts : consultacat) {
			%>
			<tr>
				<th scope="row"><%=i%></th>
				<td><%=dts.getNombreUsuario()%></td>
				<td><%=dts.getVideoOlista()%></td>
			</tr>
			<%
				i++;}
			%>
		</tbody>
	</table>
 			
 			

</form>		
</body>
</html>