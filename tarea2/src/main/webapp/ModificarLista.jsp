<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="publicadores.DtLista" %>
	<%@page import="java.util.List" %>
	<%@page import="java.util.ArrayList" %>
	<%@page import= "publicadores.WSControladorElemento"%>
	<%@page import="publicadores.WSControladorElementoService"%>
	<%@page import="publicadores.DtUsuario"%>
	<%@page import= "publicadores.WSControladorElementoServiceLocator"%>
	<%@page import="publicadores.WSControladorUsuarioService"%>
	<%@page import="publicadores.WSControladorUsuario"%>
	<%@page import= "publicadores.WSControladorUsuarioServiceLocator"%>
	<%@page import="publicadores.DtUsuario" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"/>
<title>uytube | Consulta Lista de Reproduccion</title>
<style>
@media only screen and (max-width: 768px) {
  .desktop-item {
    display:none;
  }
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
	<img src = "Multimedia\uytube2.png" 
		alt "Foto" width="50" height="50"  style="width: 122px; height: 64px"/>&nbsp;
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		
				<ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="modificarDatosUsuario.jsp">Modificar Datos Usuario</a>
		  <a class="dropdown-item" href="consultaUsuario.jsp">Consulta de Usuario</a>
		  <a class="dropdown-item" href="bajaUsuario.jsp">Baja Usuario</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Video
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaVideo.jsp">Consulta de Video</a>
          <a class="dropdown-item desktop-item" href="altaVideo.jsp">Alta de Video</a>
          <a class="dropdown-item desktop-item" href="modificarVideo.jsp">Modificar Video</a>
          <a class="dropdown-item desktop-item" href="comentarVideo.jsp">Comentar  Video</a>
          <a class="dropdown-item desktop-item" href="valorarVideo.jsp">Valorar Video</a>
          <a class="dropdown-item desktop-item" href="agregarVideoLista.jsp">Agregar video a lista</a>
          <a class="dropdown-item desktop-item" href="quitarVideoLista.jsp">Quitar video de lista</a>
        </div>
      </li>
      <li class="nav-item dropdown active desktop-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="altaCategoria.jsp">Alta Categoria</a>
          <a class="dropdown-item" href="consultaCategoria.jsp">Consulta Categoria</a>
		  <a class="dropdown-item" href="listarCategoria.jsp">Listar Categorias</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lista de Reproduccion
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="consultaLista.jsp">Consulta Lista</a>
          <a class="dropdown-item desktop-item" href="CrearLista.jsp">Crear lista</a>
          <a class="dropdown-item desktop-item" href="ModificarListaInit.jsp">Modificar lista</a>
        </div>
      </li>
      <li class="nav-item" active>
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a>
      </li>
    </ul>
<!--     			<li class="nav-item"><a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesion</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarCategoria.jsp">Listar Categorias</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="listarUsuario.jsp">Listar Usuarios</a></li> -->
<!-- 			<li class="nav-item"><a class="nav-link" href="consultaCategoria.jsp">Consulta Categoria</a></li> -->
    	
	</div>

	<%	String u = (String) request.getSession().getAttribute("ul");
	WSControladorUsuarioService cps = new WSControladorUsuarioServiceLocator();
	WSControladorUsuario port = cps.getWSControladorUsuarioPort();
			
	WSControladorElementoService cps1 = new WSControladorElementoServiceLocator();
	WSControladorElemento port2 = cps1.getWSControladorElementoPort();
	%>
	

	<form action="ServletBusqueda" method="post" class="form-inline my-2 my-lg-0" align="center">
		<input class="form-control mr-sm-2" type="text" name="search" placeholder="Buscar" aria-label="Buscar" >
		<button type="submit" class="btn btn-outline-primary" >Buscar</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=u%>&nbsp;&nbsp;

    
    <%       
   		publicadores.DtUsuario dtU = port.getUsuario(u);  
   		%>
   		
   		
	<%if (dtU.getFoto().equals("")){ %>
		<img src = "https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilustr.jpg?ver=6" 
		alt "Foto" width="50" height="50" class="img-circle" /> 

		<%}else{ %>
		<img src = "<%=dtU.getFoto()%>" 
		alt "Foto" width="50" height="50" class="rounded-circle" />    

		<%} %>	
	</form>
	</nav>
	
	<% 
		String redireccion = (String) request.getAttribute("redireccion");
		//si fue llamado desde el index, tiene un attributo redireccion, entonces se usa getAttribute
		String nombre;
		String usuario;
		String categoria;
		String privacidad;
		if (redireccion != null){
			nombre = (String) request.getAttribute("lista");
			usuario = (String) request.getAttribute("usuario");
			categoria = (String) request.getAttribute("categoria");
			privacidad = (String) request.getAttribute("listaPriv");
		}
		else { //este else es por si llamo a la pagina desde el consulta lista
			nombre = request.getParameter("lista");
			usuario = request.getParameter("usuario");
			categoria = request.getParameter("categoria");
			privacidad = request.getParameter("listaPriv");
		}
		
	%>
	
	<h1>Modificar Lista de Reproduccion</h1>
	
	<form action="ModificarLista" method="post">
		<div class="form-group">
			<label for="lblLista">Nombre Lista: <span><%=nombre%></span></label>
			<input type="hidden" name="lista" value="<%=nombre%>">
			<br>
			
			<label for="lblPrivacidad">¿Publica?</label>
			<input id="check" type="checkbox" name="listaPub" value="true" <% if (privacidad.equals("Publico")) { %> checked <% } %>>
			
			<input type="hidden" name="usuario" value="<%=usuario%>">	
			<input type="hidden" name ="categoriaVieja" value ="<%= categoria %>" >
			
			<select disabled name="categoria" class="custom-select" id="inputGroupSelect01" value="">
	            <%
	            String[] categoriasL =port.listarCategorias();
	            List <String> categorias = new ArrayList<String>();
	      		for(int i = 0; i < categoriasL.length ; i++){
	      			categorias.add(categoriasL[i]);
	      		}
 
	                out.println("<option disabled>"+"Seleccione una categoria..."+"</option>");
	             	for(String aux: categorias){
	                   	out.println("<option value=\"" + aux + "\"" + ">" + aux + "</option>");
	               	}
	            %>
	        </select>
			
	   </div>
        <button type="submit" class="btn btn-primary">Modificar Lista</button>
	</form>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>