import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import interfaces.Fabrica;
import interfaces.IUsuario;
/**
 * Servlet implementation class SimpleServlet
 */
@WebServlet("/SimpleServlet")
public class SimpleServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
   
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("text/plain");
	      Fabrica f = Fabrica.getInstancia();
	      IUsuario iu = f.getIControladorUsuario();
	      List<String> usrs = iu.listarUsuarios();
	      String total = null;
	      for(String aux: usrs) {
	          total = total +aux;
	      }
	      response.getWriter().write("Hello World! Maven Web Project Example." + total);     
	      javax.servlet.RequestDispatcher rd;
	      rd = request.getRequestDispatcher("/index.jsp");
	      rd.forward(request, response);
	}
   
   
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
         throws ServletException, IOException {
//      resp.setContentType("text/plain");
//      Fabrica f = Fabrica.getInstancia();
//      IUsuario iu = f.getIControladorUsuario();
//      List<String> usrs = iu.listarUsuarios();
//      String total;
//      for(String aux: usrs) {
//          total=total+aux;
//      }
//      resp.getWriter().write("Hello World! Maven Web Project Example." + total);     
//      RequestDispatcher rd;
//      rd = request.getRequestDispatcher("/index.jsp");
//      rd.forward(request, response);
            
   }
}