package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datatypes.DtLista;
import datatypes.DtVideo;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ModificarVideo extends JFrame {

	private JPanel contentPane;
	private JTextField nomAntV;
	private JTextField duracion;
	private JTextField nuevoNom;
	private JTextField nuevaDur;
	private JTextField url;
	private JTextField nuevaUrl;
	private JTextField descripcion;
	private JTextField nuevaDesc;
	private JTextField privacidad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarVideo frame = new ModificarVideo(null,null,null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarVideo(Fabrica f, String nickDef,String nombreVideoDef) {
		IElemento econ = f.getControladorElemento();
		IUsuario ucon = f.getIControladorUsuario();
		setTitle("Modificar Datos de Video");
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		List<String> users = ucon.listarUsuarios();
		JComboBox usuarios = new JComboBox(users.toArray());
		usuarios.setBounds(59, 10, 108, 22);
		contentPane.add(usuarios);
		
		JComboBox videos = new JComboBox();
		videos.setBounds(247, 10, 108, 22);
		contentPane.add(videos);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(0, 13, 56, 16);
		contentPane.add(lblUsuario);
		
		JLabel lblVideos = new JLabel("Videos:");
		lblVideos.setBounds(195, 13, 56, 16);
		contentPane.add(lblVideos);
		
		
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(0, 45, 56, 16);
		contentPane.add(lblNombre);
		
		JLabel lblNuevoNombre = new JLabel("Nuevo nombre:");
		lblNuevoNombre.setBounds(192, 45, 88, 16);
		contentPane.add(lblNuevoNombre);
		
		nomAntV = new JTextField();
		nomAntV.setEnabled(false);
		nomAntV.setBounds(51, 42, 116, 22);
		contentPane.add(nomAntV);
		nomAntV.setColumns(10);
		
		JLabel lblDuracion = new JLabel("Duracion:");
		lblDuracion.setBounds(0, 74, 56, 16);
		contentPane.add(lblDuracion);
		
		duracion = new JTextField();
		duracion.setEnabled(false);
		duracion.setBounds(61, 71, 46, 22);
		contentPane.add(duracion);
		duracion.setColumns(10);
		
		nuevoNom = new JTextField();
		nuevoNom.setBounds(292, 42, 116, 22);
		contentPane.add(nuevoNom);
		nuevoNom.setColumns(10);
		
		JLabel lblNuevaDuracion = new JLabel("Nueva duracion:");
		lblNuevaDuracion.setBounds(192, 74, 102, 16);
		contentPane.add(lblNuevaDuracion);
		
		nuevaDur = new JTextField();		
		nuevaDur.setBounds(302, 71, 46, 22);
		contentPane.add(nuevaDur);
		nuevaDur.setColumns(10);
		
		JLabel lblUrl = new JLabel("Url:");
		lblUrl.setBounds(0, 103, 29, 16);
		contentPane.add(lblUrl);
		
		url = new JTextField();
		url.setEnabled(false);
		url.setBounds(24, 103, 116, 22);
		contentPane.add(url);
		url.setColumns(10);
		
		JLabel lblNuevaUrl = new JLabel("Nueva url:");
		lblNuevaUrl.setBounds(192, 103, 74, 16);
		contentPane.add(lblNuevaUrl);
		
		nuevaUrl = new JTextField();
		nuevaUrl.setBounds(278, 100, 116, 22);
		contentPane.add(nuevaUrl);
		nuevaUrl.setColumns(10);
		
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(0, 138, 74, 16);
		contentPane.add(lblDescripcion);
		
		descripcion = new JTextField();
		descripcion.setEnabled(false);
		descripcion.setBounds(72, 138, 96, 50);
		contentPane.add(descripcion);
		descripcion.setColumns(10);
		
		JLabel lblNueva = new JLabel("Nueva descripcion:");
		lblNueva.setBounds(192, 132, 116, 16);
		contentPane.add(lblNueva);
		
		nuevaDesc = new JTextField();
		nuevaDesc.setBounds(312, 138, 96, 50);
		contentPane.add(nuevaDesc);
		nuevaDesc.setColumns(10);
		
		JRadioButton rdbtnCambiarPrivacidad = new JRadioButton("Cambiar privacidad");
		rdbtnCambiarPrivacidad.setBounds(192, 197, 145, 25);
		contentPane.add(rdbtnCambiarPrivacidad);
		
		JLabel lblPrivacidad = new JLabel("Privacidad:");
		lblPrivacidad.setBounds(0, 197, 63, 16);
		contentPane.add(lblPrivacidad);
		
		privacidad = new JTextField();
		privacidad.setEnabled(false);
		privacidad.setBounds(65, 198, 102, 22);
		contentPane.add(privacidad);
		privacidad.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");		
		btnAceptar.setBounds(72, 228, 97, 25);
		contentPane.add(btnAceptar);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setBounds(212, 228, 97, 25);
		contentPane.add(btnSalir);
		
		
		usuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nick = usuarios.getSelectedItem().toString();
				List<String> listas = ucon.findUsuarioByNickname(nick).getCanal().listarVideos();
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				for (String aux : listas) {
					modelo.addElement(aux);
				}
				videos.setModel(modelo);
				/* agregue todos los elementos al combo box */
			}
		});
		
		videos.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
				String nick = usuarios.getSelectedItem().toString();
				String nomVid = videos.getSelectedItem().toString();
			
			
			DtVideo dtV = ucon.findUsuarioByNickname(nick).getCanal().buscarVideoDt(nomVid);
			nomAntV.setText(dtV.getNombre());	
			url.setText(dtV.getUrl());
			descripcion.setText(dtV.getDescripcion());
			String privacidadV;
			if (dtV.isPublico()) {
				privacidadV = "Publica";
			}
			else {
				privacidadV = "Privada";
			}
			privacidad.setText(privacidadV);
			duracion.setText(String.valueOf(dtV.getDuracion()));
			
		}
		
		});
		nuevaDur.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				 try {
					 //econ.esNumero(duracion.getText());
					  }catch(Exception exc) {
					  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				  }

			}
		});
		
		
		if (nickDef != null) {
			if (!nickDef.equals("")) {
				usuarios.setSelectedItem(nickDef);
				videos.setSelectedItem(nombreVideoDef);
				usuarios.setEnabled(false);
				videos.setEnabled(false);
			}
		}
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					if(nuevoNom.getText().equals("") || nuevaDur.getText().equals("") || nuevaUrl.getText().equals("") || nuevaDesc.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Ingrese los datos que faltan");
					}else {	
						econ.datosVideoMV(usuarios.getSelectedItem().toString(),nomAntV.getText(),Integer.valueOf(nuevaDur.getText()),nuevaUrl.getText(),nuevaDesc.getText(),rdbtnCambiarPrivacidad.isSelected(),nuevoNom.getText());
						JOptionPane.showMessageDialog(null, "Se modifico el video correctamente");
						dispose();
					}
			}
		});
	}
}
	

