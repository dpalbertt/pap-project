package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.ManejadorUsuario;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class VAgregarVideoLista extends JFrame {

	private JPanel contentPane;

	//Para ejecutar la ventana sola
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Fabrica f = Fabrica.getInstancia();
	 * IUsuario iu = f.getIControlador(); IElemento ie = f.getControladorElemento();
	 * VAgregarVideoLista frame = new VAgregarVideoLista(ie, iu);
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public VAgregarVideoLista(Fabrica f) {
		IElemento ie = f.getControladorElemento();
		IUsuario iu = f.getIControladorUsuario();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 546, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(30, 45, 66, 15);
		panel.add(lblUsuario);
		
		List<String> usuarios = iu.listarUsuarios();
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
		comboBoxUsuario.setBounds(105, 40, 114, 24);
		panel.add(comboBoxUsuario);
		
		JLabel lblVideo = new JLabel("Video: ");
		lblVideo.setBounds(30, 90, 66, 15);
		panel.add(lblVideo);
		
		JComboBox comboBoxVideo = new JComboBox();
		comboBoxVideo.setBounds(105, 85, 114, 24);
		panel.add(comboBoxVideo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(30, 132, 471, 2);
		panel.add(separator);
		
		JLabel lblUsuario_1 = new JLabel("Usuario: ");
		lblUsuario_1.setBounds(30, 185, 66, 15);
		panel.add(lblUsuario_1);
		
		JComboBox comboBoxUsuarioLista = new JComboBox(usuarios.toArray());
		comboBoxUsuarioLista.setBounds(105, 178, 114, 24);
		panel.add(comboBoxUsuarioLista);
		
		JLabel lblLista = new JLabel("Lista: ");
		lblLista.setBounds(30, 228, 66, 15);
		panel.add(lblLista);
		
		JComboBox comboBoxLista = new JComboBox();
		comboBoxLista.setBounds(105, 223, 114, 24);
		panel.add(comboBoxLista);
		
		JLabel lblElegirVideo = new JLabel("Elegir video");
		lblElegirVideo.setBounds(236, 12, 93, 15);
		panel.add(lblElegirVideo);
		
		JLabel lblElegirLista = new JLabel("Elegir Lista");
		lblElegirLista.setBounds(236, 146, 93, 15);
		panel.add(lblElegirLista);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(275, 248, 114, 25);
		panel.add(btnConfirmar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(400, 248, 114, 25);
		panel.add(btnCancelar);
		
		/* De aca para abajo, eventos y cosas que hacen los elementos */
		
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user = comboBoxUsuario.getSelectedItem().toString();
				List<String> videos = iu.findUsuarioByNickname(user).getCanal().listarVideos();
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				for (String aux : videos) {
					modelo.addElement(aux);
				}
				comboBoxVideo.setModel(modelo);
				/* aca alimento el combobox de video cuando selecciono un usuario */
			}
		});
		
		comboBoxUsuarioLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user = comboBoxUsuarioLista.getSelectedItem().toString();
				List<String> listas = iu.findUsuarioByNickname(user).getCanal().listarListas();
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				for (String aux : listas) {
					modelo.addElement(aux);
				}
				comboBoxLista.setModel(modelo);
				/* aca alimento el combobox de video cuando selecciono un usuario */
			}
		});
		
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//hacer chequeo de si esta todo con un pop up
				if (!(comboBoxUsuario.getSelectedIndex() == -1) && !(comboBoxUsuarioLista.getSelectedIndex() == -1)
						&& !(comboBoxVideo.getSelectedIndex() == -1)
						&& !(comboBoxLista.getSelectedIndex() == -1)) {
					try {
						String nickVideo = comboBoxUsuario.getSelectedItem().toString();
						String nickLista = comboBoxUsuarioLista.getSelectedItem().toString();
						String nomVid = comboBoxVideo.getSelectedItem().toString();
						String nomList = comboBoxLista.getSelectedItem().toString();
						
						Fabrica f = Fabrica.getInstancia();
						IElemento icon = f.getControladorElemento();
						icon.agregarVideoALista(nickVideo, nickLista, nomVid, nomList);
						JOptionPane.showMessageDialog(null, "Supuestamente anduvo joya !");
						dispose();
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Algo anduvo mal y paso esto: " + e.getMessage());
					}
					
				}
				else {
					JOptionPane.showMessageDialog(null, "Ingrese datos en los campos obligatorios.");
				}
				
			}
		});
		
	}
}
