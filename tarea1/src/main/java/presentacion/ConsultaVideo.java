package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import datatypes.DtComentario;
import datatypes.DtVideo;
import interfaces.Fabrica;
import interfaces.IElemento;
import logica.Comentario;
import logica.Video;

import javax.swing.JButton;

public class ConsultaVideo extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JTextField txtNombre;
	private JTextField txtDuracion;
	private JTextField txtDescripcion;
	private JTextField txtUrl;
	private JTree treeVideos;
	private JCheckBox chckbxPblico;
	List<DtVideo> videosUsuario = new ArrayList<DtVideo>();
	private JButton btnSalir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaVideo frame = new ConsultaVideo(null,null,null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaVideo(Fabrica f,String nick,String nombreVideo) {
		
		IElemento elementoOperations = f.getControladorElemento();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 566, 388);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(22, 22, 66, 15);
		contentPane.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(83, 19, 220, 23);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		treeVideos = new JTree(new DefaultMutableTreeNode());
		treeVideos.setBorder(new LineBorder(Color.GRAY));
		treeVideos.setBounds(22, 53, 505, 167);
		treeVideos.addMouseListener(new MouseAdapter() {
		      public void mouseClicked(MouseEvent me) {
		    	  onClickNodo(me);
		      }
		    });
		contentPane.add(treeVideos);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 241, 93, 14);
		contentPane.add(lblNombre);
		
		JLabel lblDuracin = new JLabel("Duraci\u00F3n");
		lblDuracin.setBounds(22, 266, 93, 14);
		contentPane.add(lblDuracin);
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setBounds(292, 241, 97, 14);
		contentPane.add(lblDescripcin);
		
		JLabel lblUrl = new JLabel("URL");
		lblUrl.setBounds(292, 266, 46, 14);
		contentPane.add(lblUrl);
		
		txtNombre = new JTextField();
		txtNombre.setEnabled(false);
		txtNombre.setBounds(133, 238, 117, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtDuracion = new JTextField();
		txtDuracion.setEnabled(false);
		txtDuracion.setColumns(10);
		txtDuracion.setBounds(133, 266, 117, 20);
		contentPane.add(txtDuracion);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setEnabled(false);
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(405, 238, 117, 20);
		contentPane.add(txtDescripcion);
		
		txtUrl = new JTextField();
		txtUrl.setEnabled(false);
		txtUrl.setColumns(10);
		txtUrl.setBounds(405, 266, 117, 20);
		contentPane.add(txtUrl);
		
		chckbxPblico = new JCheckBox("P\u00FAblico");
		chckbxPblico.setEnabled(false);
		chckbxPblico.setBounds(22, 293, 97, 23);
		contentPane.add(chckbxPblico);
		if (nick != null ) {
			if (!nick.equals("")) {
				txtUsuario.setText(nick);
				try {
					  videosUsuario = elementoOperations.getUserDtVideos(txtUsuario.getText());
					  cargarTree(videosUsuario);
				  }catch(Exception exc) {
					  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				  }
				DtVideo dtv = obtenerVideo(nombreVideo);
				cargarDatosVideo(dtv);
			}
		}
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(313, 17, 114, 25);
		btnNewButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
			  if(txtUsuario.getText().length() == 0) {
				  JOptionPane.showMessageDialog(new JFrame(), "Debe ingresar un usuario para consultar", "Mensaje", JOptionPane.ERROR_MESSAGE);
			  }else {
				  try {
					  videosUsuario = elementoOperations.getUserDtVideos(txtUsuario.getText());
					  cargarTree(videosUsuario);
				  }catch(Exception exc) {
					  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				  }
			  }
		  }
		});
		contentPane.add(btnNewButton);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(222, 316, 97, 25);
		contentPane.add(btnSalir);

	}
	
	
	
	
	
	
	private void cargarTree(List<DtVideo> videosUsuario) {
		
		DefaultMutableTreeNode elementoRoot = new DefaultMutableTreeNode("Videos");
		for(DtVideo video: videosUsuario) {
			DefaultMutableTreeNode videoNode = new DefaultMutableTreeNode(video.getNombre());
			elementoRoot.add(videoNode);
			for(DtComentario comentario: video.getComentarios()) {
				cargarRespuestas(videoNode, comentario);
			}
		}
		
		DefaultTreeModel model = new DefaultTreeModel(elementoRoot);
		treeVideos.setModel(model);
	}
	
	private void cargarRespuestas(DefaultMutableTreeNode anterior, DtComentario actual) {
		
		DefaultMutableTreeNode nodoActual = new DefaultMutableTreeNode("("+actual.getNickname()+") - "+actual.getDescripcion());
		if(actual.getComentarios().size() == 0) {
			anterior.add(nodoActual);
		}else {
			anterior.add(nodoActual);
			cargarRespuestas(nodoActual, actual.getComentarios().get(0));
		}
	}
	
	private DtVideo obtenerVideo(String nombre) {
		DtVideo resultado = null;
		for(DtVideo dtVideo: videosUsuario) {
			if(dtVideo.getNombre().equals(nombre)) 
				resultado = dtVideo;
		}
		return resultado;
	}
	
	private void onClickNodo(MouseEvent me) {
		TreePath treePath = treeVideos.getPathForLocation(me.getX(), me.getY());
		if(treePath != null) {
			String[] nombreNodoArray = treePath.toString().split(",");
			String nombreNodo = "";
			if(nombreNodoArray.length == 2) {
				nombreNodo = nombreNodoArray[1].replace("]", "");
				nombreNodo = nombreNodo.trim();
				DtVideo dtVideo = obtenerVideo(nombreNodo);
				cargarDatosVideo(dtVideo);
			}
		}
	}
	
	private void cargarDatosVideo(DtVideo dtVideo) {
		
		if(dtVideo.getNombre() == null) txtNombre.setText("");
		else txtNombre.setText(dtVideo.getNombre());
		
		if(String.valueOf(dtVideo.getDuracion()) == null) txtDuracion.setText("");
		else txtDuracion.setText(String.valueOf(dtVideo.getDuracion()));
			
		if(dtVideo.getDescripcion() == null) txtDescripcion.setText("");
		else txtDescripcion.setText(dtVideo.getDescripcion());
		
		if(dtVideo.getUrl() == null) txtUrl.setText("");
		else txtUrl.setText(dtVideo.getUrl());

		if(dtVideo.isPublico()) chckbxPblico.enable();
		else chckbxPblico.disable();

	}
	
	
	
	
	
}
