package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interfaces.Fabrica;
import interfaces.IUsuario;
import javax.swing.JTextField;

public class altaCategoria extends JFrame {

	private JPanel contentPane;
	private JTextField nombreCat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					altaCategoria frame = new altaCategoria(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public altaCategoria(Fabrica f) {
		setTitle("Alta Categoria");
		IUsuario ucon = f.getIControladorUsuario();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setBounds(100, 100, 434, 287);
		getContentPane().setLayout(null);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingresar nombre");
		lblNewLabel.setBounds(84, 33, 111, 14);
		getContentPane().add(lblNewLabel);

	
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ucon.altaCategoria(nombreCat.getText());
				JOptionPane.showMessageDialog(null, "Se realizo el alta correctamente");
			}
		});
		btnNewButton.setBounds(84, 128, 89, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(208, 128, 89, 23);
		getContentPane().add(btnSalir);
		
		nombreCat = new JTextField();
		nombreCat.setBounds(198, 29, 116, 22);
		contentPane.add(nombreCat);
		nombreCat.setColumns(10);

	}
}
