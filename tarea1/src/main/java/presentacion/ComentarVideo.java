package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import datatypes.DtComentario;
import datatypes.DtVideo;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;

public class ComentarVideo extends JFrame {

	private JPanel contentPane;
	private JTextField usuario;
	private JTextField comentario;
	private JTree treeVideos = new JTree(new DefaultMutableTreeNode());
	private String itemSeleccionadoTree = "";
	List<DtVideo> videosUsuario = new ArrayList<DtVideo>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComentarVideo frame = new ComentarVideo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComentarVideo(Fabrica f) {
		IElemento econ = f.getControladorElemento();
		int cmnt =0;
		setTitle("Comentar Video");		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 687, 467);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(12, 24, 77, 16);
		contentPane.add(lblUsuario);

		usuario = new JTextField();		
		usuario.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				 try {
					  videosUsuario = econ.getUserDtVideos(usuario.getText());
					  System.out.println("el usuario tiene "+videosUsuario.size());
					  cargarTree(videosUsuario);
				  }catch(Exception exc) {
					  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				  }

			}
		});
		usuario.setBounds(94, 22, 116, 22);
		contentPane.add(usuario);
		usuario.setColumns(10);
		
		JLabel lblComentario = new JLabel("Comentario:");
		lblComentario.setBounds(326, 54, 123, 41);
		contentPane.add(lblComentario);
		
		JTextArea txtComentario = new JTextArea();
		txtComentario.setBounds(326, 89, 240, 72);
		
		JButton button = new JButton("Aceptar");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("el comentario es" + txtComentario.getText());
				if(txtComentario.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Ingrese el comentario");
				}else{ 
					econ.comentarVideo(usuario.getText(), usuario.getText(), txtComentario.getText(), itemSeleccionadoTree);
					System.out.println("el camino al arbol es"+itemSeleccionadoTree);
					JOptionPane.showMessageDialog(null, "Se realizo el comentario corectamente");
					dispose();
				}
			}
			
		});
		button.setBounds(299, 382, 97, 25);
		contentPane.add(button);
	
		treeVideos.setBounds(29, 64, 259, 356);
		treeVideos.setBorder(new LineBorder(Color.GRAY));
		treeVideos.addMouseListener(new MouseAdapter() {
		      public void mouseClicked(MouseEvent me) {
		    	  onClickNodo(me);
		      }
		    });
		contentPane.add(treeVideos);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(183, 109, 1, 15);
		contentPane.add(textArea);
		

		contentPane.add(txtComentario);
		
	}

	private void cargarTree(List<DtVideo> videosUsuario) {
		
		DefaultMutableTreeNode elementoRoot = new DefaultMutableTreeNode("Videos");
		for(DtVideo video: videosUsuario) {
			DefaultMutableTreeNode videoNode = new DefaultMutableTreeNode(video.getNombre());
			elementoRoot.add(videoNode);
			for(DtComentario comentario: video.getComentarios()) {
				cargarRespuestas(videoNode, comentario);
			}
		}
		
		DefaultTreeModel model = new DefaultTreeModel(elementoRoot);
		treeVideos.setModel(model);
	}
	
	private void cargarRespuestas(DefaultMutableTreeNode anterior, DtComentario actual) {
		
		DefaultMutableTreeNode nodoActual = new DefaultMutableTreeNode("("+actual.getNickname()+") - "+actual.getDescripcion());
		if(actual.getComentarios().size() == 0) {
			anterior.add(nodoActual);
		}else {
			anterior.add(nodoActual);
			cargarRespuestas(nodoActual, actual.getComentarios().get(0));
		}
	}
	
	private void onClickNodo(MouseEvent me) {
		TreePath treePath = treeVideos.getPathForLocation(me.getX(), me.getY());
		if(treePath != null) {
			itemSeleccionadoTree = treePath.toString();
		}
	}

}
