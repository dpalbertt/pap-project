package presentacion;

import java.awt.EventQueue;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datatypes.DtVideo;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;

public class ValorarVideo extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ValorarVideo frame = new ValorarVideo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ValorarVideo(Fabrica f) {
		
		IUsuario userOperations = f.getIControladorUsuario();
		IElemento elementoOperations = f.getControladorElemento();
		
		List<String> usuarios = userOperations.listarUsuarios();
		usuarios.add(" ");
		
		
		JComboBox 	cbUsu = new JComboBox(usuarios.toArray());
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 307, 318);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JList list = new JList();
		list.setBounds(20, 68, 245, 96);
		
	
		JLabel lblUsuario = new JLabel("Usuario"); 
		lblUsuario.setBounds(20, 16, 66, 15);
		contentPane.add(lblUsuario);
		
		JLabel lblVideos = new JLabel("Videos");
		lblVideos.setBounds(20, 42, 66, 15);
		contentPane.add(lblVideos);
		
		JLabel lblLeGusta = new JLabel("Me gusta");
		lblLeGusta.setBounds(20, 186, 66, 15);
		contentPane.add(lblLeGusta);
		
		List<String> opcMegusta = new ArrayList<String>();
		opcMegusta.add("Si");
		opcMegusta.add("No");
		opcMegusta.add(" ");
		JComboBox cbMegusta = new JComboBox(opcMegusta.toArray()); 
		cbMegusta.setBounds(71, 181, 194, 24);
		cbMegusta.setSelectedIndex(2);
		contentPane.add(cbMegusta);

		JButton btnValorar = new JButton("Valorar");
		btnValorar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String nombreVideo = (String) list.getModel().getElementAt(0);
				elementoOperations.valorarVideo(cbUsu.getSelectedItem().toString(), nombreVideo, cbMegusta.getSelectedItem().toString());
				dispose();
			}
		});
		btnValorar.setBounds(92, 234, 114, 25);
		contentPane.add(btnValorar);
		
		contentPane.add(list);
		
	
		cbUsu.setBounds(71, 11, 193, 24);
		cbUsu.addFocusListener(new FocusAdapter() {
			
			public void focusLost(FocusEvent arg0) {
				try {
					List<DtVideo> userVideos = elementoOperations.getUserDtVideos(cbUsu.getSelectedItem().toString());
					List<String> nombreVideos = getVideoNames(userVideos);
			        final DefaultListModel model = new DefaultListModel();
			        for (String c : nombreVideos) model.addElement(c);
			        list.setModel(model);
				}catch(Exception e) {
					JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			
		});
		contentPane.add(cbUsu);
	}
	
	private List<String> getVideoNames(List<DtVideo> lista){
		List<String> resultado = new ArrayList<String>();
		for(DtVideo video: lista) resultado.add(video.getNombre());
		return resultado;
	}
}
