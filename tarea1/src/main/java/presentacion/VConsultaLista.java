package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import datatypes.DtLista;
import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.ManejadorUsuario;

import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import java.util.List;
import javax.swing.JTextArea;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ItemEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VConsultaLista extends JFrame {

	private JPanel contentPane;

	//para ejecutar la ventana sola
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Fabrica f = Fabrica.getInstancia();
	 * IUsuario iu = f.getIControlador(); IElemento ie = f.getControladorElemento();
	 * VConsultaLista frame = new VConsultaLista(ie, iu); frame.setVisible(true); }
	 * catch (Exception e) { e.printStackTrace(); } } }); }
	 */

	/**
	 * Create the frame.
	 */
	public VConsultaLista(Fabrica f, String nick, String lista) {
		IElemento ie = f.getControladorElemento();
		IUsuario iu = f.getIControladorUsuario();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 539, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(36, 12, 66, 15);
		contentPane.add(lblUsuario);
		
		List<String> usuarios = iu.listarUsuarios();
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
	
		comboBoxUsuario.setBounds(249, 7, 182, 24);
		contentPane.add(comboBoxUsuario);
		
		JLabel lblListaDeReproduccion = new JLabel("Lista de Reproduccion: ");
		lblListaDeReproduccion.setBounds(36, 60, 160, 15);
		contentPane.add(lblListaDeReproduccion);
		
		JComboBox comboBoxLista = new JComboBox();
		
		comboBoxLista.setBounds(249, 55, 182, 24);
		contentPane.add(comboBoxLista);
		
		JList listVideos = new JList();
		JScrollPane scrollLista = new JScrollPane();
		scrollLista.setBounds(296, 146, 194, 123);;
		scrollLista.setViewportView(listVideos);
		contentPane.add(scrollLista);
		
		
		JLabel lblVideos = new JLabel("Videos");
		lblVideos.setBounds(365, 119, 66, 15);
		contentPane.add(lblVideos);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(23, 134, 66, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblPrivacidad = new JLabel("Privacidad: ");
		lblPrivacidad.setBounds(23, 170, 98, 15);
		contentPane.add(lblPrivacidad);
		
		JLabel lblTipoDeLista = new JLabel("Tipo de Lista:");
		lblTipoDeLista.setBounds(23, 210, 98, 15);
		contentPane.add(lblTipoDeLista);
		
		JTextArea textNombre = new JTextArea();
		textNombre.setEnabled(false);
		textNombre.setBounds(135, 134, 149, 15);
		contentPane.add(textNombre);
		
		JTextArea textPrivacidad = new JTextArea();
		textPrivacidad.setEnabled(false);
		textPrivacidad.setBounds(139, 170, 145, 15);
		contentPane.add(textPrivacidad);
		
		JTextArea textTipo = new JTextArea();
		textTipo.setEnabled(false);
		textTipo.setBounds(139, 210, 145, 15);
		contentPane.add(textTipo);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setBounds(99, 256, 97, 25);
		contentPane.add(btnSalir);
		
		/* de aca para abajo esta la logica de los botones y eso */
		
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nick = comboBoxUsuario.getSelectedItem().toString();
				List<String> listas = iu.findUsuarioByNickname(nick).getCanal().listarListas();
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				for (String aux : listas) {
					modelo.addElement(aux);
				}
				comboBoxLista.setModel(modelo);
				/* agregue todos los elementos al combo box */
			}
		});
		
		comboBoxLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nick = comboBoxUsuario.getSelectedItem().toString();
				String nomList = comboBoxLista.getSelectedItem().toString();
				DtLista dtL = iu.findUsuarioByNickname(nick).getCanal().buscarListaDT(nomList);
				textNombre.setText(dtL.getNombre());
				String privacidad;
				if (dtL.isPublico()) {
					privacidad = "Publica";
				}
				else {
					privacidad = "Privada";
				}
				textPrivacidad.setText(privacidad);
				List<String> videos = dtL.listarDtvideo();
				DefaultListModel modelo = new DefaultListModel();
				for (String aux : videos) {
					modelo.addElement(aux);
				}
				listVideos.setModel(modelo);
				textTipo.setText(dtL.getTipo());
				listVideos.setBounds(296, 146, 194, 123);
				contentPane.add(listVideos);
				
			}
			
		});
		
		if (nick != null) {
			if (!nick.equals("")) {
				comboBoxUsuario.setSelectedItem(nick);
				comboBoxLista.setSelectedItem(lista);
				//comboBoxUsuario.action(null, null); para probar despues con consulta usuario
			}
		}
		
		/* para hacer *///////////////////////////////////////////////////////////////////////////
		listVideos.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				/* aca hay que poner que llama a la ventan de consulta video de ese video */
				DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
				String video = listVideos.getSelectedValue().toString();
				ConsultaVideo consultaVideo = new ConsultaVideo(f,dtu.getNickname(),video);
				consultaVideo.setVisible(true);
			}
		});
	}
}
