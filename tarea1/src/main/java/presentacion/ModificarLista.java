package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import datatypes.DtLista;
import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

public class ModificarLista extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarLista frame = new ModificarLista(null,null,null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarLista(Fabrica f, String nickDef, String nomListDef) {
		IUsuario iu = f.getIControladorUsuario();
		IElemento ic = f.getControladorElemento();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 539, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(36, 12, 66, 15);
		contentPane.add(lblUsuario);
		
		List<String> usuarios = iu.listarUsuarios();
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
		comboBoxUsuario.setBounds(249, 7, 182, 24);
		contentPane.add(comboBoxUsuario);
		
		JLabel lblListaDeReproduccion = new JLabel("Lista de Reproduccion: ");
		lblListaDeReproduccion.setBounds(36, 60, 160, 15);
		contentPane.add(lblListaDeReproduccion);
		
		JComboBox comboBoxLista = new JComboBox();
		comboBoxLista.setBounds(249, 55, 182, 24);
		contentPane.add(comboBoxLista);
		
		List<String> categorias = iu.listarCategorias();
		
		JComboBox comboBoxCategoria = new JComboBox(categorias.toArray());
		comboBoxCategoria.setBounds(371, 197, 138, 24);
		contentPane.add(comboBoxCategoria);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(36, 114, 66, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblPrivacidad = new JLabel("Privacidad: ");
		lblPrivacidad.setBounds(36, 166, 98, 15);
		contentPane.add(lblPrivacidad);
		
		JLabel lblTipoDeLista = new JLabel("Categoria:");
		lblTipoDeLista.setBounds(36, 202, 66, 15);
		contentPane.add(lblTipoDeLista);
		
		JTextArea textNombre = new JTextArea();
		textNombre.setBounds(107, 114, 149, 23);
		contentPane.add(textNombre);
		
		JTextArea textPrivacidad = new JTextArea();
		textPrivacidad.setEnabled(false);
		textPrivacidad.setBounds(107, 162, 145, 27);
		contentPane.add(textPrivacidad);
		
		JTextArea textTipo = new JTextArea();
		textTipo.setEnabled(false);
		textTipo.setBounds(107, 198, 138, 24);
		contentPane.add(textTipo);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setBounds(302, 256, 97, 25);
		contentPane.add(btnSalir);
		

		JCheckBox chckbxNewCheckBox = new JCheckBox("Cambiar privacidad");		
		chckbxNewCheckBox.setBounds(249, 161, 150, 25);
		contentPane.add(chckbxNewCheckBox);
		
		JLabel lblNuevaCategoria = new JLabel("Nueva Categoria");
		lblNuevaCategoria.setBounds(249, 201, 104, 16);
		contentPane.add(lblNuevaCategoria);
		
		/* de aca para abajo esta la logica de los botones y eso */
		
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nick = comboBoxUsuario.getSelectedItem().toString();
				List<String> listas = iu.findUsuarioByNickname(nick).getCanal().listarListas();
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				for (String aux : listas) {
					modelo.addElement(aux);
				}
				comboBoxLista.setModel(modelo);
				/* agregue todos los elementos al combo box */
			}
		});
		
		comboBoxLista.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String nick = comboBoxUsuario.getSelectedItem().toString();
				String nomList = comboBoxLista.getSelectedItem().toString();
				DtLista dtL = iu.findUsuarioByNickname(nick).getCanal().buscarListaDT(nomList);
				textNombre.setText(dtL.getNombre());
				String privacidad;
				if (dtL.isPublico()) {
					privacidad = "Publica";
				}
				else {
					privacidad = "Privada";
				}	
				textPrivacidad.setText(privacidad);					
			}
			
		});	
		JButton btnAceptar = new JButton("Aceptar");
		
		
		
		
		if (nickDef != null) {
			if (!nickDef.equals("")) {
				comboBoxUsuario.setSelectedItem(nickDef);
				comboBoxLista.setSelectedItem(nomListDef);
				comboBoxUsuario.setEnabled(false);
				comboBoxLista.setEnabled(false);
			}
		}
		
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//chkPublico.isSelected()
				String nick = comboBoxUsuario.getSelectedItem().toString();
				String nomList = comboBoxLista.getSelectedItem().toString();
				try {
					if (comboBoxCategoria.getSelectedIndex() != -1 && 
							!comboBoxCategoria.getSelectedItem().toString().equals( textNombre.getText() ) ) {
						String nuevaCat = comboBoxCategoria.getSelectedItem().toString();
						//mando el modificar lista con el nombre de la categoria nueva
						ic.modificarLista(nomList,nick,chckbxNewCheckBox.isSelected(),nuevaCat);
					} else {
						//mando el modificar lista con el nombre de la categoria vieja
						ic.modificarLista(nomList,nick,chckbxNewCheckBox.isSelected(),textNombre.getText());
						
					}
					JOptionPane.showMessageDialog(null, "Supuestamente anduvo joya !");
					dispose();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Algo anduvo mal y paso esto: " + e1.getMessage());
				}
				
				
				
				//econ.modificarLista(nick,nomLista,chkPublico.isSelected(),nuevaCat.getText());
				dispose();
			}
		});
		btnAceptar.setBounds(135, 256, 97, 25);
		contentPane.add(btnAceptar);
		
		
	}
}
