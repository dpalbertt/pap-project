package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.ManejadorUsuario;
import logica.Usuario;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.event.ListSelectionEvent;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

public class ModificarDatosUsuario extends JFrame {
	private JPanel calendario;
	private JTextField txtNickname;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtEmail;
	private JTextField txtFechaNacimiento;
	private JTextField txtNombreCanal;
	private JTextField txtDescripcionCanal;
	private JTextField txtImagen;
	private JTextField txtNuevoNombre;
	private JTextField txtNuevoApellido;
	private JTextField txtNuevaImagen;
	private JTextField txtNuevoNombreCanal;
	private JTextField txtNuevaDescripcion;
	private JTextField txtPass;
	private JTextField txtPassNueva;



	/**
	 * Create the frame.
	 */
	public ModificarDatosUsuario(Fabrica f) {
		IUsuario iu = f.getIControladorUsuario();
		IElemento ie = f.getControladorElemento();
		setTitle("Modificar Datos de Usuario");

		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 697, 656);
		calendario = new JPanel();
		calendario.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(calendario);
		calendario.setLayout(null);
		
		JCheckBox chkPublico = new JCheckBox("Publico");
		chkPublico.setSelected(false);
		
		JLabel lblUsuarios = new JLabel("Usuarios");
		lblUsuarios.setBounds(30, 25, 48, 14);
		calendario.add(lblUsuarios);
		
		JList listVideo = new JList();
		
		listVideo.setBounds(394, 469, 174, 49);
		calendario.add(listVideo);
		
		JList listLista = new JList();
		
		listLista.setBounds(132, 469, 174, 49);
		calendario.add(listLista);
		
		JDateChooser Fecha = new JDateChooser();
		Fecha.setBounds(446, 235, 122, 20);
		calendario.add(Fecha);
		
		
		
		
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		List<String> usuarios = mu.listarUsuarios();
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
		comboBoxUsuario.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
		    	Usuario usuario = iu.findUsuarioByNickname(dtu.getNickname());

		        txtNickname.setText(dtu.getNickname());
		        if (dtu.getNombre() != null) {
		        	txtNombre.setText(dtu.getNombre());
		        }
		        if (dtu.getPassword() != null) {
		        	txtPass.setText(dtu.getPassword());
		        }
		        if (dtu.getApellido() != null) {
		        	txtApellido.setText(dtu.getApellido());
		        }
		        if (dtu.getEmail() != null) {
		        	txtEmail.setText(dtu.getEmail());
		        }
		        if (dtu.getfNacimiento().toString() != null) {
		        	txtFechaNacimiento.setText(dtu.getfNacimiento().toString());
		        }
		        if (dtu.getFoto() != null) {
		        	txtImagen.setText(dtu.getFoto());
		        }

     
		        
		        chkPublico.setSelected(dtu.getCanal().isPublico());
		        
		        
		        
		        
		        if (dtu.getCanal().getNombre() != null) {
		        	txtNombreCanal.setText(dtu.getCanal().getNombre());
		        }
		        if (dtu.getCanal().getDescripcion() != null) {
		        	txtDescripcionCanal.setText(dtu.getCanal().getDescripcion());
		        }
		        
		        //JLIST VIDEOS
		        List<String> videos =  ie.listarVideoUsuario(dtu.getNickname()); 
		        final DefaultListModel modelVideos = new DefaultListModel();
		        for (String c : videos) {
		        	modelVideos.addElement(c);
		        }
		        listVideo.setModel(modelVideos);
		        
		      //JLIST LISTAS
		        List<String> listas =  ie.listarListasRepUsuario(dtu.getNickname());
		        final DefaultListModel modelListas = new DefaultListModel();
		        for (String c : listas) {
		        	modelListas.addElement(c);
		        }
		        listLista.setModel(modelListas);
		        		
		    }
		});
		

		
		
		JButton btnAceptar = new JButton("Aceptar");		
		btnAceptar.setBounds(209, 544, 97, 25);
		calendario.add(btnAceptar);
		
		btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(txtNuevoNombre.getText().equals("") || txtNuevoApellido.getText().equals("") || txtNuevoNombreCanal.getText().equals("") || txtNuevaDescripcion.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Ingrese los datos que faltan");
					}else {	

						iu.datosUsuario(comboBoxUsuario.getSelectedItem().toString(),txtPassNueva.getText(),txtEmail.getText(),String.valueOf(txtNuevoNombre.getText()),
										txtNuevoApellido.getText(),Fecha.getDate(),txtNuevaImagen.getText(),chkPublico.isSelected(),txtNuevoNombreCanal.getText(),txtNuevaDescripcion.getText());
						JOptionPane.showMessageDialog(null, "Usuario modificado correctamente");
						dispose();
					}
				}
				
			});
		
		
		
		
		
		
	
		comboBoxUsuario.setBounds(132, 21, 122, 22);
		calendario.add(comboBoxUsuario);
		
		JLabel lblNickname = new JLabel("Nickname:");
		lblNickname.setBounds(30, 63, 92, 14);
		calendario.add(lblNickname);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(30, 146, 62, 14);
		calendario.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(30, 188, 48, 14);
		calendario.add(lblApellido);
		
		JLabel lblEmail = new JLabel("Email: ");
		lblEmail.setBounds(325, 63, 48, 14);
		calendario.add(lblEmail);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de Nac.:");
		lblFechaDeNacimiento.setBounds(30, 277, 109, 14);
		calendario.add(lblFechaDeNacimiento);
		
		JLabel lblTipoDeCanal = new JLabel("Tipo de Canal: ");
		lblTipoDeCanal.setBounds(30, 327, 92, 14);
		calendario.add(lblTipoDeCanal);
		
		JLabel lblNombreCanal = new JLabel("Nombre Canal: ");
		lblNombreCanal.setBounds(30, 369, 89, 14);
		calendario.add(lblNombreCanal);
		
		JLabel lblDescripcionCanal = new JLabel("Descripcion Canal:");
		lblDescripcionCanal.setBounds(30, 418, 92, 14);
		calendario.add(lblDescripcionCanal);
		
		JLabel lblImagen = new JLabel("Imagen: ");
		lblImagen.setBounds(30, 235, 48, 14);
		calendario.add(lblImagen);
		chkPublico.setBounds(157, 323, 97, 23);
		calendario.add(chkPublico);
		
		txtNickname = new JTextField();
		txtNickname.setEnabled(false);
		txtNickname.setBounds(132, 60, 122, 20);
		calendario.add(txtNickname);
		txtNickname.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setEnabled(false);
		txtNombre.setBounds(132, 143, 122, 20);
		calendario.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setEnabled(false);
		txtApellido.setBounds(132, 185, 122, 20);
		calendario.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setEnabled(false);
		txtEmail.setBounds(446, 60, 122, 20);
		calendario.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtFechaNacimiento = new JTextField();
		txtFechaNacimiento.setEnabled(false);
		txtFechaNacimiento.setBounds(132, 274, 122, 20);
		calendario.add(txtFechaNacimiento);
		txtFechaNacimiento.setColumns(10);
		
		txtNombreCanal = new JTextField();
		txtNombreCanal.setEnabled(false);
		txtNombreCanal.setBounds(132, 366, 122, 20);
		calendario.add(txtNombreCanal);
		txtNombreCanal.setColumns(10);
		
		txtDescripcionCanal = new JTextField();
		txtDescripcionCanal.setEnabled(false);
		txtDescripcionCanal.setBounds(132, 415, 122, 20);
		calendario.add(txtDescripcionCanal);
		txtDescripcionCanal.setColumns(10);
		
		txtImagen = new JTextField();
		txtImagen.setEnabled(false);
		txtImagen.setBounds(132, 229, 122, 20);
		calendario.add(txtImagen);
		txtImagen.setColumns(10);
		
		JLabel lblVideos = new JLabel("Videos: ");
		lblVideos.setBounds(325, 470, 48, 14);
		calendario.add(lblVideos);
		
		
		JLabel lblListasDeRep = new JLabel("Listas de Rep.");
		lblListasDeRep.setBounds(30, 470, 80, 14);
		calendario.add(lblListasDeRep);
		
		

		
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(394, 545, 89, 23);
		calendario.add(btnSalir);
		
		txtNuevoNombre = new JTextField();
		txtNuevoNombre.setBounds(446, 143, 122, 20);
		calendario.add(txtNuevoNombre);
		txtNuevoNombre.setColumns(10);
		
		JLabel lblNuevoNombre = new JLabel("Nuevo Nombre:");
		lblNuevoNombre.setBounds(325, 146, 92, 14);
		calendario.add(lblNuevoNombre);
		
		JLabel lblNuevoApellido = new JLabel("Nuevo Apellido:");
		lblNuevoApellido.setBounds(325, 188, 92, 14);
		calendario.add(lblNuevoApellido);
		
		txtNuevoApellido = new JTextField();
		txtNuevoApellido.setBounds(446, 185, 122, 20);
		calendario.add(txtNuevoApellido);
		txtNuevoApellido.setColumns(10);
		
		txtNuevaImagen = new JTextField();
		txtNuevaImagen.setBounds(446, 274, 122, 20);
		calendario.add(txtNuevaImagen);
		txtNuevaImagen.setColumns(10);
		
		JLabel lblNuevaFechaDe = new JLabel("Nueva Fecha de Nac.:");
		lblNuevaFechaDe.setBounds(325, 235, 109, 14);
		calendario.add(lblNuevaFechaDe);
		
		txtNuevoNombreCanal = new JTextField();
		txtNuevoNombreCanal.setBounds(446, 366, 122, 20);
		calendario.add(txtNuevoNombreCanal);
		txtNuevoNombreCanal.setColumns(10);
		
		txtNuevaDescripcion = new JTextField();
		txtNuevaDescripcion.setBounds(446, 415, 122, 20);
		calendario.add(txtNuevaDescripcion);
		txtNuevaDescripcion.setColumns(10);
		
		JLabel lblNuevoNombreCanal = new JLabel("Nuevo Nombre Canal:");
		lblNuevoNombreCanal.setBounds(325, 369, 122, 14);
		calendario.add(lblNuevoNombreCanal);
		
		JLabel lblNuevaDescripcion = new JLabel("Nueva Descripcion: ");
		lblNuevaDescripcion.setBounds(325, 418, 109, 14);
		calendario.add(lblNuevaDescripcion);
		
		JLabel lblNewLabel = new JLabel("Contrase\u00F1a:");
		lblNewLabel.setBounds(30, 105, 80, 14);
		calendario.add(lblNewLabel);
		
		txtPass = new JTextField();
		txtPassNueva = new JTextField();
		txtPass.setEditable(false);
		txtPass.setBounds(132, 102, 122, 20);
		calendario.add(txtPass);
		txtPass.setColumns(10);
		
		JLabel lblNuevaContrasea = new JLabel("Nueva contrase\u00F1a:");
		lblNuevaContrasea.setBounds(325, 105, 139, 14);
		calendario.add(lblNuevaContrasea);
		
		
		txtPassNueva.setBounds(446, 102, 124, 20);
		calendario.add(txtPassNueva);
		txtPassNueva.setColumns(10);
		
		JButton botonImagen = new JButton("Nueva Imagen");
		botonImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setAcceptAllFileFilterUsed(false);
				FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG and JPG images", "png", "jpg", "jpeg");
				jfc.setFileFilter(filter);
				int returnValue = jfc.showOpenDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					String selectedFile = jfc.getSelectedFile().getAbsolutePath();
					txtNuevaImagen.setText(selectedFile);
				}
			}
		});
		botonImagen.setBounds(325, 273, 111, 23);
		calendario.add(botonImagen);
		

		
		

		
		
		listVideo.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
				String video = listVideo.getSelectedValue().toString();
				ModificarVideo modificarVideo = new ModificarVideo(f, comboBoxUsuario.getSelectedItem().toString(),video);
				modificarVideo.setVisible(true);
			}
		});
		
		
		listLista.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
				String lista = listLista.getSelectedValue().toString();
				ModificarLista modificarLista = new ModificarLista(f, comboBoxUsuario.getSelectedItem().toString(),lista);
				modificarLista.setVisible(true);
			}
		});
		
		
		
	}
}