package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datatypes.DtCanal;
import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.Canal;
import logica.ManejadorUsuario;
import logica.Usuario;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class vConsultaUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNickname;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtEmail;
	private JTextField txtFechaNacimiento;
	private JTextField txtNombreCanal;
	private JTextField txtDescripcionCanal;
	private JTextField txtImagen;



	/**
	 * Create the frame.
	 */
	public vConsultaUsuario(Fabrica f) {
		IUsuario iu = f.getIControladorUsuario();
		IElemento ie = f.getControladorElemento();
		setTitle("Consulta de Usuario");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 697, 599);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JCheckBox chkPublico = new JCheckBox("Publico");
		chkPublico.setEnabled(false);
		chkPublico.setSelected(true);
		
		JLabel lblUsuarios = new JLabel("Usuarios");
		lblUsuarios.setBounds(30, 25, 48, 14);
		contentPane.add(lblUsuarios);
		
		JList listVideo = new JList();
		
		listVideo.setBounds(132, 337, 174, 49);
		contentPane.add(listVideo);
		
		JList listLista = new JList();
		
		listLista.setBounds(132, 404, 174, 49);
		contentPane.add(listLista);
		
		JList listSeguidores = new JList();
		listSeguidores.setBounds(419, 337, 174, 49);
		contentPane.add(listSeguidores);
		
		
		JList listSiguiendo = new JList();
		listSiguiendo.setBounds(419, 404, 174, 49);
		contentPane.add(listSiguiendo);
		
		
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		List<String> usuarios = mu.listarUsuarios();
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
		comboBoxUsuario.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
		    	Usuario usuario = iu.findUsuarioByNickname(dtu.getNickname());

		        txtNickname.setText(dtu.getNickname());
		        if (dtu.getNombre() != null) {
		        	txtNombre.setText(dtu.getNombre());
		        }
		        if (dtu.getApellido() != null) {
		        	txtApellido.setText(dtu.getApellido());
		        }
		        if (dtu.getEmail() != null) {
		        	txtEmail.setText(dtu.getEmail());
		        }
		        if (dtu.getfNacimiento().toString() != null) {
		        	txtFechaNacimiento.setText(dtu.getfNacimiento().toString());
		        }
		        
		        if (dtu.getFoto() != null) {
		        	txtImagen.setText(dtu.getFoto());
		        }
		        

		     
		        chkPublico.setSelected(dtu.isPublico());
		        if (dtu.getCanal().getNombre() != null) {
		        	txtNombreCanal.setText(dtu.getCanal().getNombre());
		        }
		        if (dtu.getCanal().getDescripcion() != null) {
		        	txtDescripcionCanal.setText(dtu.getCanal().getDescripcion());
		        }
		        
		        //JLIST VIDEOS
		        List<String> videos =  ie.listarVideoUsuario(dtu.getNickname());
		        final DefaultListModel modelVideos = new DefaultListModel();
		        for (String c : videos) {
		        	modelVideos.addElement(c);
		        }
		        listVideo.setModel(modelVideos);
		        
		      //JLIST LISTAS
		        List<String> listas =  ie.listarListasRepUsuario(dtu.getNickname());
		        final DefaultListModel modelListas = new DefaultListModel();
		        for (String c : listas) {
		        	modelListas.addElement(c);
		        }
		        listLista.setModel(modelListas);
		        
		        
		      //JLIST SIGUIENDO
	        
		        List<String> usuariosQueSigo =  iu.listarUsuariosQueSigo(usuario);
		        final DefaultListModel modelSiguiendo = new DefaultListModel();
		        for (String c : usuariosQueSigo) {
		        	modelSiguiendo.addElement(c);
		        }
		        listSiguiendo.setModel(modelSiguiendo);
		        
		        
		      //JLIST SEGUIDORES
		        
		        List<String> usuariosSeguidores =  iu.misSeguidores(usuario);
		        final DefaultListModel modelSeguidores = new DefaultListModel();
		        for (String c : usuariosSeguidores) {
		        	modelSeguidores.addElement(c);
		        }
		        listSeguidores.setModel(modelSeguidores);

		
		    }
		});
		
		
	
		comboBoxUsuario.setBounds(184, 21, 122, 22);
		contentPane.add(comboBoxUsuario);
		
		JLabel lblNickname = new JLabel("Nickname:");
		lblNickname.setBounds(30, 74, 92, 14);
		contentPane.add(lblNickname);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(30, 124, 62, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(30, 177, 48, 14);
		contentPane.add(lblApellido);
		
		JLabel lblEmail = new JLabel("Email: ");
		lblEmail.setBounds(30, 231, 48, 14);
		contentPane.add(lblEmail);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de Nacimiento:");
		lblFechaDeNacimiento.setBounds(30, 290, 109, 14);
		contentPane.add(lblFechaDeNacimiento);
		
		JLabel lblTipoDeCanal = new JLabel("Tipo de Canal: ");
		lblTipoDeCanal.setBounds(328, 74, 92, 14);
		contentPane.add(lblTipoDeCanal);
		
		JLabel lblNombreCanal = new JLabel("Nombre Canal: ");
		lblNombreCanal.setBounds(328, 124, 89, 14);
		contentPane.add(lblNombreCanal);
		
		JLabel lblDescripcionCanal = new JLabel("Descripcion Canal:");
		lblDescripcionCanal.setBounds(328, 177, 92, 14);
		contentPane.add(lblDescripcionCanal);
		
		JLabel lblImagen = new JLabel("Imagen: ");
		lblImagen.setBounds(328, 231, 48, 14);
		contentPane.add(lblImagen);
		chkPublico.setBounds(443, 70, 97, 23);
		contentPane.add(chkPublico);
		
		txtNickname = new JTextField();
		txtNickname.setEnabled(false);
		txtNickname.setBounds(184, 71, 122, 20);
		contentPane.add(txtNickname);
		txtNickname.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setEnabled(false);
		txtNombre.setBounds(184, 121, 122, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setEnabled(false);
		txtApellido.setBounds(184, 174, 122, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setEnabled(false);
		txtEmail.setBounds(184, 228, 122, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtFechaNacimiento = new JTextField();
		txtFechaNacimiento.setEnabled(false);
		txtFechaNacimiento.setBounds(184, 287, 122, 20);
		contentPane.add(txtFechaNacimiento);
		txtFechaNacimiento.setColumns(10);
		
		txtNombreCanal = new JTextField();
		txtNombreCanal.setEnabled(false);
		txtNombreCanal.setBounds(471, 121, 122, 20);
		contentPane.add(txtNombreCanal);
		txtNombreCanal.setColumns(10);
		
		txtDescripcionCanal = new JTextField();
		txtDescripcionCanal.setEnabled(false);
		txtDescripcionCanal.setBounds(471, 171, 122, 20);
		contentPane.add(txtDescripcionCanal);
		txtDescripcionCanal.setColumns(10);
		
		txtImagen = new JTextField();
		txtImagen.setEnabled(false);
		txtImagen.setBounds(471, 228, 122, 20);
		contentPane.add(txtImagen);
		txtImagen.setColumns(10);
		
		JLabel lblVideos = new JLabel("Videos: ");
		lblVideos.setBounds(30, 338, 48, 14);
		contentPane.add(lblVideos);
		
		
		JLabel lblListasDeRep = new JLabel("Listas de Rep.");
		lblListasDeRep.setBounds(30, 405, 80, 14);
		contentPane.add(lblListasDeRep);
		
		JLabel lblSiguiendoA = new JLabel("Siguiendo a:");
		lblSiguiendoA.setBounds(328, 405, 80, 14);
		contentPane.add(lblSiguiendoA);
		
		JLabel lblSeguidores = new JLabel("Seguidores:");
		lblSeguidores.setBounds(328, 338, 80, 14);
		contentPane.add(lblSeguidores);
		
		

		
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(303, 497, 89, 23);
		contentPane.add(btnSalir);
		
		listVideo.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
				String video = listVideo.getSelectedValue().toString();
				ConsultaVideo consultaVideo = new ConsultaVideo(f,dtu.getNickname(),video);
				consultaVideo.setVisible(true);
			}
		});
		
		
		listLista.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				DtUsuario dtu = iu.getUsuario(comboBoxUsuario.getSelectedItem().toString());
				String lista = listLista.getSelectedValue().toString();
				VConsultaLista consultaLista = new VConsultaLista(f,dtu.getNickname(),lista);
				consultaLista.setVisible(true);
			}
		});
		
		
		
	}
}
