package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

import interfaces.Fabrica;
import publicadores.WSControladorElemento;
import publicadores.WSControladorUsuario;

public class MainWindow {

	private JFrame frmUyTube;
	private AltaVideo AltaVideoInternalFrame;
	private ComentarVideo ComentarVideoInternalFrame;
	private altaCategoria AltaCategoriaInternalFrame;
	private ListarCategoria ListarCategoriaInternalFrame;
	private ConsultaVideo ConsultaVideoInternalFrame;
	private ValorarVideo ValorarVideoInternalFrame;
	private VAgregarVideoLista VAgregarVideoListaInternalFrame;
	private VConsultaLista VConsultaListaInternalFrame;
	private VCrearLista VCrearListaInternalFrame;
	private vAltaUsuario VAltaUsuarioInternalFrame;
	private vConsultaUsuario VConsultaUsuarioInternalFrame;
	private ListarUsuarios ListarUsuariosInternalFrame;
	private ListarUsuariosDeBaja ListarUsuariosDeBajaInternalFrame;
	private ModificarLista ModificarListaInternalFrame;
	private ModificarDatosUsuario ModificarDatosUsuarioInternalFrame; 
	private ModificarVideo ModificarVideoInternalFrame;
	private QuitarVideoLista QuitarVideoListaInternalFrame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				} catch (Exception e) {
					//e.printStackTrace();
				}
				MainWindow window = new MainWindow();
				window.frmUyTube.setVisible(true);
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		Fabrica fabrica = Fabrica.getInstancia();
		initialize(fabrica);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Fabrica fabrica) {
		Fabrica f = fabrica; 
		WSControladorUsuario wsu = new WSControladorUsuario();
		wsu.publicar();
		WSControladorElemento wse = new WSControladorElemento();
		wse.publicar();
		frmUyTube = new JFrame();
		frmUyTube.setTitle("Uy Tube");
		frmUyTube.setBounds(100, 100, 522, 300);
		frmUyTube.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		WSControladorElemento wsElemento = new WSControladorElemento();
		wsElemento.publicar();
		
		JMenuBar menuBar = new JMenuBar();
		frmUyTube.setJMenuBar(menuBar);
		
		JMenu mnListaDeReproduccion = new JMenu("Lista de Reproduccion");
		menuBar.add(mnListaDeReproduccion);
		
		JMenuItem mntmCrearLista = new JMenuItem("Crear lista");
		mntmCrearLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VCrearListaInternalFrame = new VCrearLista(f);
				VCrearListaInternalFrame.setVisible(true);
			}
		});
		mnListaDeReproduccion.add(mntmCrearLista);
		
		JMenuItem mntmConsultaLista = new JMenuItem("Consulta Lista");
		mntmConsultaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VConsultaListaInternalFrame = new VConsultaLista(f,null,null);
				VConsultaListaInternalFrame.setVisible(true);
			}
		});
		mnListaDeReproduccion.add(mntmConsultaLista);
		
		JMenuItem mntmAgregarVideoA = new JMenuItem("Agregar video a Lista");
		mntmAgregarVideoA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VAgregarVideoListaInternalFrame = new VAgregarVideoLista(f);
				VAgregarVideoListaInternalFrame.setVisible(true);
			}
		});
		mnListaDeReproduccion.add(mntmAgregarVideoA);
		
		JMenuItem mntmModificarLista = new JMenuItem("Modificar Lista");
		mntmModificarLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarListaInternalFrame = new ModificarLista(f,null,null);
				ModificarListaInternalFrame.setVisible(true);
			}
		});
		mnListaDeReproduccion.add(mntmModificarLista);
		
		JMenu mnVideo = new JMenu("Video");
		menuBar.add(mnVideo);
		
		JMenuItem mntmAltaVideo = new JMenuItem("Alta Video");
		mntmAltaVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaVideoInternalFrame = new AltaVideo(f);
				AltaVideoInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmAltaVideo);
		
		JMenuItem mntmComentarUnVideo = new JMenuItem("Comentar un Video");
		mntmComentarUnVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ComentarVideoInternalFrame = new ComentarVideo(f);
				ComentarVideoInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmComentarUnVideo);
		
		JMenuItem mntmValorarUnVideo = new JMenuItem("Valorar un Video");
		mntmValorarUnVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ValorarVideoInternalFrame = new ValorarVideo(f);
				ValorarVideoInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmValorarUnVideo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Consulta de Video");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaVideoInternalFrame = new ConsultaVideo(f,null, null);
				ConsultaVideoInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmNewMenuItem);
		
		JMenuItem mntmQuitarVideoDe = new JMenuItem("Quitar Video de Lista");
		mntmQuitarVideoDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				QuitarVideoListaInternalFrame = new QuitarVideoLista(f);
				QuitarVideoListaInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmQuitarVideoDe);
		
		JMenuItem mntmModificarDatosVideo = new JMenuItem("Modificar Datos Video");
		mntmModificarDatosVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarVideoInternalFrame = new ModificarVideo(f,null,null);
				ModificarVideoInternalFrame.setVisible(true);
			}
		});
		mnVideo.add(mntmModificarDatosVideo);
		
		JMenu mnUsuario = new JMenu("Usuario");
		menuBar.add(mnUsuario);
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Alta Usuario");
		mntmAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VAltaUsuarioInternalFrame = new vAltaUsuario(f);
				VAltaUsuarioInternalFrame.setVisible(true);
			}
		});
		mnUsuario.add(mntmAltaUsuario);
		
		JMenuItem mntmConsultaDeUsuario = new JMenuItem("Consulta de Usuario");
		mntmConsultaDeUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VConsultaUsuarioInternalFrame = new vConsultaUsuario(f);
				VConsultaUsuarioInternalFrame.setVisible(true);
			}
		});
		mnUsuario.add(mntmConsultaDeUsuario);
		
		JMenuItem mntmListarUsuariosExistentes = new JMenuItem("Listar Usuarios Existentes");
		mntmListarUsuariosExistentes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarUsuariosInternalFrame = new ListarUsuarios(f);
				ListarUsuariosInternalFrame.setVisible(true);
			}
		});
		mnUsuario.add(mntmListarUsuariosExistentes);
		
		JMenuItem mntmListarUsuariosDeBaja = new JMenuItem("Listar Usuarios De Baja");
		mntmListarUsuariosDeBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarUsuariosDeBajaInternalFrame = new ListarUsuariosDeBaja(f);
				ListarUsuariosDeBajaInternalFrame.setVisible(true);
			}
		});
		mnUsuario.add(mntmListarUsuariosDeBaja);
		
		JMenuItem mntmModificarDatosUsuario = new JMenuItem("Modificar Datos Usuario");
		mntmModificarDatosUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarDatosUsuarioInternalFrame = new ModificarDatosUsuario(f);
				ModificarDatosUsuarioInternalFrame.setVisible(true);

			}
		});
		mnUsuario.add(mntmModificarDatosUsuario);
		
		JMenuItem mntmSeguirUnUsuario = new JMenuItem("Seguir un Usuario");
		mntmSeguirUnUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnUsuario.add(mntmSeguirUnUsuario);
		
		JMenuItem mntmDejarDeSeguir = new JMenuItem("Dejar de Seguir un Usuario");
		mntmDejarDeSeguir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnUsuario.add(mntmDejarDeSeguir);
		
		JMenu mnCategoria = new JMenu("Categoria");
		menuBar.add(mnCategoria);
		
		JMenuItem mntmAltaCategoria = new JMenuItem("Alta Categoria");
		mntmAltaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaCategoriaInternalFrame = new altaCategoria(f);
				AltaCategoriaInternalFrame.setVisible(true);
			}
		});
		
		mnCategoria.add(mntmAltaCategoria);
		
		JMenuItem mntmListarCategorias = new JMenuItem("Listar Categorias");
		mntmListarCategorias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListarCategoriaInternalFrame = new ListarCategoria(f);
				ListarCategoriaInternalFrame.setVisible(true);
			}
		});
		
		mnCategoria.add(mntmListarCategorias);
		
		JMenuItem mntmConsultaDeCategoria = new JMenuItem("Consulta de Categoria");
		mntmConsultaDeCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnCategoria.add(mntmConsultaDeCategoria);
		
		
		
	}

}
