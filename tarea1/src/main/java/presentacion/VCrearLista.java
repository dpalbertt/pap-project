package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.util.List;
import java.awt.event.ItemEvent;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Point;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import datatypes.DtLista;
import datatypes.TipoLista;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
//import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import javax.swing.JButton;

public class VCrearLista extends JFrame {
	
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textNombreNormal;
	
	//para ejecutar la ventana sola
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Fabrica f = Fabrica.getInstancia();
	 * IUsuario iu = f.getIControlador(); IElemento ie = f.getControladorElemento();
	 * VCrearLista frame = new VCrearLista(ie, iu); frame.setVisible(true); } catch
	 * (Exception e) { e.printStackTrace(); } } }); }
	 */

	public VCrearLista(Fabrica f) {
		IUsuario iu = f.getIControladorUsuario();
		IElemento ie = f.getControladorElemento();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelOpciones = new JPanel();
		panelOpciones.setBounds(5, 5, 440, 35);
		FlowLayout fl_panelOpciones = (FlowLayout) panelOpciones.getLayout();
		fl_panelOpciones.setAlignment(FlowLayout.LEFT);
		contentPane.add(panelOpciones);
		
		JButton btnPorDefecto = new JButton("Por Defecto");
		
		JPanel panelPorDefecto = new JPanel();
		panelPorDefecto.setBounds(5, 40, 217, 190);
		FlowLayout flowPanel = (FlowLayout) panelPorDefecto.getLayout();
		flowPanel.setHgap(10);
		contentPane.add(panelPorDefecto);
		
		JLabel lblNombre = new JLabel("Nombre: ");
		panelPorDefecto.add(lblNombre);
		
		textField = new JTextField();
		panelPorDefecto.add(textField);
		textField.setColumns(10);
		
		List<String> categorias = iu.listarCategorias();
		List<String> usuarios = iu.listarUsuarios();
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 230, 440, 35);
		contentPane.add(panel);
		
		JButton btnConfirmar = new JButton("Confirmar");
		
		panel.add(btnConfirmar);
		
		JButton btnCancelar = new JButton("Cancelar");
		panel.add(btnCancelar);
		
		JPanel panelNormal = new JPanel();
		panelNormal.setBounds(222, 40, 223, 190);
		contentPane.add(panelNormal);
		panelNormal.setLayout(null);
		
		JLabel lblNombre_1 = new JLabel("Nombre");
		lblNombre_1.setBounds(12, 12, 66, 15);
		panelNormal.add(lblNombre_1);
		
		textNombreNormal = new JTextField();
		textNombreNormal.setBounds(87, 10, 124, 19);
		panelNormal.add(textNombreNormal);
		textNombreNormal.setColumns(10);
		textNombreNormal.setEnabled(false);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(12, 52, 66, 15);
		panelNormal.add(lblUsuario);
		
		JComboBox comboBoxUsuario = new JComboBox(usuarios.toArray());
		comboBoxUsuario.setBounds(87, 47, 124, 24);
		panelNormal.add(comboBoxUsuario);
		comboBoxUsuario.setEnabled(false);
		
		JLabel lblCategoria = new JLabel("Categoria");
		lblCategoria.setBounds(12, 97, 66, 15);
		panelNormal.add(lblCategoria);
		
		JComboBox comboBoxCategoria = new JComboBox(categorias.toArray());
		comboBoxCategoria.setBounds(87, 92, 124, 24);
		panelNormal.add(comboBoxCategoria);
		comboBoxCategoria.setEnabled(false);
		
		JRadioButton radioPrivado = new JRadioButton("Privado");
		radioPrivado.setBounds(12, 137, 144, 23);
		panelNormal.add(radioPrivado);
		radioPrivado.setEnabled(false);
		
		btnPorDefecto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnPorDefecto.getText() == "Por Defecto")
				{
					btnPorDefecto.setText("Particular");
					textField.setEnabled(false);
					
					textNombreNormal.setEnabled(true);
					radioPrivado.setEnabled(true);
					comboBoxCategoria.setEnabled(true);
					comboBoxUsuario.setEnabled(true);
				}
				else
				{
					btnPorDefecto.setText("Por Defecto");
					textNombreNormal.setEnabled(false);
					radioPrivado.setEnabled(false);
					comboBoxCategoria.setEnabled(false);
					comboBoxUsuario.setEnabled(false);
					
					textField.setEnabled(true);
				}
			}
		});
		panelOpciones.add(btnPorDefecto);
		
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnPorDefecto.getText() == "Por Defecto")
				{
					if (!textField.getText().equals("")) {
						/* aca tengo que ver si la lista por defecto ya existe */
						List<String> listasDF = ie.listarPorDefecto();
						if (listasDF.contains(textField.getText())) {
							JOptionPane.showMessageDialog(null, "Esa lista por defecto ya existe");
						} else {
							try {
								String nombre = textField.getText();
								ie.altaListaDf(nombre);
								//meter un pop up the exito y un try catch
								JOptionPane.showMessageDialog(null, "Supuestamente anduvo joya !");
								dispose();
							} catch (Exception e) {
								JOptionPane.showMessageDialog(null, "Algo anduvo mal y paso esto: " + e.getMessage());
							}
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Ingrese datos en los campos obligatorios.");
					}
					
					
				}
				else if (!textNombreNormal.getText().equals("") && comboBoxUsuario.getSelectedIndex() != -1)
				{
					try {
						String nombreLista = textNombreNormal.getText();
						boolean privado = radioPrivado.isSelected();
						DtLista aux = new DtLista(privado, nombreLista, TipoLista.Otra);
						String nombre = comboBoxUsuario.getSelectedItem().toString();
						ie.crearLista(aux, nombre);
						if (comboBoxCategoria.getSelectedIndex() != -1)
						{
							String cat = comboBoxCategoria.getSelectedItem().toString(); // que devuelva string
							ie.asignarCategoria(cat, aux, nombre);
						}
						/* cerrar ventana ? */
						JOptionPane.showMessageDialog(null, "Supuestamente anduvo joya !");
						dispose();
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Algo anduvo mal y paso esto: " + e.getMessage());
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Ingrese datos en los campos obligatorios.");
				}
			}
		});
		
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/* cerrar ventana */
				dispose();
				
			}
		});
	}
}
