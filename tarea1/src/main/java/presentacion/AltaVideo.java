package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.Categoria;
import logica.ManejadorCategoria;
import logica.ManejadorUsuario;
import logica.Usuario;

public class AltaVideo extends JFrame {

	private JPanel contentPane;
	private JTextField nombreVid;
	private JTextField duracion;
	private JTextField url;
	private JTextField descripcion;
	private JTextField nombreUsr;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AltaVideo frame = new AltaVideo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AltaVideo(Fabrica f) {
			IElemento econ = f.getControladorElemento();
			setBounds(100, 100, 450, 383);
			setTitle("Alta Video");
			setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			List<String> categorias = econ.listarCategorias();
			JComboBox comboBox_1 = new JComboBox(categorias.toArray());
			comboBox_1.setVisible(false);
			comboBox_1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {	
					}
			});
			
		
			JRadioButton rdbtnAsignarCategoria = new JRadioButton("Asignar categoria");
			rdbtnAsignarCategoria.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					List<String> categorias = econ.listarCategorias();
					JComboBox comboBox_1 = new JComboBox(categorias.toArray());
					comboBox_1.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {	
							}
					});
					comboBox_1.setVisible(true);
				}
			});
			
			rdbtnAsignarCategoria.setBounds(8, 230, 134, 25);
			getContentPane().add(rdbtnAsignarCategoria);
			
			JLabel lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(8, 63, 56, 16);
			getContentPane().add(lblNombre);
			
			nombreVid = new JTextField();
			nombreVid.setBounds(67, 60, 116, 22);
			getContentPane().add(nombreVid);
			nombreVid.setColumns(10);
			
			JLabel lblDuracion = new JLabel("Duracion:");
			lblDuracion.setBounds(8, 103, 56, 16);
			getContentPane().add(lblDuracion);
			
			duracion = new JTextField();
			duracion.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {

					 try {
						 //econ.esNumero(duracion.getText());
						  }catch(Exception exc) {
						  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
					  }

				}
			});
			duracion.setBounds(67, 100, 56, 22);
			getContentPane().add(duracion);
			duracion.setColumns(10);
			
			JLabel lblUrl = new JLabel("URL:");
			lblUrl.setBounds(8, 132, 56, 16);
			getContentPane().add(lblUrl);
			
			url = new JTextField();
			url.setBounds(43, 132, 140, 22);
			getContentPane().add(url);
			url.setColumns(10);
			
			JLabel lblDescripcion = new JLabel("Descripcion:");
			lblDescripcion.setBounds(8, 168, 79, 16);
			getContentPane().add(lblDescripcion);
			
			descripcion = new JTextField();
			descripcion.setBounds(93, 167, 303, 54);
			getContentPane().add(descripcion);
			descripcion.setColumns(10);
			
			JLabel lblIngresarDatos = new JLabel("Ingresar Datos");
			lblIngresarDatos.setBounds(168, 0, 85, 34);
			getContentPane().add(lblIngresarDatos);
			
			JLabel lblUsuario = new JLabel("Usuario");
			lblUsuario.setBounds(8, 34, 56, 16);
			getContentPane().add(lblUsuario);
			
			nombreUsr = new JTextField();
			nombreUsr.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {

					 try {
						 econ.existeUser(nombreUsr.getText());
						  }catch(Exception exc) {
						  JOptionPane.showMessageDialog(new JFrame(), exc.getMessage(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
					  }

				}
			});
			nombreUsr.setBounds(67, 31, 116, 22);
			getContentPane().add(nombreUsr);
			nombreUsr.setColumns(10);
			
			
			comboBox_1.setBounds(197, 231, 79, 25);
			getContentPane().add(comboBox_1);
		
			JButton btnAceptar_1 = new JButton("Aceptar");
			btnAceptar_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(nombreVid.getText().equals("") || duracion.getText().equals("") || url.getText().equals("") || descripcion.getText().equals("") || nombreUsr.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Ingrese los datos que faltan");
					}else {		
					econ.datosVideo(nombreVid.getText(), Integer.valueOf(duracion.getText()), url.getText(), descripcion.getText(), nombreUsr.getText(),"musica");
					JOptionPane.showMessageDialog(null, "Se realizo el alta del video correctamente");
					dispose();}
				}
			});
			btnAceptar_1.setBounds(168, 298, 97, 25);
			getContentPane().add(btnAceptar_1);
	}
}
