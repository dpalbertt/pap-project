package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import interfaces.Fabrica;
import interfaces.IUsuario;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;

public class ListarCategoria extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarCategoria frame = new ListarCategoria(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListarCategoria(Fabrica f) {
		IUsuario ucon = f.getIControladorUsuario();
		setTitle("Listar Categorias existentes");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JList list = new JList();
		list.setBounds(165, 107, 255, 146);
		contentPane.add(list);
		JButton btnCargar = new JButton("Cargar");
		btnCargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> categorias = ucon.listarCategorias();
				final DefaultListModel model = new DefaultListModel();		   	    
			    for(String c: categorias) {
				    model.addElement(c);
			    }				
			    list.setModel(model);
			}
		});
		btnCargar.setBounds(23, 127, 97, 25);
		contentPane.add(btnCargar);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(23, 190, 97, 25);
		contentPane.add(btnSalir);
		
	}
	
	public static String[] GetStringArray(List<String> categorias) 
    { 
  
        // declaration and initialise String Array 
        String str[] = new String[categorias.size()]; 
  
        // ArrayList to Array Conversion 
        for (int j = 0; j < categorias.size(); j++) { 
  
            // Assign each value to String array 
            str[j] = categorias.get(j); 
        } 
  
        return str; 
    } 
}
