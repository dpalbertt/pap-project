package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JCheckBox;


import datatypes.DtCanal;
import datatypes.DtLista;
import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
//import nu.zoom.swing.desktop.component.filechooser.impl.RegExpFileFilter;
import java.util.List;
import javax.swing.JFileChooser;
import com.toedter.calendar.JDayChooser;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class vAltaUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNickname;
	private JTextField txrtNombre;
	private JTextField txtApellido;
	private JTextField txtEmail;
	private JTextField txtNombreCanal;
	private JTextField txtDescripcionCanal;
	private JTextField textPath;
	private JPasswordField password;



	/**
	 * Create the frame.
	 */
	public vAltaUsuario(Fabrica f) {
		IUsuario iu = f.getIControladorUsuario();
		IElemento ie = f.getControladorElemento();
		setTitle("Alta de Usuario");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 694, 561);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("* Nickname: ");
		lblNewLabel.setBounds(39, 34, 85, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("* Nombre: ");
		lblNombre.setBounds(39, 119, 75, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("* Apellido: ");
		lblApellido.setBounds(39, 162, 56, 14);
		contentPane.add(lblApellido);
		
		JLabel lblEmail = new JLabel("* Email: ");
		lblEmail.setBounds(39, 210, 56, 14);
		contentPane.add(lblEmail);
		
		JLabel lblTipoDeCanal = new JLabel("  Tipo de Canal: ");
		lblTipoDeCanal.setBounds(352, 34, 98, 14);
		contentPane.add(lblTipoDeCanal);
		
		JCheckBox chkPublico = new JCheckBox("Publico");
		chkPublico.setBounds(491, 30, 97, 23);
		contentPane.add(chkPublico);
		
		JLabel lblNewLabel_1 = new JLabel("* Fecha de Nacimiento:");
		lblNewLabel_1.setBounds(39, 252, 145, 14);
		contentPane.add(lblNewLabel_1);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(187, 252, 112, 20);
		contentPane.add(dateChooser);
		
		JLabel lblImagen = new JLabel("Imagen:");
		lblImagen.setBounds(289, 358, 75, 14);
		contentPane.add(lblImagen);
		
		txtNickname = new JTextField();
		txtNickname.setBounds(187, 31, 112, 20);
		contentPane.add(txtNickname);
		txtNickname.setColumns(10);
		
		txrtNombre = new JTextField();
		txrtNombre.setText("");
		txrtNombre.setBounds(187, 116, 112, 20);
		contentPane.add(txrtNombre);
		txrtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(187, 159, 112, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(187, 207, 112, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		
		password = new JPasswordField();
		password.setBounds(187, 72, 112, 20);
		contentPane.add(password);
		
		JButton btnAceptar = new JButton("Confirmar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DtCanal dtc;
				
				if(txtNombreCanal.getText() == "") {
					 dtc = new DtCanal(txtNickname.getText(),chkPublico.isSelected(),txtDescripcionCanal.getText());
				}else {
					 dtc = new DtCanal(txtNombreCanal.getText(),chkPublico.isSelected(),txtDescripcionCanal.getText());
				}
				//CONTROL PARA NICKNAME
				
				if (!txtNickname.getText().equals("") && !txrtNombre.getText().equals("") && !txtApellido.getText().equals("") && !txtApellido.getText().equals("")
					&& !txtNombreCanal.getText().equals("") && !txtDescripcionCanal.getText().equals("") && !password.getText().equals("")) {
					DtUsuario dtu = new DtUsuario(txtNickname.getText(),password.getText(),txrtNombre.getText(),txtApellido.getText(), txtEmail.getText(),
							  dateChooser.getDate(),textPath.getText(), chkPublico.isSelected(),true);
					/* aca tengo que ver si el nick ya existe */
					List<String> usuarios = iu.listarUsuarios();
					if (usuarios.contains(txtNickname.getText())) {
						JOptionPane.showMessageDialog(null, "El usuario ya existe");
					} else {
						try {
							dtu.setCanal(dtc);
							iu.crearUsuario(dtu);
							//meter un pop up the exito y un try catch
							JOptionPane.showMessageDialog(null, "Anduvo o oo o o o o o");
							dispose();
						} catch (Exception ee) {
							JOptionPane.showMessageDialog(null, "Algo anduvo mal y paso esto: " + ee.getMessage());
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Ingrese datos en los campos requeridos.");
				}
				
				
			}

		});
		btnAceptar.setBounds(170, 447, 129, 23);
		contentPane.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(392, 447, 118, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblNombreCanal = new JLabel("* Nombre Canal:   ");
		lblNombreCanal.setBounds(352, 92, 129, 14);
		contentPane.add(lblNombreCanal);
		
		txtNombreCanal = new JTextField();
		txtNombreCanal.setBounds(491, 89, 96, 20);
		contentPane.add(txtNombreCanal);
		txtNombreCanal.setColumns(10);
		
		JLabel lblDes = new JLabel("* Descripcion Canal: ");
		lblDes.setBounds(352, 136, 145, 14);
		contentPane.add(lblDes);
		
		txtDescripcionCanal = new JTextField();
		txtDescripcionCanal.setBounds(492, 133, 96, 20);
		contentPane.add(txtDescripcionCanal);
		txtDescripcionCanal.setColumns(10);
		
		JLabel lblCamposRequeridos = new JLabel("* Campos requeridos");
		lblCamposRequeridos.setBounds(39, 358, 145, 14);
		contentPane.add(lblCamposRequeridos);
		
		JButton btnElegirImagen = new JButton("Seleccionar");
		btnElegirImagen.setBounds(538, 353, 118, 25);
		contentPane.add(btnElegirImagen);
		
		textPath = new JTextField();
		textPath.setEnabled(false);
		textPath.setBounds(352, 355, 158, 20);
		contentPane.add(textPath);
		textPath.setColumns(10);
		
		JLabel lblcontrasea = new JLabel("*Contrase\u00F1a:");
		lblcontrasea.setBounds(39, 75, 85, 14);
		contentPane.add(lblcontrasea);
		
		
		
		btnElegirImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setAcceptAllFileFilterUsed(false);
				FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG and JPG images", "png", "jpg", "jpeg");
				jfc.setFileFilter(filter);
				int returnValue = jfc.showOpenDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					String selectedFile = jfc.getSelectedFile().getAbsolutePath();
					textPath.setText(selectedFile);
				}
			}
		});
		
	}
}