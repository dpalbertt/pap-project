package datatypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class DtCanal {
	public DtCanal() {
		super();
	}

	private String nombre;
	private boolean publico;
	private String descripcion;


public DtCanal(String nombre, boolean publico, String descripcion) {
	super();
	this.nombre = nombre;
	this.publico = publico;
	this.descripcion = descripcion;
	}

public String getNombre() {
	return nombre;
}



public boolean isPublico() {
	return publico;
}



public String getDescripcion() {
	return descripcion;
}


}