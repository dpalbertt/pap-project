package datatypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class DtVideo extends DtElemento{
	
	public List<DtComentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<DtComentario> comentarios) {
		this.comentarios = comentarios;
	}
	
	public DtVideo() {
		super();
	}

	private int duracion;
	private String url;
	private String descripcion;
	private Date fechaPublicacion;
	private List<DtComentario> comentarios = new ArrayList<DtComentario>();
	
	public DtVideo(boolean publico, String nombre, int duracion, String url, String descripcion, Date fechaPublicacion) {
		super(publico, nombre);
		this.duracion = duracion;
		this.url = url;
		this.descripcion = descripcion;
		this.fechaPublicacion = fechaPublicacion;
	}

	public int getDuracion() {
		return duracion;
	}

	public String getUrl() {
		return url;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void addComentario(DtComentario comentario) {
		this.comentarios.add(comentario);
	}
}