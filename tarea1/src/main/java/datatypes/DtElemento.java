package datatypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class DtElemento {
	final private boolean publico;
	final private String nombre;
	
	public DtElemento(boolean publico, String nombre) {
		super();
		this.publico = publico;
		this.nombre = nombre;
	}
	
	public DtElemento() {
		super();
		this.publico = false;
		this.nombre = "";
	}

	public boolean isPublico() {
		return publico;
	} 

	public String getNombre() {
		return nombre;
	}

}