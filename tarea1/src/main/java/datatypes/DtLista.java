package datatypes;

import java.util.ArrayList;
import java.util.List;

import logica.Categoria;
import logica.Video;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class DtLista extends DtElemento {
	private List<DtVideo> dtVideos = new ArrayList<DtVideo>();
	private TipoLista tipo;
	private String categoria;
	
	public DtLista() {
		super();
	}
	
	public DtLista(boolean publico, String nombre, TipoLista tipo) {
		super(publico, nombre);
		this.tipo = tipo;
	}
	public DtLista(boolean publico, String nombre, TipoLista tipo, String categoria, List<Video> videos)
	{
		super(publico, nombre);
		this.tipo = tipo;
		this.categoria = categoria;
		for (Video vid : videos)
		{
			DtVideo aux = new DtVideo(vid.isPublico(), vid.getNombre(), vid.getDuracion(), vid.getUrl(), vid.getDescripcion(), vid.getFechaPublicacion());
			this.dtVideos.add(aux);
		}
	}
	
	public String getTipo() {
		if (this.tipo == TipoLista.PorDefecto)
			return "Por Defecto";
		else
			return "Comun";
	}
	
	public List<String> listarDtvideo() {
		List<String> videos = new ArrayList<String>();
		for (DtVideo aux : this.dtVideos) {
			videos.add(aux.getNombre());
		}
		return videos;
	}
	
	public String getCategoria() {
		return this.categoria;
	}

}
