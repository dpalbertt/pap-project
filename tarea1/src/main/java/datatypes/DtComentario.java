package datatypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class DtComentario {

	private String descripcion;
	private Date fecha;
	private String nickname;
	
	private List<DtComentario> comentarios = new ArrayList<DtComentario>();
	
	public DtComentario() {
		super();
	}
	
	public DtComentario(String descripcion, Date fecha, String nickname) {
		super();
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.nickname = nickname;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFecha() {
		return fecha;
	}
	
	public void addComentario(DtComentario comentario) {
		this.comentarios.add(comentario);
	}
	
	public void setComentarios(List<DtComentario> comentarios) {
		this.comentarios = comentarios;
	}

	public List<DtComentario> getComentarios() {
		return comentarios;
	}
	
	public String getNickname() {
		return nickname;
	}

}