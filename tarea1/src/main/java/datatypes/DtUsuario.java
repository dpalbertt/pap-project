package datatypes;

import java.awt.Image;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class DtUsuario {
	private String nickname;
	private String password;
	private String nombre;
	private String apellido;
	private String email;
	private Date fNacimiento;
	private String foto;
	private boolean publico;
	private boolean habilitado;
	private DtCanal canal;
	
	public DtUsuario(String nickname, String password, String nombre, String apellido, String email, Date fNacimiento, String foto, boolean publico, boolean habilitado) {
		super();
		this.nickname = nickname;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.fNacimiento = fNacimiento;
		this.foto = foto;
		this.publico = publico;
		this.habilitado = habilitado;
	}
	
	public DtUsuario() {
		
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setfNacimiento(Date fNacimiento) {
		this.fNacimiento = fNacimiento;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public void setPublico(boolean publico) {
		this.publico = publico;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getNickname() {
		return nickname;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}

	public String getEmail() {
		return email;
	}

	public Date getfNacimiento() {
		return fNacimiento;
	}

	public String getFoto() {
		return foto;
	}

	public boolean isPublico() {
		return publico;
	}
	
	public boolean isHabilitado() {
		return habilitado;
	}
	
	public DtCanal getCanal() {
		return canal;
	}

	public void setCanal(DtCanal canal) {
		this.canal = canal;
	}

}