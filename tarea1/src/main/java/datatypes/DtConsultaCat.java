package datatypes;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class DtConsultaCat {

	private String NombreUsuario;
	private String VideoOlista;
	
	public DtConsultaCat() {
		super();
	}
	
	public DtConsultaCat(String nombreUsuario, String videoOlista) {
		super();
		NombreUsuario = nombreUsuario;
		VideoOlista = videoOlista;
	}
		
}
