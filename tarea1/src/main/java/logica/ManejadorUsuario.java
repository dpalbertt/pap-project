package logica;


import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Session;

public class ManejadorUsuario {
	private static ManejadorUsuario instancia = null;
	
	private static Conexion conexion = null;
	
	private ManejadorUsuario(){

		if( conexion == null )
			conexion = Conexion.getInstancia();
	}
	
	public static ManejadorUsuario getInstancia() {
		if (instancia == null)
			instancia = new ManejadorUsuario();
		return instancia;
	}
	public void agregarUsuario(Usuario usu) {
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(usu);
		em.getTransaction().commit();
	}
	
	public void quitarUsuario(String usu) {
		EntityManager em = conexion.getEntityManager();
		Usuario usuario = buscarUsuario(usu);
		System.out.println("Voy a borrar al usuario "+usuario.getNickname());
		em.joinTransaction();
		em.getTransaction().begin();
		em.remove(usuario);
		em.getTransaction().commit();
	}
	
	public void actualizarUsuario(Usuario u) {
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.persist(u);
		t.commit();
		session.disconnect();
	}
	
	public Usuario buscarUsuario(String nick){
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		System.out.println(nick);
		return em.find(Usuario.class, nick);
	}
	public List<Usuario> obtenerUsuarios()
	{
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		List<Usuario> users = session.createQuery("SELECT a FROM Usuario a", Usuario.class).getResultList();
		return users;
	}
	
	
	//Lista Usuario habilitados
	public List<String> listarUsuarios(){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		List<String> users = session.createQuery("SELECT a.nickname FROM Usuario a WHERE a.habilitado <> false", String.class).getResultList();
		session.disconnect();
		return users;
	}
	
	
	//Lista todos los usuarios
		public List<String> listarUsuariosTodos(){
			Conexion conexion = Conexion.getInstancia();
			EntityManager em = conexion.getEntityManager();
			org.hibernate.Session session = (Session)em.getDelegate();
			List<String> users = session.createQuery("SELECT a.nickname FROM Usuario a", String.class).getResultList();
			session.disconnect();
			return users;
		}
		
		//Lista Usuario de baja
		public List<String> listarUsuariosBaja(){
			Conexion conexion = Conexion.getInstancia();
			EntityManager em = conexion.getEntityManager();
			org.hibernate.Session session = (Session)em.getDelegate();
			List<String> users = session.createQuery("SELECT a.nickname FROM Usuario a WHERE a.habilitado is false", String.class).getResultList();
			session.disconnect();
			return users;
		}
	
	public void asignarLista(String nick,List<PorDefecto> porDefecto) {
	
		Usuario u = buscarUsuario(nick);
		for (PorDefecto aux : porDefecto) {
		u.getCanal().addElemento(aux);
		}	
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.update(u);
		t.commit();
		session.disconnect();
	}
	
	public void seguirUsuario(String usuarioSeguidor, String usuarioASeguir) {
		
		Usuario usuarioS = buscarUsuario(usuarioSeguidor);
		Usuario usuarioAS = buscarUsuario(usuarioASeguir);
		
		usuarioS.seguirUsuario(usuarioAS); 
    	
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.update(usuarioS);
		t.commit();
		session.disconnect();
		
		
	}
}