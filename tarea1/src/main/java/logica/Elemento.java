package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Elemento {
	private boolean publico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idElemento;
	
	//static private int idAuto = 0;
	
	private String nombre;
	
	@ManyToOne
	private Categoria categoria;
	
	public Elemento() {
		
	}
	
	public Elemento(boolean publico, String nombre) {
		super();
		this.publico = publico;
		this.nombre = nombre;
	}
	public boolean isPublico() {
		return publico;
	}
	public void setPublico(boolean publico) {
		this.publico = publico;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setCategoria(Categoria cat) {
		this.categoria = cat;
	}
	
	public Categoria getCategorias() {
		
		return this.categoria;
	}
	
	
}
