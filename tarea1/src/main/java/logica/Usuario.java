package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import datatypes.DtVideo;


@Entity	
public class Usuario {

	@Id	
	private String nickname;
	private String password;
	private String nombre;
	private String apellido;
	private String email;
	private Date fecha;
	private String imagen;
	private boolean publico;
	private boolean habilitado;
	
	@OneToOne(mappedBy="usuario",cascade=CascadeType.ALL,orphanRemoval=true,fetch=FetchType.LAZY)
	private Canal canal;
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<MeGusta> megusta = new ArrayList<MeGusta>();

	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Usuario> usuariosSeguidos = new ArrayList<Usuario>();
	
	@OneToMany(mappedBy="nickname",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Usuario> usuariosAmigos;

	public Usuario(){
		
	}

	public Usuario(String nickname, String password, String nombre, String apellido, String email, Date fecha, String imagen,boolean publico, boolean habilitado) {
		super();
		this.nickname = nickname;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.fecha = fecha;
		this.imagen = imagen;
		this.publico = publico;
		this.habilitado = habilitado;
	}
	
	public String getNickname() {
		return nickname;
	}
		public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public boolean isPublico() {
		return publico;
	}
	
	public void SetIsPublico(boolean publico) {
		this.publico = publico;
	}
	
	public boolean isHabilitado() {
		return habilitado;
	}
	
	public void SetIsHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public Canal getCanal() {
		return canal;
	}

	public void setCanal(Canal canal) {
		this.canal = canal;
	}

	public List<MeGusta> getMegusta() {
		return megusta;
	}

	public void setMegusta(List<MeGusta> megusta) {
		this.megusta = megusta;
	}

	public void crearCanal(Canal canal) {
		canal.setUsuario(this);
		this.canal = canal;
	} 
	
	public void addMeGusta(MeGusta megusta) {
		this.megusta.add(megusta);
	}
	
	public List<Usuario> getSeguidos(){
		return this.usuariosSeguidos;
	}
	
	//LISTARVIDEOS
	
	public List<DtVideo> ListarVideos(String nick) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario usuario = mU.buscarUsuario(nick);
		List<DtVideo> videos = new ArrayList();
		for (Elemento vid : usuario.getCanal().getElementos()) {
			if (vid instanceof Video) {
				DtVideo aux = new DtVideo(vid.isPublico(), vid.getNombre(),
						((Video) vid).getDuracion(), ((Video) vid).getUrl(),
						((Video) vid).getDescripcion(), ((Video) vid).getFechaPublicacion());
				videos.add(aux);
			}
		}
		return videos;
	}
			
	// SEGUIR O DEJAR DE SEGUIR
			
	public void addSeguido(Usuario u){
		this.usuariosSeguidos.add(u);
	}
	
	public void quitarSeguido(Usuario u) {
		this.usuariosSeguidos.remove(u);
	}



	public boolean soySeguidor(String nickname) {
		boolean soySeguidor = false;
		int i = 0;
		while(!soySeguidor && i<this.usuariosSeguidos.size()) {
			if(this.usuariosSeguidos.get(i).getNickname().equals(nickname)) {
				soySeguidor = true;
			}
		}
		return soySeguidor;
	}
	
	public void agregarMeGusta(MeGusta megusta) {
		this.megusta.add(megusta);
	}
	
	
	public void seguirUsuario(Usuario usuario) {
		this.usuariosSeguidos.add(usuario);
	}

}
