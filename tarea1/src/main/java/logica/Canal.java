package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import datatypes.DtLista;
import datatypes.DtVideo;
import datatypes.TipoLista;

@Entity
public class Canal {
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String nombre;
	private boolean publico;
	private String descripcion;

	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Elemento> elementos = new ArrayList<Elemento>();
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Categoria> categorias;
	
	@OneToOne
	private Usuario usuario;

	public Canal() {

	}

	public Canal(String nombre, boolean publico, String descripcion) {
		super();
		this.nombre = nombre;
		this.publico = publico;
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isPublico() {
		return publico;
	}
	public void setPublico(boolean publico) {
		this.publico = publico;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void addElemento(ListaDeReproduccion lista) {
		this.elementos.add(lista);
	}
	

	public Video buscarVideo(String nomVid) {
		Video aux = null;
		for (Elemento iter : this.elementos)
		{
			if (iter instanceof Video)
				if (iter.getNombre().equals(nomVid))
					aux = (Video) iter;
		}
		return aux;
	}
	public DtVideo buscarVideoDt(String nomVid) {
		DtVideo aux = null;
		for (Elemento iter : this.elementos)
		{
			if (iter instanceof Video)
				if (iter.getNombre().equals(nomVid))
					aux = new DtVideo(iter.isPublico(),iter.getNombre(),((Video) iter).getDuracion(),((Video) iter).getUrl(),((Video) iter).getDescripcion(),((Video) iter).getFechaPublicacion());
		}
		return aux;
	}

	public ListaDeReproduccion buscarLista(String nomList) {
		ListaDeReproduccion aux = null;
		for (Elemento iter : this.elementos)
		{
			if (iter instanceof ListaDeReproduccion)
				if (iter.getNombre().equals(nomList))
					aux = (ListaDeReproduccion) iter;
		}
		return aux;
	} /* no se si esta funcion se usa */
	
	public DtLista buscarListaDT(String nomList) {
		DtLista aux = null;
		for (Elemento iter : this.elementos)
		{
			if (iter instanceof ListaDeReproduccion)
				if (iter.getNombre().equals(nomList)) {
					TipoLista tip;
					if (iter instanceof PorDefecto) {
						tip = TipoLista.PorDefecto;
					}
					else {
						tip = TipoLista.Otra;
					}
					String categoria;
					if (iter.getCategorias() != null)
						categoria = iter.getCategorias().getNombre();
					else
						categoria = "";
					aux = new DtLista(iter.isPublico(), iter.getNombre(), tip, categoria, ((ListaDeReproduccion) iter).getVideos());
				}
					
		}
		return aux;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void addElemento(Video video) {
		this.elementos.add(video);
	}
	
	public List<String> listarVideos() {
		List<String> videos = new ArrayList();
		for (Elemento e : this.elementos) {
			if (e instanceof Video) {
				videos.add(e.getNombre());
			}
		}
		return videos;
	}
	
	public List<String> listarListas() {
		List<String> listas = new ArrayList();
		for (Elemento e : this.elementos) {
			if (e instanceof ListaDeReproduccion) {
				listas.add(e.getNombre());
			}
		}
		return listas;
	}
}