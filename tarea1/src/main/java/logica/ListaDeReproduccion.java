package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("LR")

public class ListaDeReproduccion extends Elemento {

	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Video> videos = new ArrayList<Video>();
	
	
	
	public ListaDeReproduccion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ListaDeReproduccion(boolean publico, String nombre) {
		super(publico, nombre);
		// TODO Auto-generated constructor stub
	}
	public List<Video> getVideos()
	{
		return this.videos;
	}

	public void addVideo(Video auxVid) {
		this.videos.add(auxVid);
		
	}
	public void removeVideo(Video video) {
		this.videos.remove(video);
	}
	
}
