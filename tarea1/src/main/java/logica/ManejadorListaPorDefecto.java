package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;

public class ManejadorListaPorDefecto {
	private static ManejadorListaPorDefecto instancia = null;
	private List<PorDefecto> listasDf;
	
	private ManejadorListaPorDefecto(){}
	
	public static ManejadorListaPorDefecto getInstancia() {
		if (instancia == null)
			instancia = new ManejadorListaPorDefecto();
		return instancia;
	}
	
	public void agregarListaDF(PorDefecto usu) {
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(usu);
		em.getTransaction().commit();
	}
	
	public PorDefecto buscarListaDf(String nombreListaDf) {
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		return em.find(PorDefecto.class, nombreListaDf);
	}
	
	public void quitarListaDF(String nombreListaDf) {
		PorDefecto lista = buscarListaDf(nombreListaDf);
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.remove(lista);
		em.getTransaction().commit();
	}
	
	
	public List<PorDefecto> obtenerListas()
	{
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		List<Elemento> listasElementos = session.createQuery("SELECT a FROM Elemento a", Elemento.class).getResultList();
		List<PorDefecto> porDefecto = new ArrayList<PorDefecto>();
		for (Elemento aux : listasElementos) {
			if (aux instanceof PorDefecto) {
				porDefecto.add((PorDefecto) aux);
			}
		}
		return porDefecto;

	}
}
