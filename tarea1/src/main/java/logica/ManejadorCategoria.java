package logica;

import java.util.List;

import org.hibernate.Session;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;

public class ManejadorCategoria {

private static ManejadorCategoria instancia = null;
	
	private ManejadorCategoria(){}
	
	public static ManejadorCategoria getInstancia() {
		if (instancia == null)
			instancia = new ManejadorCategoria();
		return instancia;
	}
	public void agregarCategoria(Categoria cat) {
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(cat);
		em.getTransaction().commit();
	}
	
	public Categoria buscarCategoria(String nombre){
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		return em.find(Categoria.class, nombre);
	}

	
	public void eliminarCategoria(String nombre) {
		Categoria categoria = buscarCategoria(nombre);
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.remove(categoria);
		em.getTransaction().commit();
	}
	
	
	public List<String> listarCategorias(){
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		List<String> cats = session.createQuery("SELECT c.nombreCat FROM Categoria c", String.class).getResultList();
		return cats;
	}
}
