package logica;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.Transaction;

import datatypes.DtCanal;
import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import interfaces.IUsuario;



public class CUsuario implements IUsuario {
	private Image imagen;
	
	public void guardarImagen(Image origImage) {
		this.imagen = origImage;
	}
	
	public Image getImagen() {
		return this.imagen;
	}
			
	
	public boolean soySeguidor(String nickSeguidor, String seguido) {
		boolean dummy = false;
		Usuario user = findUsuarioByNickname(seguido);
		if (user.soySeguidor(nickSeguidor))
			dummy = true;
		return dummy;
	}
	public void crearUsuarioL(DtUsuario datosU, DtLista datosL){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conexion");
		EntityManager em = emf.createEntityManager();
		Usuario user = new Usuario(datosU.getNickname(), datosU.getPassword(), datosU.getNombre(), datosU.getApellido(),datosU.getEmail(), datosU.getfNacimiento(),datosU.getFoto(), datosU.isPublico(), datosU.isHabilitado());
		DtCanal canal = datosU.getCanal();
		Canal c1 = new Canal(canal.getNombre(),canal.isPublico(),canal.getDescripcion());
		user.crearCanal(c1);
		ManejadorUsuario mUsuario = ManejadorUsuario.getInstancia();
		mUsuario.agregarUsuario(user);	
		asignarListas(user.getNickname());
		
		ListaDeReproduccion ldr = new ListaDeReproduccion(datosL.isPublico(),datosL.getNombre());
		user.getCanal().addElemento(ldr);
		mUsuario.agregarUsuario(user);
	}

	public void asignarListas(String nick) {
		ManejadorListaPorDefecto l = ManejadorListaPorDefecto.getInstancia();
		List<PorDefecto> lpd = l.obtenerListas();
		ManejadorUsuario mUsuario = ManejadorUsuario.getInstancia();
		mUsuario.asignarLista(nick,lpd);
		}
	
	
	@Override
	public void altaCategoria(String nombre) {
		//nombre = VentanaAltaCategoria.nombre.getText();
		ManejadorCategoria mC = ManejadorCategoria.getInstancia();
		Categoria cat = new Categoria(nombre);
		mC.agregarCategoria(cat);
		
		
	}
	

	
	public DtUsuario  getUsuario(String nickname) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nickname);
		DtCanal dtc = new DtCanal(user.getCanal().getNombre(),user.getCanal().isPublico(),user.getCanal().getDescripcion());
		DtUsuario dtu = new DtUsuario(user.getNickname(), user.getPassword(), user.getNombre(),user.getApellido(), user.getEmail(),user.getFecha(), user.getImagen(), user.getCanal().isPublico(), user.isHabilitado());
		dtu.setCanal(dtc);
		return dtu;
	}

	public List<String> listarCategorias(){
		ManejadorCategoria mC= ManejadorCategoria.getInstancia();
		List<String> categorias = new ArrayList<String>();
		categorias = mC.listarCategorias();
		return categorias;
	}


	public Usuario findUsuarioByNickname(String nickname) {
		
		EntityManagerFactory entityFactory = Persistence.createEntityManagerFactory("Conexion");
		EntityManager entityManager = entityFactory.createEntityManager();
		Usuario userResult = entityManager.find(Usuario.class, nickname);
		return userResult;
		
	}

	
	public void seguirUsuario(String usuarioSeguidor, String usuarioASeguir) {
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		mu.seguirUsuario(usuarioSeguidor, usuarioASeguir);
		
	}
	
	public void dejarSeguirUsuario(String seguidor, String seguido) {
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		Usuario follower = mu.buscarUsuario(seguidor);
		Usuario userSeguido = mu.buscarUsuario(seguido);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		follower.quitarSeguido(userSeguido);
		session.update(follower);
		session.update(userSeguido);
		t.commit();
		session.disconnect();
	}
	//LISTAR USUARIOS
	public List<String> listarUsuarios(){
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		return mU.listarUsuarios();
	}
	
	//LISTAR USUARIOS TOTALES
		public List<String> listarUsuariosTodos(){
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			return mU.listarUsuariosTodos();
		}
		
		//LISTAR USUARIOS DE BAJA
				public List<String> listarUsuariosBaja(){
					ManejadorUsuario mU = ManejadorUsuario.getInstancia();
					return mU.listarUsuariosBaja();
				}
	//LISTAR USUARIOS QUE SIGO
		public List<String> listarUsuariosQueSigo( Usuario u){
			List<String> UsuariosALosQueSigo = new ArrayList<String>();
			for(Usuario user : u.getSeguidos()) {
				UsuariosALosQueSigo.add(user.getNickname());
			}
			return UsuariosALosQueSigo;
		}
		
	//LISTAR MIS SEGUIDORES
		public List<String> misSeguidores(Usuario u){
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			List<Usuario> usuarios = mU.obtenerUsuarios();
			List<String> misSeguidores = new ArrayList<>();
			for (Usuario aux : usuarios) {
				for (Usuario aux2 : aux.getSeguidos()) {
					if (aux2.getNickname().equals(u.getNickname())) {
						misSeguidores.add(aux.getNickname());
					}
				}
			}
			return misSeguidores;
		}
	
		public void crearUsuario(DtUsuario datosU){
			Usuario user = new Usuario(datosU.getNickname(), datosU.getPassword(), datosU.getNombre(), datosU.getApellido(),datosU.getEmail(), datosU.getfNacimiento(),datosU.getFoto(), datosU.isPublico(),true);
			DtCanal canal = datosU.getCanal();
			Canal c1 = new Canal(canal.getNombre(),canal.isPublico(),canal.getDescripcion());
			user.crearCanal(c1);
			ManejadorUsuario mUsuario = ManejadorUsuario.getInstancia();
			mUsuario.agregarUsuario(user);	
			asignarListas(user.getNickname());
		}


		
		public void modificarUsuario(DtUsuario dtu, String nick, String nuevoPassword, String nuevoNombre, String nuevoApellido, Date fecha, String imagen,boolean publico,
				String nombreCanal, String descripcion,boolean habilitado) {
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			Usuario user = mU.buscarUsuario(nick);
			Conexion conexion = Conexion.getInstancia();
			EntityManager em = conexion.getEntityManager();
			System.out.println(dtu.getPassword() + dtu.getNombre() + dtu.getApellido() + dtu.getfNacimiento() + dtu.getFoto() + dtu.isPublico());
			user.setPassword(nuevoPassword);
			user.setNombre(nuevoNombre);
			user.setApellido(nuevoApellido);
			user.setFecha(fecha);
			user.setImagen(imagen);
			user.SetIsHabilitado(habilitado);
			user.getCanal().setPublico(publico);
			user.getCanal().setNombre(nombreCanal);
			user.getCanal().setDescripcion(descripcion);
				org.hibernate.Session session = (Session)em.getDelegate();
				Transaction t = session.beginTransaction();
				session.persist(user);
				t.commit();
				session.disconnect();
								
			}
		
		
		//BAJA USUARIO
		public void bajaUsuario(DtUsuario dtu, boolean habilitado) {
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			mU.quitarUsuario(dtu.getNickname());
			Usuario user = new Usuario(dtu.getNickname(),dtu.getPassword(), dtu.getNombre(), dtu.getApellido(), dtu.getEmail(), new Date(), "", dtu.isPublico(), false);
			Canal dtc = new Canal(dtu.getCanal().getNombre(),dtu.getCanal().isPublico(),dtu.getCanal().getDescripcion());
			user.crearCanal(dtc);
			mU.agregarUsuario(user);
								
			}
		public List<String> listarListaDeUsuario(String nickname) {
			
			Usuario usuario = this.findUsuarioByNickname(nickname);
			List<Elemento> elementosCanalUsuario = usuario.getCanal().getElementos();
			return obtenerNombresLista(elementosCanalUsuario);
			
		}
		private List<String> obtenerNombresLista(List<Elemento> elementosUsuario) {
			
			List<String> resultado = new ArrayList<String>();
			for(Elemento elemento: elementosUsuario) {
				if(elemento instanceof ListaDeReproduccion)
					resultado.add(elemento.getNombre());
			}
			
			return resultado;
		}
		
		
		public void datosUsuario(String nick, String password, String email, String nuevoNombre, String nuevoApellido, Date nuevaFecha, String nuevaImagen,boolean publico,String nombreCanal,
				String descripcion) {
			System.out.println(nick + password + email + nuevoNombre + nuevoApellido + nuevaFecha + nuevaImagen + publico + nombreCanal + descripcion);
			DtCanal dtc = new DtCanal(nombreCanal,publico,descripcion);
			DtUsuario dtu = new DtUsuario(nick,password,nuevoNombre,nuevoApellido,email,nuevaFecha,nuevaImagen,publico,true);
			modificarUsuario(dtu,nick,password,nuevoNombre,nuevoApellido,nuevaFecha,nuevaImagen,publico,nombreCanal,descripcion,true);
		
		}

		public boolean existeUsuario(String usuario) {			
			Usuario u= findUsuarioByNickname(usuario);
			if(u == null) {
				return false;
			}else {				
				return true;
			}
		}
		public boolean iniciarSesion(String usuario, String password) {
			if(existeUsuario(usuario)) {
				if(password.equals(findUsuarioByNickname(usuario).getPassword())) {
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}

}