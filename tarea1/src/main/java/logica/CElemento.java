package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;

import datatypes.DtComentario;
import datatypes.DtConsultaCat;
import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import datatypes.Favorito;
import datatypes.TipoLista;
import interfaces.IElemento;

public class CElemento implements IElemento {

	public void altaListaDf(String nombre)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		List<Usuario> users = mU.obtenerUsuarios();
		/* obtengo todos los usuarios y le agrego la lista */
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		for (Usuario u : users)
		{
			/* creo la lista */
			session.persist(u);
			ListaDeReproduccion lista = new PorDefecto(false, nombre);
			session.persist(lista);
			u.getCanal().addElemento(lista);
		}
		t.commit();
		session.disconnect();
	}

	public void crearLista(DtLista lista, String nick)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);
		ListaDeReproduccion aux = new ListaDeReproduccion(lista.isPublico(), lista.getNombre());
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(user);
		user.getCanal().addElemento(aux);
		em.getTransaction().commit();
	}

	public DtUsuario seleccionarUsuario(String nick)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);

		DtUsuario aux = new DtUsuario(user.getNickname(), user.getPassword(), user.getNombre(),user.getApellido(), user.getEmail(),user.getFecha(), user.getImagen(), user.getCanal().isPublico(),user.isHabilitado());
		return aux;
	}

	public void asignarCategoria(String nombreCat, DtLista lista, String nick)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);
		ListaDeReproduccion aux = user.getCanal().buscarLista(lista.getNombre());
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		Categoria cat = mc.buscarCategoria(nombreCat);
		aux.setCategoria(cat);
		cat.addElemento(aux);
		
		//persistencia
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.update(user);
		session.update(cat);
		t.commit();
		session.disconnect();
		//DtLista devuelvo = new DtLista(aux.isPublico(), aux.getNombre(),TipoLista.Otra, aux.getVideos());
		
	}

	public DtVideo getUserVideo(String video, String nickname) {

		Usuario user = getUser(nickname);
		Video videoSearched = searchVideoInChannel(user.getCanal(), video);
		DtVideo result = null;
		if(videoSearched == null) {
			System.out.println("El video es null, habria que tirar una excepcion");
		}else {
			result = new DtVideo(videoSearched.isPublico(), videoSearched.getNombre(), videoSearched.getDuracion(), videoSearched.getUrl(), videoSearched.getDescripcion(), videoSearched.getFechaPublicacion());
		}
		return result;
	}
	
	public Video getVideo(String video, String nickname) {
		Video videoSearched = searchVideoInChannel(getUser(nickname).getCanal(), video);
		return videoSearched;
	}

	private Video searchVideoInChannel(Canal channel, String video) {

		List<Elemento> elements = channel.getElementos();
		Video result = null;
		for(Elemento element: elements) {
			if(element instanceof Video) {
				if(element.getNombre().equals(video)) {
					result = (Video) element;
					break;
				} 
			}
		}
		return result;
	}


	public List<DtVideo> listarVideos(DtUsuario dtUser)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario usuario = mU.buscarUsuario(dtUser.getNickname());
		List<DtVideo> videos = new ArrayList();
		for (Elemento vid : usuario.getCanal().getElementos()) {
			if (vid instanceof Video) {
				DtVideo aux = new DtVideo(vid.isPublico(), vid.getNombre(),
						((Video) vid).getDuracion(), ((Video) vid).getUrl(),
						((Video) vid).getDescripcion(), ((Video) vid).getFechaPublicacion());
				videos.add(aux);
			}
		}
		return videos;
	}

	public List<DtLista> listarListas(DtUsuario dtUser)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario usuario = mU.buscarUsuario(dtUser.getNickname());
		List<DtLista> listas = new ArrayList<DtLista>();
		for (Elemento listita : usuario.getCanal().getElementos()) {
			if (listita instanceof ListaDeReproduccion) {
				String categoria = "";
				if (listita.getCategorias() != null)
					categoria = listita.getCategorias().getNombre();
				DtLista aux = new DtLista(listita.isPublico(), listita.getNombre(), TipoLista.Otra, categoria, ((ListaDeReproduccion) listita).getVideos());
				
				listas.add(aux);
			}
		}
		return listas;
	}
	public ListaDeReproduccion buscarLista(String usuario, String nombreL) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(usuario);
		ListaDeReproduccion aux = user.getCanal().buscarLista(nombreL);		
		return aux;
	}
	
	public DtLista buscarListaDT(String usuario, String nombreL) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(usuario);
		DtLista dtL;
		if (user.getCanal().buscarListaDT(nombreL) != null)
			dtL = user.getCanal().buscarListaDT(nombreL);
		else
			dtL = new DtLista();
		return dtL;
	}
	
	public void modificarLista(String nombreL,String nick, boolean publico, String cat) {
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		Categoria categoria = mc.buscarCategoria(cat);
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);
		ListaDeReproduccion lista = buscarLista(nick,nombreL);
		Categoria catVieja = lista.getCategorias();
		lista.setCategoria(categoria);
		if (catVieja != null) catVieja.quitarElemento(lista);
		if (categoria != null) categoria.addElemento(lista);
		lista.setPublico(publico);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.persist(lista);
		session.persist(user);
		if (catVieja != null) session.persist(catVieja);	
		if (categoria != null) session.persist(categoria);
		t.commit();
		session.disconnect();
	}
	public DtLista seleccionarLista(String nombreList, String nick)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);
		ListaDeReproduccion aux = user.getCanal().buscarLista(nombreList);
		String categoria = "";
		if (aux.getCategorias() != null)
			categoria = aux.getCategorias().getNombre();
		DtLista devuelvo = new DtLista(aux.isPublico(), aux.getNombre(), TipoLista.Otra, categoria, aux.getVideos());
		/* hice un constructor que toma la lista de videos y construye dtVideos */
		return devuelvo;

	}

	public void agregarVideoALista(String nickVideo, String nickLista, String nomVid, String nomList)
	{
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario usuarioVideo = mU.buscarUsuario(nickVideo);
		Video auxVid = usuarioVideo.getCanal().buscarVideo(nomVid);
		Usuario usuarioLista = mU.buscarUsuario(nickLista);
		ListaDeReproduccion auxList = usuarioLista.getCanal().buscarLista(nomList);
		auxList.addVideo(auxVid);
		
		//persistencia
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.persist(auxList);
		session.persist(usuarioLista);
		t.commit();
		session.disconnect();
	}
	public List<String> listarCategorias(){
	ManejadorCategoria mc = ManejadorCategoria.getInstancia();
	List<String> categorias = mc.listarCategorias();
	return categorias;
}

	public List<String> listarVideoUsuario(String nick){
		List<Video> userVideos = getUserVideos(nick);
		List<String> videosUsr = new ArrayList<String>();
		for(Video video: userVideos) {
			videosUsr.add(video.getNombre());
		}
		return videosUsr;
	}
	
	
	public List<Video> getUserVideos(String nickname) {
		
		List<Video> userVideos = new ArrayList<Video>();
		Usuario user = getUser(nickname);
		if(userIsNull(user)) {
			System.out.println("El usuario es null, habria que tirar una excepcion");
		}else{
			userVideos = getChannelVideos(user.getCanal());
		}
		
		return userVideos;
	}
	
	private List<Video> getChannelVideos(Canal channel) {
		List<Video> channelVideos = new ArrayList<Video>();
		List<Elemento> channelElements = channel.getElementos();
		for(Elemento element: channelElements) {
			if(element instanceof Video) channelVideos.add((Video)element);
		}
		
		return channelVideos;
	}
	
	private List<DtVideo> getChannelVideosDt(Canal channel) {

		List<DtVideo> channelVideos = new ArrayList<DtVideo>();
		List<Elemento> channelElements = channel.getElementos();
		DtVideo dtVideo;
		for(Elemento element: channelElements) {
			if(element instanceof Video){
				dtVideo = new DtVideo(element.isPublico(), element.getNombre(), ((Video) element).getDuracion(), ((Video) element).getUrl(), ((Video) element).getDescripcion(), ((Video) element).getFechaPublicacion());
				List<Comentario> comentariosVideo = ((Video) element).getComentarios();
				List<DtComentario> dtComentarios =agregarSubComentarios(comentariosVideo);
				dtVideo.setComentarios(dtComentarios);
				channelVideos.add(dtVideo);
			}
		}
		
		return channelVideos;
	}
	
	private List<DtComentario> agregarSubComentarios(List<Comentario> comentarios) {
		
		List<DtComentario> dtComentarios = new ArrayList<DtComentario>();
		
		Iterator<Comentario> iterator = comentarios.iterator();
		while (iterator.hasNext()) {
			Comentario comentario = iterator.next();
			if(comentario.getComentarios().size() == 0) {
				DtComentario dtComentario = new DtComentario(comentario.getDescripcion(), comentario.getFecha(), comentario.getUsuario().getNickname());
				dtComentarios.add(dtComentario);
			}else {
				recursivoComentario(comentario, comentario.getComentarios().get(0), dtComentarios);
			}
		}
		return dtComentarios;
	}
	
	private void recursivoComentario(Comentario anterior, Comentario actual, List<DtComentario> dtComentarios) {
		
		DtComentario dtAnterior, dtActual;
		
		if(actual.getComentarios().size() == 0) {
			dtAnterior = new DtComentario(anterior.getDescripcion(), anterior.getFecha(), anterior.getUsuario().getNickname());
			dtActual = new DtComentario(actual.getDescripcion(), actual.getFecha(), actual.getUsuario().getNickname());
			dtAnterior.addComentario(dtActual);
			dtComentarios.add(dtAnterior);
		}else {
			recursivoComentario(actual, actual.getComentarios().get(0), dtComentarios);
		}
	}
		
	////////////////////////////////////
	public List<String> listarListasRepUsuario(String nick){
		List<ListaDeReproduccion> userListas = getUserListas(nick);
		List<String> listasUsr = new ArrayList<String>();
		for(ListaDeReproduccion lista: userListas) {
			listasUsr.add(lista.getNombre());
			System.out.println(lista.getNombre());
		}
		return listasUsr;
	}
	
	public List<ListaDeReproduccion> getUserListas(String nickname) {
		
		List<ListaDeReproduccion> userListas = new ArrayList<ListaDeReproduccion>();
		Usuario user = getUser(nickname);
		if(userIsNull(user)) {
			System.out.println("El usuario es null, habria que tirar una excepcion");
		}else{
			userListas = getChannelListas(user.getCanal());
		}
		
		return userListas;
	}
	
	private List<ListaDeReproduccion> getChannelListas(Canal channel) {
		List<ListaDeReproduccion> channelListas = new ArrayList<ListaDeReproduccion>();
		List<Elemento> channelElements = channel.getElementos();
		for(Elemento element: channelElements) {
			if(element instanceof ListaDeReproduccion) channelListas.add((ListaDeReproduccion)element);
		}
		
		return channelListas;
	}
	 
	
	public void datosVideo(String nombre, int duracion, String url, String descripcion, String nick, String nombreCat) {
		boolean isPublico = false;
		DtVideo dtv = new DtVideo(isPublico,nombre,duracion,url,descripcion,new Date());
		altaVideo(dtv, nick,nombreCat);
	}
	
	public void altaVideo(DtVideo dtv, String nick, String nombreCat) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);;
		ManejadorCategoria mc= ManejadorCategoria.getInstancia();
		Video aux = new Video(dtv.isPublico(),dtv.getNombre(),dtv.getDuracion(),dtv.getUrl(),dtv.getDescripcion(),dtv.getFechaPublicacion());		
		if(nombreCat.equals("")) {
		}else {
			Categoria cat = mc.buscarCategoria(nombreCat);
			cat.addElemento(aux);		
			aux.setCategoria(cat);
		}
		user.getCanal().addElemento(aux);		
		mU.actualizarUsuario(user);
		
	}
	
	public void datosVideoMV(String nick, String nombre, int duracion, String url, String descripcion, boolean publico,String nuevoNombre) {
		System.out.println(nick + nombre + duracion + url + descripcion);
		DtVideo dtv = new DtVideo(publico, nombre, duracion, url, descripcion, new Date());
		modificarVideo(dtv,nick,nuevoNombre);
		
	}
	public void modificarVideo(DtVideo dtv, String nick, String nuevoNombre) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario user = mU.buscarUsuario(nick);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		System.out.println(dtv.getNombre() + dtv.getDescripcion() + dtv.getDuracion() + dtv.getUrl());
		Video video = searchVideoInChannel(user.getCanal(),dtv.getNombre());
		boolean publico = true;		
		video.setNombre(nuevoNombre);
		video.setUrl(dtv.getUrl());
		video.setDescripcion(dtv.getDescripcion());
		if(video.isPublico() && dtv.isPublico()){		
			publico = false;
		}else if (!video.isPublico() && dtv.isPublico()){
			
		}else {			
			publico = video.isPublico();
		}
			video.setPublico(publico);
			video.setDuracion(dtv.getDuracion());
			org.hibernate.Session session = (Session)em.getDelegate();
			Transaction t = session.beginTransaction();
			session.persist(video);
			session.persist(user);
			t.commit();
			session.disconnect();
							
		}

	public void comentarVideo(String userC, String usuario, String comentario,String caminoArbolComentarios) {
		
		Usuario userCo = getUser(userC);
		Usuario user = getUser(usuario);
		Comentario c = new Comentario(comentario,new Date());
		c.setUsuario(userCo);
		String videoNombre = "";
		Video video = null;
		String[] videoComentarioPath = caminoArbolComentarios.split(",");
		videoNombre = videoComentarioPath[1];
		videoNombre = videoNombre.replace("]", "");
		videoNombre = videoNombre.replace(",", "");
		video = searchVideoInChannel(user.getCanal(), videoNombre.trim());
		System.out.println("voy a buscar el video -"+videoNombre+"-");
		System.out.println("el video tiene un path de "+videoComentarioPath.length);
		if(videoComentarioPath.length == 2) {
			video.addComentario(c);
		}else if(videoComentarioPath.length > 2) {
			String comentarioABuscar = videoComentarioPath[videoComentarioPath.length-1];
			comentarioABuscar = comentarioABuscar.replace("]", "");
			comentarioABuscar = comentarioABuscar.replace(",", "");
			boolean encontreComentario = false;
			Iterator<Comentario> iterator = video.getComentarios().iterator();
			while (iterator.hasNext() && !encontreComentario) {
				Comentario comentarioVideo = iterator.next();
				if(comentarioVideo.getDescripcion().equals(comentarioABuscar.trim())) {
					comentarioVideo.addComentario(c);
				}else {
					comentarioABuscar = comentarioABuscar.split("-")[1].trim();
					agregarComentarioRecursivo(c, comentarioVideo.getComentarios().get(0), comentarioABuscar.trim());
				}
			}
		}
		
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		mU.actualizarUsuario(user);
		mU.actualizarUsuario(userCo);
	
	}
	
	private void agregarComentarioRecursivo(Comentario nuevoComentario, Comentario actual, String comentarioABuscar) {
		System.out.println("Estoy parado en "+actual.getDescripcion());
		System.out.println("Quiero agregar a "+comentarioABuscar);
		if(actual.getDescripcion().equals(comentarioABuscar)) {
			actual.addComentario(nuevoComentario);
		}else {
			agregarComentarioRecursivo(nuevoComentario, actual.getComentarios().get(0), comentarioABuscar);
		}
		
	}

	public Usuario getUser(String nickname) {
		return ManejadorUsuario.getInstancia().buscarUsuario(nickname);
	}
	public void existeUser(String nickname) throws Exception {
		Usuario user =  ManejadorUsuario.getInstancia().buscarUsuario(nickname);
		if(userIsNull(user)) {
			throw new Exception("El usuario ingresado no existe");
		}
	}
	
	
	public boolean userIsNull(Usuario user) {
		return user == null;
	}

	@Override
	public List<DtVideo> getUserDtVideos(String nickname) throws Exception {
		List<DtVideo> userVideos = new ArrayList<DtVideo>();
		Usuario user = getUser(nickname);
		if(userIsNull(user)) {
			System.out.println("el usuario es null");
			throw new Exception("El usuario ingresado no existe");
		}else{
			userVideos = getChannelVideosDt(user.getCanal());
		}
		 
		return userVideos;
	}

	@Override
	public void valorarVideo(String nickname, String nombreVideo, String opcion) {
		
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario usuarioVideo = mU.buscarUsuario(nickname);
		Video video = getVideo(nombreVideo, nickname);
		
		Favorito favorito;
		if(opcion.equals("Si")) favorito = Favorito.Si;
		else favorito = Favorito.No;
		
		MeGusta meGusta = new MeGusta(favorito);
		meGusta.setVideo(video);
		usuarioVideo.addMeGusta(meGusta);
		
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		org.hibernate.Session session = (Session)em.getDelegate();
		Transaction t = session.beginTransaction();
		session.update(usuarioVideo);
		session.persist(meGusta);
		t.commit();
		session.disconnect();

	}
	public List<DtVideo> listarVideos1(String nick) {
		Usuario usuario = new Usuario();
		List<DtVideo> videos = new ArrayList();
		return videos = usuario.ListarVideos(nick);

	}

	@Override
	public List<String> listarPorDefecto() {
		ManejadorListaPorDefecto mdf = ManejadorListaPorDefecto.getInstancia();
		List<String> coso = new ArrayList<String>();
		for (PorDefecto u : mdf.obtenerListas()) {
			coso.add(u.getNombre());
		}
		return coso;
	}
	public List<DtConsultaCat> consultaCategoria(String cat){
		List<DtConsultaCat> resultado = new ArrayList<DtConsultaCat>();
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		List<String> users = mU.listarUsuarios();
		boolean tiene=true;
		for(String user: users) {
			Usuario usuario = getUser(user);
			Canal channel = usuario.getCanal();
			List<Elemento> channelElements = channel.getElementos();			
			for(Elemento element: channelElements) {
				if((element.getCategorias()) != null) {
					tiene = element.getCategorias().getNombre().equals(cat); // Este bool te trae true si el elemento tiene alguna categoria.
	
				/*
					if((element instanceof Video) && (tiene)){
						resultado.add(element.getNombre());
					}
					if((element instanceof ListaDeReproduccion) && (tiene)){
						resultado.add(element.getNombre());
					}
				*/
					if(tiene) {
						DtConsultaCat aux = new DtConsultaCat(usuario.getNickname(),element.getNombre());
						resultado.add(aux);
					}
				}
			}
		}
		return resultado;
		
	}
	public List<String> listarVideosLista(String nick, String nombreLista) {
			
			List<String> resultado = new ArrayList<String>();
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			Usuario usuario = mU.buscarUsuario(nick);
			List<Elemento> elementosUsuario = usuario.getCanal().getElementos();
			ListaDeReproduccion lista = buscarListaEnCanal(elementosUsuario, nombreLista);
			resultado = obtenerNombreVideos(lista.getVideos());
			return resultado;
			
		}
	
	private ListaDeReproduccion buscarListaEnCanal(List<Elemento> elementos, String nombreLista) {
			
			ListaDeReproduccion lista = null;
			boolean encontreLista = false;
			int i = 0;
			while(!encontreLista && i<elementos.size()) {
				Elemento elemento = elementos.get(i);
				if(elemento.getNombre().equals(nombreLista)) {
					lista = (ListaDeReproduccion) elemento;
					encontreLista = true;
				}
				i++;
			}
			return lista;
		}
		
	
	public void quitarVideoLista(String nickname, String nombreLista, String nombreVideo) {
		
			List<String> resultado = new ArrayList<String>();
			ManejadorUsuario mU = ManejadorUsuario.getInstancia();
			Usuario usuario = mU.buscarUsuario(nickname);
			List<Elemento> elementosUsuario = usuario.getCanal().getElementos();
			ListaDeReproduccion lista = buscarListaEnCanal(elementosUsuario, nombreLista);
			
			boolean encontreVideo = false;
			int i = 0;
			while(!encontreVideo && i<lista.getVideos().size()) {
				if(lista.getVideos().get(i).getNombre().equals(nombreVideo)) {
					lista.removeVideo(lista.getVideos().get(i));
					encontreVideo = true;
				}
				i++;
			}
			
			Conexion conexion = Conexion.getInstancia();
			EntityManager em = conexion.getEntityManager();
			org.hibernate.Session session = (Session)em.getDelegate();
			Transaction t = session.beginTransaction();
			session.update(usuario);
			session.update(lista);
			t.commit();
			session.disconnect();
		}	
	private List<String> obtenerNombreVideos(List<Video> videos){
		List<String> resultado = new ArrayList<String>();
		for(Video video: videos) resultado.add(video.getNombre());
		return resultado;
	}

}