package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("V")
public class Video extends Elemento {
	private Integer duracion;
	private String url;
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Comentario> comentarios = new ArrayList<Comentario>();
	
	@ManyToMany(mappedBy="videos")
	private List<ListaDeReproduccion> listasDeReprod;
	
	public Video() {
		
	}
	
	public Video(boolean publico, String nombre, Integer duracion, String url, String descripcion,
			Date fechaPublicacion) {
		super(publico, nombre);
		this.duracion = duracion;
		this.url = url;
		this.descripcion = descripcion;
		this.fechaPublicacion = fechaPublicacion;
	}

	public Integer getDuracion() {
		return duracion;
	}

	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescripcion() {
		return descripcion;
	}
	

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private String descripcion;
	private Date fechaPublicacion;
	
	public Video(boolean publico, String nombre) {
		super(publico, nombre);
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	
	public List<ListaDeReproduccion> getListasDeReprod() {
		return listasDeReprod;
	}

	public void setListasDeReprod(List<ListaDeReproduccion> listasDeReprod) {
		this.listasDeReprod = listasDeReprod;
	}

	public void addComentario(Comentario comentario) {
		this.comentarios.add(comentario);
	}


}
