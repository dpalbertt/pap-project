package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Categoria {
	
	@Id
	private String nombreCat;
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Elemento> videos = new ArrayList<Elemento>();
	
	public Categoria() {
		
	}
	public Categoria(String nombre) {
		super();
		this.nombreCat = nombre;
	}

	public String getNombre() {
		return nombreCat;
	}


	public void addElemento(Video video) {
		this.videos.add(video);
	}
	public void addElemento(ListaDeReproduccion lista) {
		this.videos.add(lista);
	}
	
	public void quitarElemento (Elemento e) {
		this.videos.remove(e);
	}
}
