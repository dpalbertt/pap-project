package logica;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import datatypes.Favorito;

@Entity
public class MeGusta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private Favorito tipo;
	
	@ManyToOne
	private Video video;
	public MeGusta() {
		
	}
	public MeGusta(Favorito tipo) {
		super();
		this.tipo = tipo;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

}
