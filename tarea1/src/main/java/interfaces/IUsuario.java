package interfaces;

import java.awt.Image;
import java.util.Date;
import java.util.List;

import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import logica.Categoria;
import logica.Usuario;

public interface IUsuario {
	
	public void crearUsuarioL(DtUsuario datosU, DtLista datosL);
	public void crearUsuario(DtUsuario datosU);
	public void altaCategoria(String nombre);
	public List<String> listarCategorias();
	public DtUsuario  getUsuario(String nickname);
	public void guardarImagen(Image origImage);
	public Image getImagen();
	public Usuario findUsuarioByNickname(String nickname);
	
	public void seguirUsuario(String seguidor, String seguido);
	public void dejarSeguirUsuario(String seguidor, String seguido);
	
	public List<String> listarUsuarios();
	public List<String> listarUsuariosTodos();
	public List<String> listarUsuariosBaja();
	public List<String> listarUsuariosQueSigo(Usuario u);
	public List<String> misSeguidores(Usuario usuario);
	public void modificarUsuario(DtUsuario dtu,String nuevoPassword, String nick, String nuevoNombre, String nuevoApellido, Date fecha, String imagen,boolean publico,
				String nombreCanal,String descripcion,boolean habilitado);
	public void bajaUsuario(DtUsuario dtu, boolean habilitado);
	public void datosUsuario(String nick, String password, String email, String nuevoNombre, String nuevoApellido, Date nuevaFecha, String nuevaImagen,boolean publico,
			String nombreCanal,String descripcion);
	public List<String> listarListaDeUsuario(String nick);
	public boolean existeUsuario(String usuario);
	public boolean iniciarSesion(String usuario, String password);
	public boolean soySeguidor(String nickSeguidor, String seguido);
}