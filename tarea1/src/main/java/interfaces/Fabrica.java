package interfaces;

import logica.CElemento;
import logica.CUsuario;

public class Fabrica {
	private static Fabrica instancia = null;
	
	private Fabrica(){}
	
	public static Fabrica getInstancia() {
		if (instancia == null)
			instancia = new Fabrica();
		return instancia;
	}
	
	public IUsuario getIControladorUsuario() {
		return new CUsuario();
	}
	
	public IElemento getControladorElemento() {
		return new CElemento();
	}

}
