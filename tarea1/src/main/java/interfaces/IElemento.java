package interfaces;

import java.util.Date;
import java.util.List;

import datatypes.DtConsultaCat;
import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import datatypes.Favorito;
import datatypes.TipoLista;

import datatypes.DtLista;
import datatypes.DtVideo;
import datatypes.TipoLista;
import logica.Categoria;
import logica.Comentario;
import logica.ManejadorUsuario;
import logica.Usuario;
import logica.Video;

public interface IElemento {
	/* funciones para Crear lista */
	//public void tipoDeLista(TipoLista tipo);
	public void altaListaDf(String nombre); //Hecho en WS
	public void crearLista(DtLista lista, String user); //Hecho en WS
	public void asignarCategoria(String nombreCat, DtLista lista, String user); //Hecho en WS
	
	/*funciones para Agregar video a lista */
	public DtUsuario seleccionarUsuario(String nick); //Hecho en WS
	public List<DtVideo> listarVideos(DtUsuario user); //Hecho en WS
	public List<DtVideo> listarVideos1(String nick); //Hecho en WS
	public List<DtLista> listarListas(DtUsuario user); //Hecho en WS
	public DtLista seleccionarLista(String nombreList, String nick); //Hecho en WS
	public void agregarVideoALista(String nickVideo, String nickLista, String nomVid, String nomList); //Hecho en WS
	public List<String> listarCategorias(); //Hecho en WS
	
	/* Operaciones para videos */
	public List<String> listarVideoUsuario(String nick); //Hecho en WS
	public List<String> listarVideosLista(String nick, String nombreLista); //Hecho en WS
	public void datosVideo(String nombre, int duracion, String url, String descripcion,String nick,String nombreCat); //Hecho en WS
	public void altaVideo(DtVideo dtv,String nick,String nombreCat); //Hecho en WS
	public void comentarVideo(String userC,String usuario,String comentario,String videoNombre); //Hecho en WS
	public void modificarLista(String nombreL,String nick, boolean publico, String cat); //Hecho en WS
	public void quitarVideoLista(String nickname, String nombreLista, String nombreVideo); //Hecho en WS
	/* funciones para Consultar lista */
	
	/* Operaciones para videos */
	public List<Video> getUserVideos(String nickname); //Hecho en WS
	public List<DtVideo> getUserDtVideos(String nickname) throws Exception; //Hecho en WS
	public void existeUser(String nickname) throws Exception; //Hecho en WS
	public DtVideo getUserVideo(String video, String nickname); //Hecho en WS
	public void valorarVideo(String nickname, String nombreVideo, String opcion); //Hecho en WS
	public void modificarVideo(DtVideo dtv, String nick,String nuevoNombre); //Hecho en WS
	public void datosVideoMV(String nick, String nombre, int duracion, String url, String descripcion, boolean publico,String nuevoNombre); //Hecho en WS
	//sobre los manejadores
	public List<String> listarPorDefecto(); //Hecho en WS
	
	/*Operaciones para listas */
	public List<String> listarListasRepUsuario(String nick); //Hecho en WS
	public DtLista buscarListaDT(String usuario, String nombreL); 
	
	/*Consulta categoria*/
	public List<DtConsultaCat> consultaCategoria(String cat); //Hecho en WS
}

