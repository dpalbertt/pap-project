package publicadores;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import datatypes.DtConsultaCat;
import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import interfaces.Fabrica;
import interfaces.IElemento;
import logica.Usuario;
import logica.Video;


@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class WSControladorElemento {
	private Fabrica fabrica;
	private IElemento ielemento;
	private Endpoint endpoint;

	public WSControladorElemento() {
		fabrica = Fabrica.getInstancia();
		ielemento = fabrica.getControladorElemento();
		
	}

	@WebMethod(exclude = true)
	public void publicar() {
		endpoint = Endpoint.publish("http://localhost:1224/cElemento", this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
        return endpoint;
	}
	

	@WebMethod
	public void altaListaDf(String nombre) {
		ielemento.altaListaDf(nombre);
	}
	
	@WebMethod
	public void crearLista(DtLista lista, String user) {
		ielemento.crearLista(lista,user);
	}
	
	
	@WebMethod
	public void asignarCategoria(String nombreCat, DtLista lista, String user) {
		ielemento.asignarCategoria(nombreCat, lista, user);
	}
	

	@WebMethod
	public DtUsuario seleccionarUsuario(String nick) {
		return ielemento.seleccionarUsuario(nick);
	}
	
	@WebMethod
	public DtVideo[] listarVideos(DtUsuario user){
		List<DtVideo> videos = ielemento.listarVideos(user);
		DtVideo[] devuelvo = new DtVideo[videos.size()];
		int i = 0;
		for (DtVideo v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtVideo[] listarVideos1(String nick){
		List<DtVideo> videos = ielemento.listarVideos1(nick);
		DtVideo[] devuelvo = new DtVideo[videos.size()];;
		int i = 0;
		for (DtVideo v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtLista[] listarListas(DtUsuario user){
		List<DtLista> listas = ielemento.listarListas(user);
		DtLista[] devuelvo = new DtLista[listas.size()];
		int i = 0;
		for (DtLista l : listas) {
			devuelvo[i] = l;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtLista seleccionarLista(String nombreList, String nick) {
		return ielemento.seleccionarLista(nombreList,nick);
	}
	
	@WebMethod
	public void agregarVideoALista(String nickVideo, String nickLista, String nomVid, String nomList) {
		ielemento.agregarVideoALista(nickVideo, nickLista, nomVid, nomList);
	}
	
	@WebMethod
	public String[] listarCategorias(){
		List<String> categorias = ielemento.listarCategorias();
		String[] devuelvo = new String[categorias.size()];
		int i = 0;
		for (String c : categorias) {
			devuelvo[i] = c;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] listarVideoUsuario(String nick) {
		List<String> videos = ielemento.listarVideoUsuario(nick);
		String[] devuelvo = new String[videos.size()];
		int i = 0;
		for (String v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] listarVideosLista(String nick, String nombreLista){
		List<String> videos = ielemento.listarVideosLista(nick, nombreLista);
		String[] devuelvo = new String[videos.size()];
		int i = 0;
		for (String v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public void datosVideo(String nombre, int duracion, String url, String descripcion,String nick,String nombreCat) {
		ielemento.datosVideo(nombre, duracion, url, descripcion, nick, nombreCat);
	}
	
	@WebMethod
	public void altaVideo(DtVideo dtv,String nick,String nombreCat) {
		ielemento.altaVideo(dtv, nick, nombreCat);
	}
	
	@WebMethod
	public void comentarVideo(String userC,String usuario,String comentario,String videoNombre) {
		System.out.println("me llego un "+userC+"-"+usuario+"-"+comentario+"-"+videoNombre);
		ielemento.comentarVideo(userC, usuario, comentario, videoNombre);
	}
	
	@WebMethod
	public void modificarLista(String nombreL,String nick, boolean publico, String cat) {
		ielemento.modificarLista(nombreL, nick, publico, cat);
	}
	
	@WebMethod
	public void quitarVideoLista(String nickname, String nombreLista, String nombreVideo) {
		ielemento.quitarVideoLista(nickname, nombreLista, nombreVideo);
	}
	
	@WebMethod
	public Video[] getUserVideos(String nickname){
		List<Video> videos = ielemento.getUserVideos(nickname);
		Video[] devuelvo = new Video[videos.size()];
		int i = 0;
		for (Video v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtVideo[] getUserDtVideos(String nickname) throws Exception{
		List<DtVideo> videos = ielemento.getUserDtVideos(nickname);
		DtVideo[] devuelvo = new DtVideo[videos.size()];
		int i = 0;
		for (DtVideo v : videos) {
			devuelvo[i] = v;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public void existeUser(String nickname) throws Exception{
		ielemento.existeUser(nickname);
	}
	
	@WebMethod
	public DtVideo getUserVideo(String video, String nickname) {
		return ielemento.getUserVideo(video, nickname);
	}
	
	@WebMethod
	public void valorarVideo(String nickname, String nombreVideo, String opcion) {
		ielemento.valorarVideo(nickname, nombreVideo, opcion);
	}

	@WebMethod
	public void modificarVideo(DtVideo dtv, String nick,String nuevoNombre) {
		ielemento.modificarVideo(dtv, nick, nuevoNombre);
	}
	
	@WebMethod
	public void datosVideoMV(String nick, String nombre, int duracion, String url, String descripcion, boolean publico,String nuevoNombre) {
		ielemento.datosVideoMV(nick, nombre, duracion, url, descripcion, publico, nuevoNombre);
	}
	
	@WebMethod
	public String[] listarPorDefecto(){
		List<String> listasPd = ielemento.listarPorDefecto();
		String[] devuelvo = new String[listasPd.size()];
		int i = 0;
		for (String l : listasPd) {
			devuelvo[i] = l;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] listarListasRepUsuario(String nick){
		List<String> listas = ielemento.listarListasRepUsuario(nick);
		String[] devuelvo = new String[listas.size()];
		int i = 0;
		for (String l : listas) {
			devuelvo[i] = l;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtConsultaCat[] consultaCategoria(String cat){
		List<DtConsultaCat> consultas = ielemento.consultaCategoria(cat);
		DtConsultaCat[] devuelvo = new DtConsultaCat[consultas.size()];
		int i = 0;
		for (DtConsultaCat c : consultas) {
			devuelvo[i] = c;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public DtLista buscarListaDT(String usuario, String nombreL) {
		return ielemento.buscarListaDT(usuario, nombreL);
	}
}
