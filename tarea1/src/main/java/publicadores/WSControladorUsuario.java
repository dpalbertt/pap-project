package publicadores;

import java.awt.Image;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import datatypes.DtLista;
import datatypes.DtUsuario;
import interfaces.Fabrica;
import interfaces.IUsuario;
import logica.Usuario;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class WSControladorUsuario {
	private Fabrica fabrica;
	private IUsuario iuser;
	private Endpoint endpoint;

	public WSControladorUsuario() {
		fabrica = Fabrica.getInstancia();
		iuser = fabrica.getIControladorUsuario();
	}

	@WebMethod(exclude = true)
	public void publicar() {
		endpoint = Endpoint.publish("http://localhost:1224/cUsuario", this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
        return endpoint;
	} 
	
	//LOS MÉTODOS QUE VAMOS A PUBLICAR
	@WebMethod
	public boolean iniciarSesion(String usuario, String password) {
		return iuser.iniciarSesion(usuario, password);
	}
	
	@WebMethod
	public void crearUsuarioL(DtUsuario datosU, DtLista datosL) {
		iuser.crearUsuarioL(datosU, datosL);
	}
	@WebMethod
	public void crearUsuario(DtUsuario datosU) {
		iuser.crearUsuario(datosU);
	}
	@WebMethod
	public void altaCategoria(String nombre) {
		iuser.altaCategoria(nombre);
	}
	@WebMethod
	public String[] listarCategorias() {
		List<String> categorias = iuser.listarCategorias();
		String[] devuelvo = new String[categorias.size()];
		int i = 0;
		for (String s : categorias) {
			devuelvo[i] = s;
			i++;
		} 
		return devuelvo;
	}
	@WebMethod
	public DtUsuario getUsuario(String nickname) {
		return iuser.getUsuario(nickname);
	}
	
	@WebMethod
	public Usuario findUsuarioByNickname(String nickname) {
		return iuser.findUsuarioByNickname(nickname);
	}
	
	@WebMethod
	public void seguirUsuario(String seguidor, String seguido) {
		iuser.seguirUsuario(seguidor, seguido);
	}
	
	
	
	@WebMethod
	public void dejarSeguirUsuario(String seguidor, String seguido) {
		iuser.dejarSeguirUsuario(seguidor, seguido);
	}
	
	@WebMethod
	public String[] listarUsuariosTodos(){
		List<String> usuarios = iuser.listarUsuariosTodos();
		String[] devuelvo = new String[usuarios.size()];
		int i = 0;
		for (String s : usuarios) {
			devuelvo[i] = s;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] listarUsuarios(){
		List<String> usuarios = iuser.listarUsuarios();
		String[] devuelvo = new String[usuarios.size()];
		int i = 0;
		for (String s : usuarios) {
			devuelvo[i] = s;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] listarUsuariosQueSigo(String u) {
		Usuario user = iuser.findUsuarioByNickname(u);
		List<String> usuarios = iuser.listarUsuariosQueSigo(user);
		String[] devuelvo = new String[usuarios.size()];
		int i = 0;
		for (String s : usuarios) {
			devuelvo[i] = s;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public String[] misSeguidores(String usuario) {
		Usuario user = iuser.findUsuarioByNickname(usuario);
		List<String> usuarios = iuser.misSeguidores(user);
		String[] devuelvo = new String[usuarios.size()];
		int i = 0;
		for (String s : usuarios) {
			devuelvo[i] = s;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public void modificarUsuario(DtUsuario dtu,String nuevoPassword, String nick, String nuevoNombre, String nuevoApellido, Date fecha, String imagen,boolean publico,
				String nombreCanal,String descripcion,boolean habilitado) {
		iuser.modificarUsuario(dtu, nuevoPassword, nick, nuevoNombre, nuevoApellido, fecha, imagen, publico, nombreCanal, descripcion, habilitado);
	}
	
	@WebMethod
	public void datosUsuario(String nick, String password, String email, String nuevoNombre, String nuevoApellido, Date nuevaFecha, String nuevaImagen,boolean publico,
			String nombreCanal,String descripcion) {
		iuser.datosUsuario(nick, password, email, nuevoNombre, nuevoApellido, nuevaFecha, nuevaImagen, publico, nombreCanal, descripcion);
	}
	
	@WebMethod
	public String[] listarListaDeUsuario(String nick) {
		List<String> listas = iuser.listarListaDeUsuario(nick);
		String[] devuelvo = new String[listas.size()];
		int i = 0;
		for (String s : listas) {
			devuelvo[i] = s;
			i++;
		}
		return devuelvo;
	}
	
	@WebMethod
	public boolean existeUsuario(String usuario) {
		return iuser.existeUsuario(usuario);
	}
	
	@WebMethod
	public boolean soySeguidor(String nickSeguidor, String seguido) {
		return iuser.soySeguidor(nickSeguidor, seguido);
	}
	
	@WebMethod
	public void bajaUsuario(DtUsuario dtu, boolean habilitado) {
		iuser.bajaUsuario(dtu, habilitado);
	}
}
