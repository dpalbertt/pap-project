package test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtCanal;
import datatypes.DtComentario;
import datatypes.DtConsultaCat;
import datatypes.DtLista;
import datatypes.DtUsuario;
import datatypes.DtVideo;
import datatypes.TipoLista;
import interfaces.Fabrica;
import interfaces.IElemento;
import interfaces.IUsuario;
import logica.CElemento;
import logica.Canal;
import logica.Elemento;
import logica.ManejadorCategoria;
import logica.ManejadorListaPorDefecto;
import logica.ManejadorUsuario;
import logica.Video;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApplicationTest {
	
	private static Fabrica fabrica = Fabrica.getInstancia();
	private static IUsuario IUsuario = fabrica.getIControladorUsuario();
	private static ManejadorUsuario manejadorUsuario = ManejadorUsuario.getInstancia();
	private static ManejadorCategoria manejadorCategoria = ManejadorCategoria.getInstancia();
	private static ManejadorListaPorDefecto manejadorListaDf = ManejadorListaPorDefecto.getInstancia();
	private static IElemento IElemento = fabrica.getControladorElemento();
	private CElemento cElemento = new CElemento(); 
	
	@BeforeClass
	public static void setupTest() {		
		DtUsuario dtUsuario = obtenerUsuarioSetup();
		dtUsuario.setCanal(obtenerCanalSetup());
		
		DtUsuario dtUsuarioDos = obtenerUsuarioSetupDos();
		dtUsuarioDos.setCanal(obtenerCanalSetupDos());
		
		IUsuario.crearUsuario(dtUsuario);
		IUsuario.crearUsuario(dtUsuarioDos);
	}
	
	@Test
	public void CrearNuevoUsuario() {
		DtUsuario dtUsuario = obtenerUsuarioValido();
		dtUsuario.setCanal(obtenerCanalValido());
		IUsuario.crearUsuario(dtUsuario);
		Assert.assertNotNull(manejadorUsuario.buscarUsuario(dtUsuario.getNickname()));
	}
	
	@Test
	public void CrearNuevoUsuarioConLista() {
		DtLista dtLista = new DtLista(true, "NuevaListaReprod", TipoLista.PorDefecto);
		DtUsuario dtUsuario = obtenerOtroUsuarioValido();
		dtUsuario.setCanal(obtenerOtroCanalValido());
		IUsuario.crearUsuarioL(dtUsuario, dtLista);
		Assert.assertNotNull(manejadorUsuario.buscarUsuario(dtUsuario.getNickname()));
	}
	
	@Test
	public void existeUsuario() {
		Assert.assertTrue(IUsuario.existeUsuario("usuarioSetup"));
	}
	
	@Test
	public void iniciarSesion() {
		Assert.assertTrue(IUsuario.iniciarSesion("usuarioSetup", "usuarioSetup"));
	}
	
	@Test
	public void altaCategoria() {
		IUsuario.altaCategoria("categoriaTest");
		Assert.assertNotNull(manejadorCategoria.buscarCategoria("categoriaTest"));
	}
	
	@Test
	public void getUsuarioExistente() {
		Assert.assertNotNull(IUsuario.getUsuario("usuarioSetup"));
	}
	
	@Test
	public void listarUsuarios() {
		Assert.assertNotNull(IUsuario.listarUsuarios());
	}
	
	@Test
	public void listarCategorias() {
		Assert.assertNotNull(IUsuario.listarCategorias());
	}
	
	@Test
	public void listarUsuariosQueSigo() {
		Assert.assertNotNull(IUsuario.listarUsuariosQueSigo(manejadorUsuario.buscarUsuario("usuarioSetup")));
	}
	
	@Test
	public void misSeguidores() {
		Assert.assertNotNull(IUsuario.misSeguidores(manejadorUsuario.buscarUsuario("usuarioSetup")));
	}
	
	@Test
	public void listarListaDeUsuario() {
		Assert.assertNotNull(IUsuario.listarListaDeUsuario("usuarioSetup"));
	}
	
	@Test
	public void modificarUsuario() {
		IUsuario.datosUsuario("usuarioSetup", "usuarioSetup", "usuarioSetup", "usuarioSetup", "nuevoApellido", new Date(), "usuarioSetup", true, "usuarioSetup", "usuarioSetup");
		Assert.assertEquals("nuevoApellido", IUsuario.getUsuario("usuarioSetup").getApellido());
	}

	@Test
	public void altaListaDf() {
		IElemento.altaListaDf("testListaDF");
		Assert.assertNotNull(manejadorListaDf.obtenerListas()); 
	}
	
	@Test 
	public void crearLista() {
		DtLista dtLista = new DtLista(true, "testListaDF2", TipoLista.PorDefecto);
		IElemento.crearLista(dtLista, "usuarioSetup");
		Assert.assertNotNull(manejadorListaDf.obtenerListas());
	}

	@Test
	public void seleccionarUsuario() {
		Assert.assertNotNull(IElemento.seleccionarUsuario("usuarioSetup"));
	}
	
	@Test
	public void listarVideos() {
		DtUsuario dtUsuario = IUsuario.getUsuario("usuarioSetup");
		Assert.assertNotNull(IElemento.listarVideos(dtUsuario));
	}
	
	@Test
	public void altaVideo() {
		DtVideo dtVideo = new DtVideo(true, "videoTest",200, "videoTest", "videoTest", new Date());
		IElemento.altaVideo(dtVideo, "usuarioSetup", "categoriaTest");
		Assert.assertNotNull(IElemento.getUserVideo("videoTest", "usuarioSetup"));
	}
	
	@Test 
	public void getUserVideo() {
		Assert.assertNotNull(IElemento.getUserVideo("videoTest", "usuarioSetup"));
	}
	
	@Test
	public void listarListas() {
		DtUsuario dtUsuario = IUsuario.getUsuario("usuarioSetup");
		Assert.assertNotNull(IElemento.listarListas(dtUsuario));
	}
	
	@Test
	public void getUserVideos() {
		Assert.assertNotNull(IElemento.getUserVideos("usuarioSetup"));
	}
	
	@Test
	public void getUserDtVideos() throws Exception {
		Assert.assertNotNull(IElemento.getUserDtVideos("usuarioSetup"));
	}
	
	@Test
	public void listarVideosLista() {
		Assert.assertNotNull(IElemento.listarVideosLista("usuarioSetup", "testListaDF2"));
	}
	
	@Test
	public void consultaCategoria() {
		Assert.assertNotNull(IElemento.consultaCategoria("categoriaTestEl"));
	}
	
	@Test
	public void listaPorDefecto() {
		Assert.assertNotNull(IElemento.listarPorDefecto());
	}
	
	@Test
	public void listarVideos1() {
		Assert.assertNotNull(IElemento.listarVideos1("usuarioSetup"));
	}
	
	@Test
	public void listarVideoUsuario() {
		Assert.assertNotNull(IElemento.listarVideoUsuario("usuarioSetup"));
	}
	
	@Test
	public void listarCategoriasElemento() {
		Assert.assertNotNull(IElemento.listarCategorias());
	}

	@Test
	public void seleccionarLista() {
		Assert.assertNotNull(IElemento.seleccionarLista("testListaDF2", "usuarioSetup"));
	}
	
	@Test
	public void zBuscarListaDt() {
		Assert.assertNotNull(IElemento.seleccionarLista("testListaDF2", "usuarioSetup"));
	}
	
	@Test
	public void valorarVideo() {
		IElemento.valorarVideo("usuarioSetup", "videoTest", "Si");
		Assert.assertTrue(manejadorUsuario.buscarUsuario("usuarioSetup").getMegusta().size() == 1);
	}
	
	@Test
	public void asignarCategoria() {
		DtLista dtLista = new DtLista(true, "testListaDF", TipoLista.PorDefecto);
		IElemento.asignarCategoria("categoriaTest", dtLista, "usuarioSetup");
		Assert.assertNotNull(manejadorUsuario.buscarUsuario("usuarioSetup").getCanal().getElementos());
	}
	
	@Test
	public void modificarVideo() {
		IElemento.datosVideoMV("usuarioSetup", "videoTest", 10, "url", "descripcion", true, "videoTest");
		
		List<Elemento> elementos = manejadorUsuario.buscarUsuario("usuarioSetup").getCanal().getElementos();
		int i = 0;
		boolean flagBusqueda = false;
		Video elementoBusqueda = null;
		while(i < elementos.size() && !flagBusqueda) {
			if(elementos.get(i).getNombre().equals("videoTest")) {
				elementoBusqueda = (Video) elementos.get(i);
				flagBusqueda = true;
			}
			i++;
		}
		Assert.assertTrue(elementoBusqueda.getDuracion() == 10);
	}
	
	@Test
	public void listarTodosUsuarios() {
		Assert.assertNotNull(manejadorUsuario.listarUsuariosTodos());
	}
	
	@Test
	public void listarUsuariosBaja() {
		Assert.assertNotNull(manejadorUsuario.listarUsuariosBaja());
	}
	
	@Test
	public void seguirUsuarioManejador() {
		manejadorUsuario.seguirUsuario("usuarioValido", "usuarioSetup");
		Assert.assertNotNull(manejadorUsuario.buscarUsuario("usuarioValido").soySeguidor("usuarioSetup"));
	}
	
	@Test
	public void listarListasUsuario() {
		Assert.assertNotNull(IElemento.listarListasRepUsuario("usuarioSetup"));
	}
	
	@Test
	public void buscarLista() {
		Assert.assertNotNull(cElemento.buscarLista("usuarioSetup", "testListaDF"));
	}
	
	@Test
	public void getUser() {
		Assert.assertNotNull(cElemento.getUser("usuarioSetup"));
	}
		
	@Test
	public void esNumero() {
		Assert.assertFalse(cElemento.userIsNull(manejadorUsuario.buscarUsuario("usuarioSetup")));
	}
	
	@Test
	public void modificarLista() {
		IElemento.modificarLista("testListaDF", "usuarioSetup", true, "categoriaTest");
	}
	
	@Test
	public void testCanal() {
		Canal canal = new Canal();
		canal.buscarVideoDt("videoTest");
		canal.buscarVideo("videoTest");
		canal.buscarListaDT("testListaDF");
		canal.buscarLista("testListaDF");
		canal.listarListas();
		canal.listarVideos();
		Assert.assertNotNull(canal);
	}
	
	@Test
	public void testDtConsultaCat() {
		DtConsultaCat dtConsDos = new DtConsultaCat();
		DtConsultaCat dtCons = new DtConsultaCat("usuarioSetup","testListaDF");
		Assert.assertNotNull(dtCons);
	}
	
	@Test
	public void testDtCanal() {
		DtCanal dtCanal = new DtCanal();
		Assert.assertNotNull(dtCanal);
	}
	
	@Test
	public void testDtLista() {
		DtLista dtLista = new DtLista();
		dtLista.getCategoria();
		dtLista.getTipo();
		dtLista.listarDtvideo();
		Assert.assertNotNull(dtLista);
	}
	
	@Test
	public void testDtUsuario() {
		DtUsuario dtUsuario = new DtUsuario();
		dtUsuario.setApellido("test");
		dtUsuario.setCanal(new DtCanal());
		dtUsuario.setEmail("test");
		dtUsuario.setfNacimiento(new Date());
		dtUsuario.setFoto("test");
		dtUsuario.setHabilitado(true);
		dtUsuario.setNickname("test");
		dtUsuario.setNombre("a");
		dtUsuario.setPassword("ass");
		dtUsuario.setPublico(true);
		Assert.assertNotNull(dtUsuario);
	}
	
	@Test
	public void testDtComentario() {
		DtComentario dtComentario = new DtComentario();
		DtComentario dtComentarioDos = new DtComentario("descripcion", new Date(), "test");
		dtComentario.setComentarios(new ArrayList<DtComentario>());
		dtComentario.getDescripcion();
		dtComentario.getFecha();
		dtComentario.getNickname();
		Assert.assertNotNull(dtComentario);
	}
	
	@Test
	public void testDtVideo() {
		DtVideo dtVideo = new DtVideo();
		dtVideo.getComentarios();
		dtVideo.addComentario(new DtComentario());
		Assert.assertNotNull(dtVideo);
	}
	
	private DtUsuario obtenerUsuarioValido() {
		return new DtUsuario("usuarioValido", "usuarioValido","usuarioValido", "usuarioValido","usuarioValido@uytubepap.com", new Date(), "usuarioValido", true, true);
	}
	
	private DtCanal obtenerCanalValido() {
		return new DtCanal("usuarioValido", true, "usuarioValido");
	}
	
	private DtUsuario obtenerOtroUsuarioValido() {
		return new DtUsuario("otroUsuarioValido", "otroUsuarioValido","otroUsuarioValido", "otroUsuarioValido","otroUsuarioValido@uytubepap.com", new Date(), "otroUsuarioValido", true, true);
	}
	
	private DtCanal obtenerOtroCanalValido() {
		return new DtCanal("otroUsuarioValido", true, "otroUsuarioValido");
	}
	
	private static DtUsuario obtenerUsuarioSetup() {
		return new DtUsuario("usuarioSetup", "usuarioSetup","usuarioSetup", "usuarioSetup","usuarioSetup", new Date(), "usuarioSetup", true, true);
	}
	
	private static DtCanal obtenerCanalSetup() {
		return new DtCanal("usuarioSetup", true, "usuarioSetup");
	}
	
	private static DtUsuario obtenerUsuarioSetupDos() {
		return new DtUsuario("usuarioSetupDos", "usuarioSetupDos","usuarioSetupDos", "usuarioSetupDos","usuarioSetupDos", new Date(), "usuarioSetupDos", true, true);
	}
	
	private static DtCanal obtenerCanalSetupDos() {
		return new DtCanal("usuarioSetupDos", true, "usuarioSetupDos");
	}
		

	@AfterClass
	public static void tearDownTest() {
		manejadorUsuario.quitarUsuario("usuarioValido");
		manejadorUsuario.quitarUsuario("usuarioSetup");
		manejadorUsuario.quitarUsuario("otroUsuarioValido");
		manejadorUsuario.quitarUsuario("usuarioSetupDos");
		manejadorCategoria.eliminarCategoria("categoriaTest");
		manejadorListaDf.quitarListaDF("testListaDF2");
		manejadorListaDf.quitarListaDF("testListaDF");
	}
}
